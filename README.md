# Installation
This code requires a variety of libraries:
- numpy
- scipy
- matplotlib
- astropy
- astroqueries (to look up the coordinates of objects in the tables from the literature)
- openopt (for the global optmization)
- emcee (for the MCMC)
All of these can be installed through your package manager or pip
Extract the code somewhere and make sure that the parent "apogee" directory is in your PYTHONPATH environmental variable

Some scripts also require Michiel's photometry library to load in the Dartmouth isochrones

To install:
1. Install python (probably easiest with a package manager or anaconda)
2. Either install libraries through same package manager or through pip. To install through pip make sure first it is installed and then run `pip install numpy scipy matplotlib astropy astroqueries openopt emcee` (removing any libaries that you installed in another mannar. You probably want to install `ipython` as well (i.e., pip install ipython) and run python through the ipython shell.
3. ensure that the directy containing the extracted apogee folder is in the environmental variable PYHTONPATH. This should ensure that the code can be imported; test by running `from apogee import model_spectra` in python.

# Running any code
To run most of the code presented here you will need to be in the directory with the apogee data. In here you should be able to run:
```python
from apogee import model_spectra, observed_spectrum, spectral_parameters
arr = sp.load('btsettl_vary.npy').view(spectral_parameters.KnownStellarParameters)
arr.grid = model_spectra.pre_loaded('grid/BT-Settl.npy')
specinfo = observed_spectrum.cluster_list('spectra')
```
After running this `arr` should contain the best-fit stellar parameters (test with `print(arr['em_Teff'])`) and `specinfo` should contain a summary of the APOGEE spectra prior to fitting.

# Fitting the spectra
The fitting code relies on code in three well documented files:
- "fit.py"
- "model_spectra.py"
- "observed_spectrum.py"
This code works through the interaction of 4 main objects:
- `SpectralFit` (code in "fit.py") is the overarching object that computes the chi-squared, does the fitting, and the MCMC. This object also provides access to the other objects.
- `Parameter` (code in "fit.py") represents provides the stellar parameters of the current fit. Can be accessed as the `parameters` property of a `SpectralFit` object
- `ModelSpectrumGrid` (code in "model_spectra.py") represents a grid of model spectra and the capability to interpolate within the model grid. After interpolation a `ModelSpectrum` object is returned. This object can be shifted with a radial velocity, rotationally broadened, veiled, DIBs can be added, etc. It is typically stored in the `grid` property of the `Spectralfit`.
- `ApogeeSpectrum` (code in "observed_spectrum.py") represents an observed APOGEE spectrum over a single chip. A list of 3 `ApogeeSpectrum` objects is used to respresent a full APOGEE spectrum in a single epoch. A list of these single-epoch APOGEE spectra represents allspectra observed for a star, while a list of these represents all observed APOGEE spectra for a group of (potentially all) stars.

Typically to run the fitting the following steps should be taken:
1. A ModelSpectrumGrid object is created by loading in a grid of model spectra.
2. A SpectralFit object is created for a specific star and the ModelSpectrumGrid.
3. This SpectralFit object is used to optimize one or more stellar parameters and is used to compute the MCMC.
Each of these steps is explained in more detail below. The code in "fit_script.py" goes through these steps for all the epochs observed for a single star, while the code in "fit_script_epoch.py" goes through these steps for a single epoch.

A script to run all these steps is available in "fit_script_epoch.py". This script actually only runs a very short MCMC, which is not useful to measure the uncertainties. It's mainly meant to compute the global chi-squared minimum in all stellar parameters, after which the script "fit_script_epoch_mc.py" takes the result of this initial fit and computes the MCMC. 
## Loading in and using a ModelSpectrumGrid
The code to work with model spectra is provided in "model_spectra.py", so we start by importing this code by:
```python
from apogee import model_spectra
```
If this code crashes the code is either not correctly installed, dependencies are missing, or it is not installed in a folder named "apogee" like on my computer.

### Loading in grids of model spectra
Several functions have been provided in "model_spectra.py" to load in grids of model spectra:
- `coelho`
- `RVgrid`
- `btsettl_7`
- `btsettl_hdf`
- `phoenix`
- `pre_loaded`

All of these functions return a ModelSpectrumGrid object. For example to load the BT-Settl grid, that I have saved previously, use:
```python
from apogee import model_spectra
grid = model_spectra.pre_loaded('grid/BT-Settlgrid.npz')
```

### Introspecting and altering the grid
Some information will be available for every model spectrum grid, which can be useful to figure out its properties:
- `grid.size`: contains the number of model spectra in the grid.
- `grid.variable_names`: contains a list of variable names, that define the dimensions of the grid (e.g., ['Teff', 'logg', 'FeH'] for a 3-dimensional grid with effective temperature, surface gravity and metallicity)
- `grid.ndim`: contains number of dimensions in the grid (same as `len(grid.variable_names)`)
- `grid.shape`: contains the number of different values the model spectra adopt in the different dimensions (e.g., (15, 5, 3) for the grid with ['Teff', 'logg', 'FeH'] as `grid.variable_names` has model spectra at 15 different effective temperatures, 5 different surface gravities, and three different metallicities). Note that if the grid is perfectly filled its `grid.size` will be equal to the product of `grid.shape`, but typically there will be gaps.
- `grid.possible_values(...)`: Gives all values of a parameter in the model grid. If other parameters are given, the possible values in the model grid at that value for the other parameter are give. For example in the BT-Settl grid the `grid.possible_values('logg')` returns [-0.5,  0. ,  0.5,  1. ,  1.5,  2. ,  2.5,  3. ,  3.5,  4. ,  4.5, 5. ,  5.5], but `grid.possible_values('logg', Teff=6111) returns [ 2. ,  2.5,  3. ,  3.5,  4. ,  4.5,  5. ,  5.5], because at such a high effective temperature only a subset of surface gravities were computed (note that the given Teff does not have to be on a grid point).

Sometimes not all parameters are of interest to the user. In that case a new grid with only the model spectra at one particular value for this dimension can be returned with `grid.at_value(...)`. For example to load the spectra used by the APOGEE data reduction team to measure RV's, you can use:
```python
from apogee import model_spectra
full_grid = model_spectra.RVgrid(path_to_grid)
```
The part of the grid which contains only stars with [C/Fe] = 0 and [alpha/Fe] = 0 is then found by
```python
grid = full_grid.at_value('alpha', 0).at_value('carbon', 0)
```
where we use that the first call to `at_value('alpha', 0)` returns a new ModelSpectrumGrid object on which we call `at_value('carbon', 0)` and the result of this call (which is again a ModelSpectrumGrid object) we store under the variable name `grid`.

Some grid do not load in the individual model spectra, because they would take too long to run, for example the following code runs very quickly because it never actuall loads in the grid:
```python
grid = model_spectra.coelho(path_to_grid)
```
The spectra will be loaded in, when needed. This can significantly slow down the code, so sometimes it is a good idea to explicitly load in the spectra:
```python
grid.load(wavelength_range=(15000, 17200))
```
This code loads in the model spectra over the limited wavelength range given. You do not have to give a wavelength range, but then the whole model spectrum is loaded in, which can lead to memory errors (which this whole system was designed to prevent.

### Interpolating the model grid
Individual spectra can be retreived from a `ModelSpectrumGrid` object stored under the variable `grid` by:
```python
model_spectrum = grid.get_spectrum(...)
```
To this function a value for every parameter has to be provided, so `grid.get_spectrum(Teff=3400, logg=3.2)` will return a model spectrum at this effective temperature and surface gravity, unless there is a third dimension (e.g., metallicity). In that case a `ValueError` is raised. If the provided set of parameters requires extrapolation an `OutOfGridError` is raised.

The interpolation can either use linear or cubic interpolation. Cubic interpolation is used by default, this can be changed by explicitly setting the interpolation style when calling `get_spectrum` (e.g., `grid.get_spectrum(Teff=3400, logg=3.2, interpolation='linear')) or by changing the default for one specific ModelSpectrumGrid object by setting it to the `interpolation` property (e.g., `grid.interpolation = 'linear'). 

This method is called by the `compute_fit` method of the `SpectralFit` object to extract a model spectrum for the current set of parameters.

### Altering the model spectra
An important part of fitting the model spectra to the observed spectra is to alter the spectra. All alterations can be handled by calling `convert` on a `ModelSpectrum` (as returned by `grid.get_spectrum` when interpolating a `ModelSpectrumGrid`). Note that these alterations do not actually alter the model spectrum, but rather they return a new ModelSpectrum with the requested alterations implemented. For example to retreive the BT-Settl model spectrum at Teff=3400, logg=3.2, which has been rotationally broadened by 10 km/s, you can use:
```python
from python import model_spectrum
grid = model_spectra.pre_loaded('grid/BT-Settlgrid.npz')
unbroadened_spectrum = grid.get_spectrum(Teff=3400, logg=3.2)
spectrum = unbroadened_spectrum.convert(vsini=10)
``` 
The `ModelSpectrum` object is a regular numpy record array with the wavelength and flux as columns. So, if matplotlib is installed you can make a plot of the resulting spectrum with:
```python
import matplotlib.pyplot as plt
plt.plot(spectrum.wavelength, spectrum.flux)
plt.show()
```
or alternatively:
```python
import matplotlib.pyplot as plt
plt.plot(spectrum['wavelength'], spectrum['flux'])
plt.show()
```

If you want to apply an alteration to every spectrum in the model grid, you can call `convert_spectra` directly on the `ModelSpectrumGrid` object. So `new_grid = grid.convert_spectra(resolution=22500)` will convolve every model spectrum in the grid to 22,500. Note that the model spectra keep track of their current resolution, so that if you later call `bad_resolution_grid = new_grid.convert_spectra(resolution=20000)` the spectra will only be convolved by enought to convert from their current resolution of 22,500 to the new resolution of 20,000.

### Example to load in the Coelho grid and alter the model spectra to the resolution and wavelength range of APOGEE and saving the resulting grid
So to prepare the Coelho grid for fitting the APOGEE spectra, you can call:
```python
from apogee import model_spectra
grid = model_spectra.coelho(griddir).at_value('FeH', 0.).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.)).convert_spectra(resolution=22500, new_wavelength=15100. * sp.exp(sp.arange(36e3)/ 3e5))
```
which goes through the following steps:
1. ModelSpectrumGrid object is created by calling `model_spectra.coelho(...)`
2. A new ModelSpectrumGrid object is created by calling `at_value(...)` on the first which only contains the model spectra where the [Fe/H] = 0
3. A third ModelSpectrumGrid object is created containing the same number of model spectra as the previous, but on a wavelength grid given by 15000 * sp.exp(sp.arange(1e5) / 700000.) (which ranges from 1.5 micron to about 1.72 micron at a resolution of 700,000). If the model spectra were not actually loaded into memory, this will automatically happen at this step, because we have to access the individual model spectra to interpolate them to the new wavelength grid.
4. A fourth ModelSpectrumGrid object is created where the resolution of all model spectra is decreased and the spectra are once again interpolated to a new wavelength grid by calling `.convert_spectra(resolution=22500, new_wavelength=15100. * sp.exp(sp.arange(36e3)/ 3e5))`.
To prevent us from having to go through all of these steps every time the resulting grid can be saved:
```python
grid.save(new_filename)
```
In later sessions the grid can then be restored with:
```python
from apogee import pre_loaded
grid = pre_loaded(new_filename)
```

## Fitting code
### Loading the SpectralFit object
Typically we start our fitting by creating a `SpectralFit` object by calling `fit.load_fit(...)`. This `SpectralFit` object will have access to the grid of model spectra, the observed spectrum, and the parameters. This function can load in the APOGEE spectrum and the parameters by itself, but we will need to provide the grid of model spectra explicitly:
```python
from apogee import model_spectra, fit
grid = model_spectra.pre_loaded('grid/BT-Settl.npz')
star_fit = fit.load_fit('spectra', grid, star_name='2M03391582+3124306')
```
The first argumetn to `load_fit` is the path to the directory containing the APOGEE spectra (in apVisit files) and the second argument gives the grid of model spectra. This new `SpectralFit` object stored under `star_fit` can be used to fit the spectra observed for 2M03391582+3124306.

To set the intitial parameters to the result of a previous fit, provide the directory where these previous fits were stored: `star_fit = fit.load_fit('spectra', grid, star_name='2M03391582+3124306', resultdir='previous_fit')`.

Hint: To find all spectra with the given `star_name` the code will have to read in the header files of all apVisit files, which can take a long time. To speed things up if you often create new `SpectralFit` objects, you can read in the header files in advance:
```python
from apogee import observed_spectrum
specinfo = observed_spectrum.cluster_list('spectra')
```
where I again assumed that the spectra are stored in the "spectra" directory. This can be passed on to any later call of `load_fit` under the `specinfo` keyword: `star_fit = fit.load_fit('spectra', grid, star_name='2M03391582+3124306', specinfo=specinfo)`, which should greatly increase the efficiency.
### Different levels of fitting
The `SpectralFit` object is designed to be very flexible, so that it can just as easily fit the spectrum in one chip, in one epoch, all the spectra observed for a star, or all spectra observed for all stars at once (although the latter will typically be far too slow). It can be this flexible because at every level the fit is represented by a different `SpectralFit` object, which has the full functionality of `SpectralFit` objects. So the `star_fit` `SpectralFit` object created above for 2M03391582+3124306 actually contains N `SpectralFit` objects, which all fit one of the N epochs of 2M03391582+3124306 and each of these contain 3 SpectralFit objects representing the individual chips. These are referred to as children and can be accessed through the normal python indexing. So if we only wanted to fit the first object of 2M03391582+3124306, which we previously loaded in `star_fit`, we can retreive the requited `SpectralFit` object by calling `epoch_fit = star_fit[0]`. The middle (second) chip of this epoch can be fitted using the object retreived by `chip_fit = epoch_fit[1]` (note that python indices like IDL are zero-based). To move back up the chain you can access the `parent` property, so `epoch_fit.parent` is the same object as was referenced by `star_fit`.

Above I mentioned that a `SpectralFit` object can also fit all spectra observed for all stars at once. This object can be created by simply not providing a `star_name` to the `fit.load_fit` function (i.e., `all_star_fit = fit.load_fit('spectra', grid)`), but I have not really found a use for this yet.

### Keeping parameters fixed at different levels
Together with the `SpectralFit` object a new `Parameters` object is created. This contains the new set of parameters in a dictionary-like object, which is stored in the `parameters` property. By default this will contain parameters for
- all dimensions of the grid of model spectra (Teff, logg, [Fe/H], etc.). These will be automatically read out of the grid of model spectra provided to the `SpectralFit` object.
- "vrad" (radial velocity in km/s), "vsini" (rotational velocity in km/s), "veiling" (veiling level relative to flux), and "poly_params" (parameters of the polynomial fit to the continuum)

For every `SpectralFit` these parameters the values of these parameters are set by one of the following:
1. all of the children have their own values for this stellar parameter.
2. this `SpectralFit` object has its own value, which is applied to all the children (if they exist).
3. The value of this parameter is set by the parent `SpectalFit` object.
For example for radial velocities by default every epoch can have a different radial velocity, so for the `star_fit` property 1 holds, for the `epoch_fit` property 2 holds, but for the individual chips (`chip_fit`) property 3 holds. On the other hand the continuum is fitted seperately in every chip, so for the "poly_params" parameter for both the `star_fit` and `epoch_fit` the parameter varies for the children (property 1) and only the individual chips have a single value (property 2). This behavior can be altered by calling `make_constant`, `make_variable`, or `set_variable_level` (see documentation of these methods). It can also be changed by setting the parameters directly (see below).

### Accessing and setting parameters
The `Parameters` object is dictionary-like, so for example the radial velocity can be read out by `print(star_fit.parameters['vrad'])` or altered by `star_fit.parameters['vrad'] = 15`. By setting this to a single number for the `star_fit` we now ensure that all epochs from now on will have the same radial velocity (i.e., property 2 from previous section), which is initially set to 15 km/s. If we want to set the radial velocities of the individual epochs to 15 km/s (retain property 1), while ensuring that they can in future fitting they can still vary from each other, you can:
1. Iterate through all the subfits and set their radial velocity directly: `for epoch_fit in star_fit: epoch_fit.parameters['vrad'] = 15`
2. Set the radial velocity parameter to a list with one radial velocity per epoch: `star_fit.parameters['vrad'] = [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]` (where we used that our example star 2M03391582+3124306 has been observed for 13 epochs)
3. Step 2 can be generalized by using that len(star_fit) returns the length of the `star_fit` object, which in this case returns the number of subfits (i.e., the number of epochs): `star_fit.parameters['vrad'] = [15] * len(star_fit)`

### Computing the current fit
To compute the model spectrum and the offset from the observed spectrum for the current set of parameters, `compute_fit` can be called. This returns an array, which contains the observed spectrum, the model spectrum, and the offset between the two. So if matplotlib is installed the model spectrum for the current set of parameters and the observed spectrum for the first epoch in star_fit can be overplotted with:
```python
import matplotlib.pyplot as plt
epoch_fit = star_fit[0]
spectrum = epoch_fit.compute_fit(renormalize=True)
# plot observed spectrum in every chip in blue
for spec in spectrum:
	plt.plot(spec['wavelength'], spec['flux'], 'b-')
# plot model spectrum in every chip in red
for spec in spectrum:
	plt.plot(spec['wavelength'], spec['theoretical'], 'r-')
```
where we set `renormalize` to True in the call to `compute_fit` to ensure that the polynomial parameters are refitted to the observed continuum to minimize the offset between the observed and model spectrum.

To compute the chi-squared of the current fit `star_fit.chisq()` can be called.

### Optimizing the current fit
Optimization can be called at every level (i.e., individual stars, individual epochs, or individual chips) by calling the `optimize` method. When calling this method you have to provide a set of parameter that will be varied, for example `star_fit.optimize(['Teff', 'logg', 'vrad', 'vsini', 'veiling'])` will fit all stellar parameters, while `star_fit.optimize(['Teff', 'vrad'])` will only vary the effective temperature and radial velocity, while keeping the surface gravity, rotational velocity, and veiling fixed to their current value. Note that by default there is a single effective temperature over all epochs, while every epoch has a unique radial velocity, so in the latter fit 14 parameters will be actually fitted for our example star 2M03391582+3124306 (i.e., a single Teff and 13 radial velocities for every one of the 13 epochs).

By setting the `method` keyword in the call to `optimize` the optimization routine can be changed. I typically call this routine with `renormalize` set to True, which ensures that the polynomial parameters are adjusted to minimize the chi-squared before the chi-squared is computed for any set of parameters.

#### Global optimization
One routine for global optimization (i.e., searching the whole parameter space for the minimum) has been implemented, namely differential evolution. To use this routine the `openopt` library should be installed. If it is installed the global optimization can be started with `star_fit.optimize([...], method='de')`, where the dots represent the parameters to be varied. This routine tends not to stop, even after it found the global minimum, so it is important to provide a reasonable number of `maxFunEvals`.

#### Local optimization
There are three routines available for local optimization (i.e., searching the local minimum from the current set of parameters). These are all just scipy optimization routines, namely:
- `star_fit.optimize([...], method='')`: Nelder-Mead optimization (seems to be the most reliable; http://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.fmin.html)
- `star_fit.optimize([...], method='tnc')`: Truncated Newton algorithm (http://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.fmin_tnc.html)
- `star_fit.optimize([...], methoc='l_bfgs_b')`: L-BFGS-B algorithm (http://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.fmin_l_bfgs_b.html)

### Computing the MCMC
I use the `emcee` library to compute the uncertainties on the stellar parameters with a Markov chain Monte Carlo simulation. This library is very nicely explained on their website http://dan.iel.fm/emcee/current/ .

Calling this routine is very similar to the optimization: `star_fit.emcee(['Teff', 'logg', 'vsini', 'veiling', 'vrad'], <nwalkers>, <nsteps>)` where the first argument lists the parameters that will be varied during the MCMC and the others list the number of walkers and steps in the MCMC (for more information on this see the `emcee` website http://dan.iel.fm/emcee/current/ ). Once again if the subfits can have different values for a given parameter, then all of these parameters will be varies in the MCMC.

The result of the MCMC will both be returned directly and stored in the propery `res_emcee`. `star_fit.emcee_params` is a helper function to compute the mean, median, and standard deviation of the parameters from the last run MCMC (i.e., the one stored in `res_emcee`).

### Saving the result
Calling `save` on a `SpectralFit` object will save the result in the provided directory. This will store:
- The header of the APOGEE spectrum(a).
- The current set of parameters (which will be assumed to be the best-fit)
- The result of the latest MCMC run (as stored under `res_emcee`)

### Example to fit single epoch
```python
from apogee import model_spectra, fit  # import the required libraries
grid = model_spectra.pre_loaded('grid/BT-Settl.npz')  # load the previously saved model spectrum grid (already at the APOGEE resolution)
star_fit = fit.load_fit('spectra', grid, star_name='2M03391582+3124306') # Create the stellar tofit object
epoch_fit = star_fit[0]  # select the first epoch of this star

# start of with a global optmization to find the global minimum
epoch_fit.optmize(['Teff', 'logg', 'vsini', 'vrad', 'veiling'], method='de', renormalize=True)

# ensure that we are really in the deepest point using local optimization
epoch_fit.optmize(['Teff', 'logg', 'vsini', 'vrad', 'veiling'], method='', renormalize=True)

# vary the number of parameters in the polynomial fit
epoch_fit.optmize_npoly(max_npoly=6)

# Find the best-fit parameters for the new polynomial dimensions
epoch_fit.optmize(['Teff', 'logg', 'vsini', 'vrad', 'veiling'], method='', renormalize=True)

# Run an MCMC around the best-fit
epoch_fit.mcmc(['Teff', 'logg', 'vsini', 'vrad', 'veiling'])

# save the result
epoch_fit.save('some_directory')
```
This is a somewhat simplified version of the scripts used to produce the current set of stellar parameters (i.e., "fit_script_epoch.py" and "fit_script_epoch_mc.py")

# Creating a single table
In the "apogee/scripts" directory there is a (poorly documented) script called "summarize_pickles.py" that reads in all the pickle files produced from fitting the individual stellar epochs (or stars) and produces a single table. At the very bottom of this script is the actual code being run (calling the functions defined above that point):
```python
if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        directory = sys.argv[1]
    else:
        directory = '.'
    print 'reading pickles'
    pickle_list = read_pickles(directory)
    if True: # set to True if every epoch is fitted individually, set to False if if every star is fitted individually
        cut_list = cut_epochs(pickle_list, min_SNR=0, min_Teff=0)
        print 'merging single epochs'
        star_list = merge_pickles(cut_list)
    else:
        star_list = pickle_list
    print 'converting to array'
    arr = convert_to_array(star_list)
    print 'adding literature values'
    arr_with_lit = literature.get_data(arr)
    print 'adding extinction + RV correction'
    arr_with_ext = add_photometry(arr_with_lit)
    arr_with_vcorr = rv_offset.add_rv_correction(arr_with_ext.view(spectral_parameters.AllParameters))
    print 'storing array'
    sp.save(os.path.join(directory, 'per_star.npy'), arr_with_vcorr.view(sp.recarray))
```
This code goes through the following steps:
- Any argument passed on to this function is used as the directory, where the results of the spectral fitting is and where the resulting numpy table will be stored (if None is provided, the current working directory is assumed).
- All the pickle-files from the spectral fitting are read into a single list (i.e., `pickle_list`)
- If individual epochs are fitted the epochs with S/N ratio below min_SNR or best-fit effective temperature below min_Teff are removed from the list resulting in `cut_list`. All epochs representing the same star are then merged to produce a `star_list`.
- If all epochs for a star were fit simultaneously then the `pickle_list` is already the `star_list` from the step above.
- This is then converted into an array by `convert_to_array`. This function also searches for ASPCAP files listing fit results to add to the table.
- Literature values are added by calling the `get_data` function in "scripts/literature.py". This can crash if the `astroqueries` library is not running properly. Rerunning starting from this step should fix that. If it keeps failing and you are not interested in the literature values this step can be skipped by commenting this line and replacing it with `arr_with_lit = arr`.
- Then the extinction estimates are added by `add_photometry`. This requires that the Pleiades stars are within the array. Otherwise the code will crash. This step can be skipped by replacing this line with `arr_with_ext = arr_with_lit`.
- Another column is added to the table containing the results of the RV correction for cool stars. This requires sufficient stars from IC 348 and NGC 1333 to be present to accurately identify the offset. Again the step can be skipped by replacing this line with `arr_with_vcorr = arr_with_ext`.
- Finally the result is saved in the directory under `per_star.npy`.

# Analyzing the stellar parameters
A powerful helper class to analyze the data produced in the `per_star.npy` numpy array is provided in "apogee/spectral_parameters.py". To access this power, use:
```python
from apogee import spectral_parameters
import scipy as sp
arr = sp.load('per_star.npy').view(spectral_parameters.KnownStellarParameters)
```
Nearly all plots I made start with something similar to these lines (although I sometimes replace `sp.load('per_star.npy').view(...)` with `spectral_parameters.load_run(...)`, but the resulting object is the same)

The resulting array referenced by `arr` can be viewed as a table with columns for the best-fit stellar parameters, the mean and uncertainties on the stellar parameters from the MCMC, the information from the header in the original APOGEE spectra, the information from the ASPCAP files (if available while creating "per_star.npy"), and information from literature source. These various sources of information are distiguished by the first letter of the column name:
| First letters | Meaning                                                                                                                                        |
|---------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| a_            | Information of ASPCAP fit                                                                                                                      |
| ah_           | Information from header file of ASPCAP fit                                                                                                     |
| p_            | best-fit parameter value                                                                                                                       |
| em_           | mean of the MCMC                                                                                                                               |
| es_           | standard deviation of the uncertainty                                                                                                          |
| esc_          | MCMC-based uncertainty calibrated to the epoch-to-epoch variability                                                                            |
| ec_           | Change in MCMC in the final steps (should be around 1 for a converged MCMC)                                                                    |
| h_            | Header file of the APOGEE spectrum                                                                                                             |
| l_            | Literature value (the next three letters are a reference to the paper, just search in apogee/scripts/literature.py to figure out which papers) |
| e_            | Information about extinction                                                                                                                   |
| ...           | A few general names (objid, RA, dec, nepochs, etc.) have no prefix.                                                                            |

Every row in the resulting table represents a single star, so this means that some cells might contain multiple values, namely one per observed epoch. Several helper functions are provided to retreive single stellar parameters per star:
- `arr.weighted('Teff')`: Returns a tuple containing an array of the weighted mean of the effective temperatures and an array of the uncertainty on this weighted mean, so use `arr.weighted('Teff')[0]` to get the weighted means and `arr.weighted('Teff')[1]` to get the uncertainties. This works for all stellar parameters (typically 'Teff', 'logg', 'vsini', 'veiling', 'vrad')
- `arr.get_median(...)`: where ... is any column name. This returns the median of the multiple values for a star, for example arr.get_median('p_vrad') returns an array containing for every star the median best-fit radial velocity over all epochs and arr.get_median('JD-MID') returns an array containing for every star the median Julian Date of the observations.
- `arr.get_mean(...)` or arr.get_std(...)`: same as above, but returns respectivily the mean or the standard deviation.

Often you want to analyze the individual epochs rather than the individual stars. This is also provided through:
- `arr.per_epoch(...)`: Returns an array with one row per epoch, containing the value of the parameter for that epoch, for example `arr.per_epoch('em_vrad')` returns an array with the mean radial velocity from the MCMC for every epoch. If no column name is provided an array is returned containing again one row per epoch, but with all columns (a few columns might be missing, that could not be split over multiple epochs).

`arr` is a subclass of a normal numpy array, so all the numpy/scipy goodies are available. This is especially useful for selecting a subset of stars. For example to select all stars with weighted mean effective temperatures cooler than 4500 K use `arr_cool = arr[arr.weighted('Teff')[0] < 4500]` or for all stars in IC 348 use `arr_ic348 = arr[arr['cluster'] == 'IC 348']` (the latter is so common that there is a shortcut, namely `arr_ic348 = arr.get_cluster('IC 348')`). Note that the syntax for the selection of subsets is the same as in IDL, but without the `where` function.

Many more features are available. You can find more of them in "spectral_parameters.py" in the `AllParameters`, `KnownStellarParameters`, and `StellarParameters` classes, all of which are available to an array loaded in like described above. You can also have a look at the example plots.

## Accessing the individual spectra
Often when playing around with the stellar parameter it is useful to look at a specific fit. These individual fits can be retreived after some preperatory steps in which we provide the grid of model spectra and the directory containing the APOGEE spectra:
```python
from apogee import spectral_parameters, model_spectra
import scipy as sp
arr = sp.load('per_star.npy').view(spectral_parameters.KnownStellarParameters)

arr.grid = model_spectra.pre_loaded('grid/BT-Settl.npz')  # load the previously saved model spectrum grid (already at the APOGEE resolution)
arr.specdir = path_to_spectra
star_fit = arr.spectrum('2M03391582+3124306')
```
The resulting `star_fit` is a `SpectralFit` object like described above with the current set of parameters set to the best-fit spectral parameters of the fits.

Hint: To find all spectra with the given `star_name` the code will have to read in the header files of all apVisit files, which can take a long time. To speed things up if you often create new `SpectralFit` objects, you can read in the header files in advance:
```python
from apogee import observed_spectrum
specinfo = observed_spectrum.cluster_list('spectra')
```
where I again assumed that the spectra are stored in the "spectra" directory. This can be passed on to any later call of `load_fit` under the `specinfo` keyword: `star_fit = fit.load_fit('spectra', grid, star_name='2M03391582+3124306', specinfo=specinfo)`, which should greatly increase the efficiency.
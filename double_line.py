"""Fits a double-lined spectroscopic binary.
"""
import scipy as sp
import model_spectra

class DoubleLineSpectrum(object):
    """Represents a double line spectrum.

    The two spectral components are stored in `spectrumA` and `spectrumB`.
    """
    spectrumA = None
    spectrumB = None

    def __init__(self, spectrumA, spectrumB, rel_flux=0.5):
        assert spectrumA.resolution == spectrumB.resolution, 'co-added spectra should have the same resolution!'
        self.spectrumA = spectrumA
        self.spectrumB = spectrumB
        self.rel_flux = rel_flux

    @property
    def resultion(self, ):
        return self.spectrumA.resolution

    def convert(self, resolution=None, vsiniA=None, vsiniB=None, vradA=None, vradB=None, hydrogen_params=None,
                veiling=None, poly_params=None, new_wavelength=None, rel_flux=None):
        """Returns a new shifted, broadened, interpolated spectrum

        In order, convert does:
        1. Degrade the resolution to `resolution` (if `resolution` is not None)
        2. Degrade the indivdiual spectra with rotational velocity `vsiniA` and `vsinB` respectively in km/s (if `vsini` is not None)
        3. Shift the individual spectra with `vradA` and `vradB` in km/s respectively (if `vrad` is not None).
        4. Add the hydrogen lines with `hydrogen_params` as a tuple with (`vrad`, `width`, `depth`, `upper_levels`, `lower_levels`).
        5. Increase flux with `veiling` (if `veiling` is not None).
        6. Multiply flux with a polynomial (if `poly_params` is not None).
        7. Linearly Interpolate to new wavelength scale (if `new_wavelength` is not None).
        """
        if rel_flux is None:
            rel_flux = self.rel_flux
        specA = self.spectrumA.convert(resolution, vsiniA, vradA, hydrogen_params, veiling, poly_params, new_wavelength)
        specB = self.spectrumB.convert(resolution, vsiniB, vradB, hydrogen_params, veiling, poly_params, new_wavelength)
        return DoubleLineSpectrum(specA, specB, rel_flux)

    def __getitem__(self, item):
        if item == 'wavelength':
            return sp.sort(sp.unique(sp.append(self.spectrumA['wavelength'], self.spectrumB['wavelength'])))
        elif item == 'flux':
            if (self.spectrumA['wavelength'] == self.spectrumB['wavelength']).all():
                return self.rel_flux * self.spectrumA['flux'] + (1. - self.rel_flux) * self.spectrumB['flux']
            else:
                wavelength = self['wavelength']
                fluxA = sp.interp(wavelength, self.spectrumA['wavelength'], self.spectrumA['flux'], sp.nan, sp.nan)
                fluxB = sp.interp(wavelength, self.spectrumB['wavelength'], self.spectrumB['flux'], sp.nan, sp.nan)
                return self.rel_flux * fluxA + (1. - self.rel_flux) * fluxB
        raise KeyError("only spec['wavlength'] and spec['flux'] are defined for DoubleLineSpectrum")

    def __getattr__(self, attr):
        try:
            return self[attr]
        except KeyError:
            raise AttributeError()


class DoubleLineGrid(object):
    """Represents a grid of double-line spectra.

    The interface mimics that of a normal grid.
    """
    subgrid = None
    def __init__(self, subgrid):
        """Draws two spectra from given grid.
        """
        self.subgrid = subgrid

    @property
    def ndim(self, ):
        return self.subgrid.ndim * 2

    @property
    def variable_names(self, ):
        return (['%sA' % name for name in self.subgrid.variable_names] +
                ['%sB' % name for name in self.subgrid.variable_names])

    @property
    def shape(self, ):
        return self.subgrid.shape * 2

    @property
    def size(self, ):
        return self.subgrid.size ** 2

    def variable(self, axis_name, all_values=False):
        """return the grid points along the axis named `axis_name`.

        If the last letter of `axis_name` is A or B it is ignored.

        If `all_values` is set to True, returns every grid point multiple times, depending on how many spectra reside on that axis.
        """
        if axis_name[-1] in 'AB':
            axis_name = axis_name[:-1]
        return self.subgrid.variable(axis_name, all_values)

    def possible_values(self, axis_name, **kwargs):
        """Returns the possible values for `axis_name`, where the values in kwargs can still be reached.

        For example possible_values('Teff', logg=1) will return all effective temperatures for which the grid contains values with logg <= 1 and logg >=1.
        Multiple constraints can be added by including multiple keywords.

        Arguments:
        - `axis_name`: name of the axis of which the possible values should be returned. If the last letter is A or B it is ignored.
        - `kwargs`: constraints on the other parameters.
        """
        if axis_name[-1] in 'AB':
            axis_name = axis_name[:-1]
        return self.subgrid.possible_values(axis_name, **kwargs)

    def get_spectrum(self, *args, **kwargs):
        """Returns the DoubleLineSpectrum for the given TeffA, loggA, ... and TeffB, loggB, ... .

        Arguments:
        - `interpolation`: Type of interpolation (currently 'linear' is the only one properly tested)
        - `axis_name`: Sets the variable with which the interpolation will start (default: try them all, untill one does not crash).
        """
        paramsA = dict([(name[:-1], kwargs.pop(name)) for name in self.variable_names[:self.ndim / 2]])
        paramsB = dict([(name[:-1], kwargs.pop(name)) for name in self.variable_names[self.ndim / 2:]])
        paramsA.update(kwargs)
        paramsB.update(kwargs)
        spectrumA = self.subgrid.get_spectrum(*args, **paramsA)
        spectrumB = self.subgrid.get_spectrum(*args, **paramsB)
        return DoubleLineSpectrum(spectrumA, spectrumB)

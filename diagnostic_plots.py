"""Code from Kevin Covey to make some plots.

The code in spectral_parameters.py can simplify most of this.
"""
#import important packages
import scipy as sp
import matplotlib
import matplotlib.pyplot as plt
import numpy 
import os.path
import pyfits
import glob
import model_spectra
import observed_spectrum
from matplotlib import rc

rc('text', usetex=True)

#close plots from the previous iteration
plt.close('all')

#load the file of spectral fits
specfits = numpy.load('per_star.npy')
mock = numpy.load('mock_spectra.npy')

mock_teffs = [3500, 4000, 4500, 5000, 5500, 6000, 7000]
mock_sn = [5, 10, 20, 30, 50, 70, 100]
mock_vsini = [0, 10, 30, 50, 100]
print specfits.dtype.names

def get_mean(parameter):
    return sp.array([sp.mean(sp.array(values, dtype='f8')) for values in specfits[parameter]])

def get_median(parameter):
    return sp.array([sp.median(sp.array(values, dtype='f8')) for values in specfits[parameter]])

def get_std(parameter):
    return sp.array([sp.std(sp.array([values], dtype='f8'), ddof=1) for values in specfits[parameter]])

#quantities that have one entry PER STAR
vrad = sp.array([sp.median(vr) for vr in specfits['em_vrad']])
vrad_sigma = sp.array([sp.median(vr) for vr in specfits['es_vrad']])
vrad_scatter = sp.array([sp.std([vr], ddof=1) for vr in specfits['em_vrad']])
pipe_RV = sp.array([sp.median(sp.array(vr, dtype='f8')) for vr in specfits['h_VHELIO']])
pipe_RV_sigma = sp.array([sp.std(sp.array([vr], dtype='f8'), ddof=1) for vr in specfits['h_VHELIO']])
pipe_RV_vscatter = sp.array([sp.median(sp.array(vr, dtype='f8')) for vr in specfits['h_VSCATTER']])
vrad_residual = vrad - pipe_RV
RV_sigma_ratio = vrad_sigma / pipe_RV_sigma
pipe_RV_sigma_ratio = pipe_RV_vscatter / pipe_RV_sigma
SNR = sp.array([sp.median([SNR]) for SNR in specfits['h_SNR']])

#identify stars in each cluster
ngc1333 = (specfits['h_RA'] < 54) & (specfits['h_DEC'] > 29.) & ((specfits['h_TARG2'] & 2 ** 13) != 0) 
ic348 = (specfits['h_RA'] > 54) & (specfits['h_RA'] < 60) & (specfits['h_DEC'] > 29.) & ((specfits['h_TARG2'] & 2 ** 13) != 0) 
control = ((specfits['h_TARG2'] & 2 ** 13) == 0) & ((specfits['h_TARG2'] & 2 ** 10) == 0)
ngc2264 = (specfits['h_RA'] > 95) & (specfits['h_DEC'] < 13.) & ((specfits['h_TARG2'] & 2 ** 13) != 0)
pleiades = (specfits['h_RA'] < 60) & (specfits['h_DEC'] < 29.) & ((specfits['h_TARG2'] & 2 ** 10) != 0)

ngc1333_highqual = (specfits['h_RA'] < 54) & (specfits['h_DEC'] > 29.) & ((specfits['h_TARG2'] & 2 ** 13) != 0) & (SNR > 50)
ic348_highqual = (specfits['h_RA'] > 54) & (specfits['h_RA'] < 60) & (specfits['h_DEC'] > 29.) & ((specfits['h_TARG2'] & 2 ** 13) != 0) & (SNR > 50)
control_highqual = (specfits['h_RA'] > 60) & (specfits['h_DEC'] > 29.) & (SNR > 50)
ngc2264_highqual = (specfits['h_RA'] > 95) & (specfits['h_DEC'] < 13.) & ((specfits['h_TARG2'] & 2 ** 13) != 0) & (SNR > 50)
pleiades_highqual = (specfits['h_RA'] < 60) & (specfits['h_DEC'] < 29.) & ((specfits['h_TARG2'] & 2 ** 10) != 0) & (SNR > 50)

cluster_list = [(control, 'giants', 'b'), (ic348, 'IC 348', 'g'), (ngc1333, 'NGC 1333', 'r'), (ngc2264, 'NGC 2264', 'c'), (pleiades, 'Pleiades', 'm')]

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
    axes.plot(specfits[cluster]['h_RA'], specfits[cluster]['h_DEC'], 'o', label=name, ms=8)
    #scatter_object = axes.scatter(specfits['h_RA'], specfits['h_DEC'], c=vrad)
axes.set_xlim(105,51)
axes.legend(loc='best')
axes.set_xlabel('RA (deg.)')
axes.set_ylabel('Dec. (deg.)')
fig.savefig('plots/APOGEE_Fields_RA_Dec_w_RVcolors.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('NGC 1333 ($<$1 Myr)','IC 348 (2-5 Myrs)','Pleiades  (100 Myrs)','Field Giants ($\sim$1 Gyr)'), (ngc1333,ic348, pleiades, control_highqual)):
        axes.plot(specfits[cluster]['em_Teff'], specfits[cluster]['em_logg'], 'o', label=name, ms=8)
axes.set_xlim(7000,3600)
axes.set_ylim(4.9,1.5)
axes.legend(loc='best')
axes.set_xlabel('Temp. (K)')
axes.set_ylabel('log g')
fig.savefig('plots/APOGEE_Fields_Teff_logg.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
        axes.plot(specfits[cluster]['em_vsini'], specfits[cluster]['em_logg'] - specfits[cluster]['a_logg'], 'o', label=name, ms=8)
axes.set_xlim(0,25)
axes.set_ylim(-0.75,1.6)
axes.legend(loc='best')
axes.set_ylabel('$\Delta$ log $g$ (Michiel - ASPCAP)')
axes.set_xlabel('$v$ sin $i$')
fig.savefig('plots/Logg_residuals_vsini.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
        axes.plot(specfits[cluster]['em_vsini'], specfits[cluster]['a_Flogg'], 'o', label=name, ms=8)
axes.set_xlim(0,120)
axes.set_ylim(5,0)
axes.legend(loc='best')
axes.set_ylabel('ASPCAP log $g$')
axes.set_xlabel('$v$ sin $i$')
fig.savefig('plots/Logg_residuals_vsini.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
        axes.plot(SNR[cluster], specfits[cluster]['em_Teff'] - specfits[cluster]['a_FTeff'], 'o', label=name, ms=8)
	axes.set_xlim(1,600)
	#	axes.set_ylim(0.1,6)
	#axes.set_yscale('log')
axes.set_xscale('log')
axes.legend(loc='best')
axes.set_ylabel('$\Delta$ T$_{eff}$ (Michiel - ASPCAP)')
axes.set_xlabel('Median SNR')
fig.savefig('plots/Teff_residuals_SNR.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
        axes.plot(specfits[cluster]['em_Teff'], (specfits[cluster]['em_Teff'] - specfits[cluster]['a_FTeff'])/specfits[cluster]['em_Teff'], 'o', label=name, ms=8)
axes.set_xlim(3500,7000)
axes.set_ylim(-0.5,0.5)
	#axes.set_yscale('log')
        #axes.set_xscale('log')
axes.legend(loc='best')
axes.set_ylabel('Normalized $\Delta$ T$_{eff}$ (Michiel - ASPCAP / Michiel)')
axes.set_xlabel('T$_{eff}$')
fig.savefig('plots/Teff_residuals.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
        axes.plot((specfits[cluster]['em_Teff'] - specfits[cluster]['a_FTeff']),(specfits[cluster]['em_logg'] - specfits[cluster]['a_Flogg']), 'o', label=name, ms=8)
axes.set_ylim(-5,5)
axes.set_xlim(-2500,2500)
	#axes.set_yscale('log')
        #axes.set_xscale('log')
axes.legend(loc='best')
axes.set_ylabel('$\Delta$ log $g$ (Michiel - ASPCAP)')
axes.set_xlabel('$\Delta$ T$_{eff}$ (Michiel - ASPCAP)')
fig.savefig('plots/Delta_Teff_Delta_logg.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_Teff'], vrad[cluster], 'o', label=name, ms=8)
axes.legend(loc='best')
axes.set_xlabel('Temp. (K)')
axes.set_ylabel('RV (km/s)')
fig.savefig('plots/Large_Teff_RV.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
#axes.set_xlim(54., 51.)
axes.set_ylim(0, 30)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_Teff'], vrad[cluster], 'o', label=name, ms=8)
axes.legend(loc='best')
axes.set_xlabel('Temp. (K)')
axes.set_ylabel('RV (km/s)')
fig.savefig('plots/Zoom_Teff_RV.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
axes.set_xlim(3500., 5500)
axes.set_ylim(0, 30)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['a_Teff'], pipe_RV[cluster], 'o', label=name, ms=8)
axes.legend(loc='best')
axes.set_xlabel('ASPCAP Temp. (K)')
axes.set_ylabel('Pipe RV (km/s)')
fig.savefig('plots/Teff_Pipe_RV.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_vsini'], vrad[cluster], 'o', label=name, ms=8)
axes.legend(loc='best')
axes.set_xlabel('v sin i (km/s)')
axes.set_ylabel('RV (km/s)')
fig.savefig('plots/Large_vsini_RV.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
axes.set_xlim(0, 250.)
axes.set_ylim(-160, 160)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_vsini'], vrad[cluster], 'o', label=name)
axes.legend(loc='best')
axes.set_xlabel('v sin i (km/s)')
axes.set_ylabel('RV (km/s)')
fig.savefig('plots/Zoom_vsini_RV.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	plt.hist(vrad_residual[cluster], range = [-10,10], bins = 40, histtype = 'step', label=name)
axes.legend(loc='best')
axes.set_xlabel('RV residuals (Michiel - APOGEE; km/s)')
fig.savefig('plots/RV_residual_hist.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
axes.set_xlim(3, 600.)
axes.set_ylim(0.01, 10.)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(SNR[cluster], abs(vrad_residual[cluster]), 'o', label=name)
        axes.plot(mock_sn, sp.std(mock[1,:,0,:,1]['vrad'], axis=-1, ddof=1), linestyle = 'dashed', linewidth = 1.5, color = 'black')
axes.legend(loc='best')
axes.set_xlabel('SNR')
axes.set_xscale('log')
axes.set_ylabel('RV residuals (km/s)')
axes.set_yscale('log')
fig.savefig('plots/SNR_RV_residuals.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
axes.set_xlim(20, 700.)
axes.set_ylim(0.01, 40.)
for name, cluster in zip(('NGC 1333 ($<$1 Myr)','IC 348 (2-5 Myrs)','Pleiades  (100 Myrs)','Field Giants ($\sim$1 Gyr)'), (ngc1333,ic348, pleiades, control_highqual)):
	axes.plot(SNR[cluster], vrad_scatter[cluster], 'o', label=name)
        axes.plot(mock_sn, sp.std(mock[1,:,0,:,1]['vrad'], axis=-1, ddof=1), linestyle = 'solid', linewidth = 6, color = 'black')
        axes.plot(mock_sn, sp.std(mock[1,:,2,:,1]['vrad'], axis=-1, ddof=1), linestyle = 'solid', linewidth = 6, color = 'black')
        #        axes.plot([1,200],[1,1], linestyle = 'dashed', linewidth = 6, color = '0.75')
axes.legend(loc='upper right')
axes.set_xlabel('SNR')
axes.set_xscale('log')
axes.set_ylabel('RV scatter (km/s)')
axes.set_yscale('log')
#axes.ticklabel_format(style='plain')
#axes.text(125,.025, 'Instrumental RV precision limit for:', fontsize = 15, bbox = dict(facecolor='white', alpha = 1))
axes.text(105,.013, 'RV precision limit for $v$ sin $i$ = 0 km/s', fontsize = 12.5, bbox = dict(facecolor='white', alpha = 1))
axes.text(105,.05, 'RV precision limit for $v$ sin $i$ = 30 km/s', fontsize = 12.5, bbox = dict(facecolor='white', alpha = 1))
#axes.text(250, 0.5, 'typical \underline{astrophysical} precision \n limit for T Tauri star RVs \n from single-epoch NIR \n spectra (see Fig. 3)', fontsize = 13, bbox = dict(facecolor='0.75', alpha = 0.85), multialignment = 'left')
fig.savefig('plots/SNR_RV_scatter.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
axes.set_xlim(0.01, 40.)
axes.set_ylim(0.01, 40.)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(pipe_RV_sigma[cluster], vrad_scatter[cluster], 'o', label=name)
axes.legend(loc='best')
axes.set_xlabel('Pipe RV scatter (km/s)')
axes.set_xscale('log')
axes.set_ylabel('RV scatter (km/s)')
axes.set_yscale('log')
fig.savefig('plots/RV_scatter_both_pipelines.pdf')


fig = plt.figure()
axes = fig.add_subplot(111)
axes.set_xlim(0, 100.)
axes.set_ylim(0.01, 40.)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_vsini'], vrad_scatter[cluster], 'o', label=name)
        axes.plot(mock_vsini, sp.std(mock[1,-3,:,:,1]['vrad'], axis=-1, ddof=1), linestyle = 'dashed', linewidth = 1.5, color = 'black')
axes.legend(loc='best')
axes.set_xlabel('$v$ sin $i$ (km/s)')
axes.set_ylabel('RV scatter (km/s)')
axes.set_yscale('log')
fig.savefig('plots/vsini_RV_scatter.pdf')

ic348_lowvsini = (specfits['h_RA'] > 54) & (specfits['h_RA'] < 60) & (specfits['h_DEC'] > 29.) & ((specfits['h_TARG2'] & 2 ** 13) != 0 ) & (specfits['em_vsini'] < 13) 


fig = plt.figure()
axes = fig.add_subplot(111)
plt.hist(vrad_scatter[ic348], range = [0,5], bins = 20, histtype = 'step', label='IC 348', color = 'purple', linewidth = 2)
plt.hist(vrad_scatter[ic348_highqual], range = [0,5], bins = 20, histtype = 'step', label='IC 348 (SNR > 50)', color = 'blue', linestyle = 'dashed', linewidth = 2)
plt.hist(vrad_scatter[ic348_lowvsini], range = [0,5], bins = 20, histtype = 'step', label='IC 348 (vsini < 13)', color = 'red', linestyle = 'dashed', linewidth = 2)
axes.legend(loc='best')
axes.set_xlabel('RV scatter (km/s)')
axes.set_ylim(0.5,110)
axes.set_yscale('log')
fig.savefig('plots/IC348_RVscatter_hist.pdf')


fig = plt.figure()
axes = fig.add_subplot(111)

for name, cluster, this_color in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades, ngc1333,ic348), ('blue','green','red','cyan','purple')):
	plt.hist(vrad[cluster], range = [-5,35], bins = 65, histtype = 'step', label=name, color=this_color, linewidth = 1.5)

        #for name, cluster, this_color in zip(('control (SNR > 50)', 'NGC 2264 (SNR > 50)', 'Pleiades (SNR > 50)','NGC 1333 (SNR > 50)','IC 348 (SNR > 50)'), (control_highqual, ngc2264_highqual, pleiades_highqual, ngc1333_highqual, ic348_highqual), ('blue','green','red','cyan','purple')):
	#plt.hist(vrad[cluster], range = [-10,45], bins = 65, histtype = 'step', label=name, linestyle = 'dashed', color=this_color, linewidth = 1.5)
axes.legend(loc='best')
axes.set_xlabel('RV (Michiel; km/s)')
fig.savefig('plots/Michiel_Cluster_RV_hists.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
for name, cluster, this_color in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades, ngc1333,ic348), ('blue','green','red','cyan','purple')):
	plt.hist(pipe_RV[cluster], range = [-10,45], bins = 65, histtype = 'step', label=name, color=this_color, linewidth = 1.5)

for name, cluster, this_color in zip(('control (SNR > 50)', 'NGC 2264 (SNR > 50)', 'Pleiades (SNR > 50)','NGC 1333 (SNR > 50)','IC 348 (SNR > 50)'), (control_highqual, ngc2264_highqual, pleiades_highqual, ngc1333_highqual, ic348_highqual), ('blue','green','red','cyan','purple')):
	plt.hist(pipe_RV[cluster], range = [-10,45], bins = 65, histtype = 'step', label=name, linestyle = 'dashed', color=this_color, linewidth = 1.5)
axes.legend(loc='best')
axes.set_xlabel('RV (RV Pipeline; km/s)')
fig.savefig('plots/Pipe_Cluster_RV_hists.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
#axes.set_xlim(0, 250.)
axes.set_ylim(-0.75, 1.20)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_Teff'], vrad_residual[cluster], 'o', label=name)
axes.legend(loc='best')
axes.set_xlabel('Teff (K)')
axes.set_ylabel('RV residuals (Michiel - APOGEE; km/s)')
fig.savefig('plots/RV_residuals_Teff.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
#axes.set_xlim(0, 250.)
axes.set_ylim(-1.5, 1.5)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_Teff'], vrad_residual[cluster], 'o', label=name)
axes.legend(loc='best')
axes.set_xlabel('Teff (K)')
axes.set_ylabel('RV residuals (Michiel - APOGEE; km/s)')
fig.savefig('plots/Zoom_RV_residuals_Teff.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
#axes.set_xlim(0, 20.)
axes.set_ylim(-30, 30)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_Teff'], vrad_residual[cluster]/vrad_sigma[cluster], 'o', label=name)
	print name, specfits[cluster].size
axes.legend(loc='best')
axes.set_xlabel('Teff (K)')
axes.set_ylabel('RV residuals ( (Michiel - APOGEE) / Michiel Sigma)')
fig.savefig('plots/Zoom_Norm_RV_residuals_Teff.pdf')

fig = plt.figure()
axes = fig.add_subplot(111)
axes.set_xlim(0, 20.)
axes.set_ylim(-1.5, 1.5)
for name, cluster in zip(('control', 'NGC 2264', 'Pleiades','NGC 1333','IC 348',), (control, ngc2264, pleiades,ngc1333,ic348)):
	axes.plot(specfits[cluster]['em_vsini'], vrad_residual[cluster], 'o', label=name)
axes.legend(loc='best')
axes.set_xlabel('vsini (km/s)')
axes.set_ylabel('RV residuals (Michiel - APOGEE; km/s)')
fig.savefig('plots/Zoom_RV_residuals_vsini.pdf')


fig, axes = plt.subplots(1, 3, figsize=(12, 5))
for cluster, name, color in cluster_list:
    axes[0].plot(specfits[cluster]['em_Teff'], specfits[cluster]['em_logg'], color + '.', label=name)
    axes[0].set_title('IN-SYNC pipeline')
    axes[1].plot(specfits['a_Teff'][cluster], specfits['a_logg'][cluster], color + '.', label=name)
    axes[1].set_title('ASPCAP pipeline (calibrated)')
    axes[2].plot(specfits['a_FTeff'][cluster], specfits['a_Flogg'][cluster], color + '.', label=name)
    axes[2].set_title('ASPCAP pipeline (uncalibrated))')
try:
    from photometry import isochrone
    siess_iso = isochrone.siess_isochrone()
    mass_arr = sp.unique(siess_iso['mass'])
    for age, color in [(5, 'y'), (8, 'k'), (100, 'Orange')]:
        Teff = []
        logg = []
        for mass in mass_arr:
            subset_iso = siess_iso[siess_iso['mass'] == mass]
            Teff.append(sp.interp(sp.log10(age * 1e6), sp.log10(subset_iso['age']), subset_iso['Teff']))
            logg.append(sp.interp(sp.log10(age * 1e6), sp.log10(subset_iso['age']), subset_iso['logg']))
        for ax in axes:
            ax.plot(Teff, logg, '-', color=color, label='Siess %i Myr isochrone' % age)
    marigo_iso = isochrone.marigo_isochrone()
    post_ms = marigo_iso[marigo_iso['age'] == 10 ** 9.7]
    for ax in axes:
        ax.plot(post_ms['Teff'], post_ms['logg'], '-', color = 'Sienna', label='Padova 5 Gyr isochrone')
except ImportError:
    print 'isochrones not found'
for ax in axes:
    ax.legend(loc='upper left')
    ax.set_xlabel('T$_{\mathrm{eff}}$')
    ax.set_ylabel('log$g$')
    ax.set_xlim(7000, 3400)
    ax.set_ylim(5.1,1.5)
fig.tight_layout()
fig.savefig('plots/HRD_comparison_own_ASPCAP.pdf')

fig, axes = plt.subplots()
axes.plot(specfits['a_FTeff'], specfits['em_Teff'], '.', label='giants')
axes.plot(specfits['a_FTeff'][~control], specfits['em_Teff'][~control], '.', label='(pre-) main sequence')
badfits = (get_mean('em_vrad') <= -800) & (specfits['em_vsini'] > 500)
axes.plot(specfits['a_FTeff'][badfits], specfits['em_Teff'][badfits], 'r.', label='bad fits')
axes.plot([3500, 7000], [3500, 7000], 'k-')
axes.legend(loc='upper right')
axes.set_xlabel(r'T$_{\mathrm{eff}}$ (uncalibrated ASPCAP)')
axes.set_ylabel(r'T$_{\mathrm{eff}}$ (IN-SYNC)')
fig.savefig('plots/Teff_comparison_badfits.pdf')

snr = get_median('h_SNR')
snr[~sp.isfinite(snr)] = 5
fig, axes = plt.subplots()
mappable = axes.scatter(specfits['em_Teff'], get_std('em_vrad'), c=sp.log10(snr))
axes.set_yscale('log')
axes.set_xlabel(r'T$_{\mathrm{eff}}$ (K)')
axes.set_ylabel(r'scatter in RV (km/s)')
axes.set_ylim(1e-2, 1e4)
fig.colorbar(mappable).set_label('SNR')
fig.savefig('plots/vscatter_Teff_perSNR.pdf')

fig, axes = plt.subplots()
for cluster, name, color in cluster_list:
    axes.plot(specfits['em_Teff'][cluster], get_std('em_vrad')[cluster], color + 'o', label=name)
axes.set_yscale('log')
axes.legend(loc='upper right')
axes.set_xlabel(r'T$_{\mathrm{eff}}$ (K)')
axes.set_ylabel(r'scatter in RV (km/s)')
axes.set_ylim(1e-2, 1e4)
fig.savefig('plots/vscatter_Teff_percluster.pdf')

fig, axes = plt.subplots()
RV_M09 = specfits['l_M09_RV'].copy()
RV_M09[~(specfits['l_M09'])] = sp.nan
axes.plot(specfits['em_Teff'], get_median('em_vrad') - RV_M09, '.', label='IN-SYNC RV offset')
axes.plot(specfits['em_Teff'], get_median('h_VHELIO') - RV_M09, '.', label='ASPCAP RV offset')
axes.set_xlabel(r'T$_{\mathrm{eff}}$ (K)')
axes.set_ylabel(r'Offset in RV with respect to Mermilliod et al. (2009)')
axes.legend(loc='best')
axes.set_ylim(-10, 10)
fig.savefig('plots/RVoffset_with_Mermilliod2009.pdf')

fig, axes = plt.subplots()
for RV_full, color, label, Nmedian in [(get_median('em_vrad'), 'b', 'IN-SYNC', 50), (get_median('h_VHELIO'), 'r', 'RV pipeline', 50),
                              (RV_M09, 'm', 'Mermilliod et al. (2009)', 20)]:
    Teff = specfits[pleiades]['em_Teff']
    rv = RV_full[pleiades]
    axes.plot(Teff, rv, color + '.', label='%s (%i stars)' % (label, sp.sum(sp.isfinite(rv))))
    sorter = sp.argsort(Teff)
    sorter = sorter[sp.isfinite(rv[sorter])]
    import scipy.ndimage.filters
    filtered_rv = sp.ndimage.filters.median_filter(rv[sorter], Nmedian)
    axes.plot(Teff[sorter], filtered_rv, color + '-', label='%s median filtered (N=%i)' % (label, Nmedian))
axes.set_xlabel(r'T$_{\mathrm{eff}}$ (K)')
axes.set_ylabel(r'radial velocity for Pleiades star')
axes.set_ylim(3, 8)
axes.legend(loc='lower right')
fig.savefig('plots/Pleiades_RV_comprison.pdf')


fig, axes = plt.subplots()
RV_F06 = specfits['l_F06_HRV1'].copy()
RV_F06[~(specfits['l_F06'])] = sp.nan
for RV_full, color, label, Nmedian in [(get_median('em_vrad'), 'b', 'IN-SYNC', 30), (get_median('h_VHELIO'), 'r', 'RV pipeline', 30),
                              (RV_F06, 'm', 'Furesz et al. (2006)', 20)]:
    Teff = specfits[ngc2264]['em_Teff']
    rv = RV_full[ngc2264]
    axes.plot(Teff, rv, color + '.', label='%s (%i stars)' % (label, sp.sum(sp.isfinite(rv))))
    sorter = sp.argsort(Teff)
    sorter = sorter[sp.isfinite(rv[sorter])]
    import scipy.ndimage.filters
    filtered_rv = sp.ndimage.filters.median_filter(rv[sorter], Nmedian)
    axes.plot(Teff[sorter], filtered_rv, color + '-', label='%s median filtered (N=%i)' % (label, Nmedian))
axes.set_xlabel(r'T$_{\mathrm{eff}}$ (K)')
axes.set_ylabel(r'radial velocity for NGC 2264 star')
axes.set_ylim(15, 30)
axes.legend(loc='lower right')
fig.savefig('plots/NGC2264_RV_comprison.pdf')

fig, axes = plt.subplots()
axes.plot(RV_F06, get_median('h_VHELIO') - RV_F06, '.', label='RV pipeline')
axes.plot(RV_F06, get_median('em_vrad') - RV_F06, '.', label='IN-SYNC')
axes.set_xlabel('Radial velocity (Furesz et al. 2006)')
axes.set_ylabel('RV (APOGEE) - RV (Furesz et al. 2006)')
axes.legend(loc='lower left')
axes.set_ylim(-10, 10)
axes.set_xlim(14, 28)
fig.savefig('plots/Furesz06_RVprecision.pdf')

fig, axes = plt.subplots()
axes.plot(specfits['em_Teff'], get_median('h_VHELIO') - RV_F06, '.', label='RV pipeline')
axes.plot(specfits['em_Teff'], get_median('em_vrad') - RV_F06, '.', label='IN-SYNC')
axes.set_xlabel('Effective temperature (IN-SYNC)')
axes.set_ylabel('RV (APOGEE) - RV (Furesz et al. 2006)')
axes.legend(loc='lower left')
fig.savefig('plots/RVoffset_with_Furesz06.pdf')

#fig = plt.figure()
#axes = fig.add_subplot(111)
#axes.set_xlim(0, 20.)
#axes.set_ylim(-1.5, 1.5)
#for name, cluster in zip(('IC 348', 'NGC 1333', 'control', 'NGC 2264', 'Pleiades'), (ic348, ngc1333, control, ngc2264, pleiades)):
#	axes.plot(specfits[cluster]['em_vsini'], vrad_residual[cluster]/reduce(specfits[cluster]['es_vrad']), 'o', label=name)
#axes.legend(loc='best')
#axes.set_xlabel('vsini (km/s)')
#axes.set_ylabel('RV residuals (Michiel - APOGEE; Sigma)')
#fig.savefig('plots/Zoom_Norm_RV_residuals_vsini.pdf')


#vrad_residual = vrad - pipe_RV


#vrad = sp.array([sp.median(vr) for vr in specfits['em_vrad']])
#vrad_sigma = sp.array([sp.std([vr]) for vr in specfits['em_vrad']])
#pipe_RV = sp.array([sp.median(vr) for vr in specfits['h_VHELIO']])
#pipe_RV_sigma = sp.array([sp.std([vr]) for vr in specfits['h_VHELIO']])
#pipe_RV_vscatter = sp.array([sp.median([vr]) for vr in specfits['h_VSCATTER']])
#R = [sp.median(SNR) for SNR in specfits['h_SNR']]
#vrad_residual = vrad - pipe_RV
#RV_sigma_ratio = vrad_sigma / pipe_RV_sigma
#pipe_RV_sigma_ratio = pipe_RV_vscatter / pipe_RV_sigma
#Teff = [sp.median(teff) for teff in specfits['em_Teff']]
#Teff_sigma = [sp.median(teff) for teff in specfits['es_Teff']]
#logg = [sp.median(logg) for logg in specfits['em_logg']]
#logg_sigma = [sp.median(logg) for logg in specfits['es_logg']]
#vsini = [sp.median(vsini) for vsini in specfits['em_vsini']]
#vsini_sigma = [sp.median(vsini) for vsini in specfits['es_vsini']]
#veiling = [sp.median(veiling) for veiling in specfits['em_veiling']]
#veiling_sigma = [sp.median(veiling) for veiling in specfits['es_veiling']]





#convert quantities that have one entry PER STAR into quantities that have one entry for EACH EPOCH

ra_all = specfits['h_RA'].repeat(specfits['nepochs'])
dec_all = specfits['h_DEC'].repeat(specfits['nepochs'])
Teff_all = specfits['em_Teff'].repeat(specfits['nepochs'])
Teff_sigma_all = specfits['es_Teff'].repeat(specfits['nepochs'])
logg_all = specfits['em_logg'].repeat(specfits['nepochs'])
logg_sigma_all = specfits['es_logg'].repeat(specfits['nepochs'])
vsini_all = specfits['em_vsini'].repeat(specfits['nepochs'])
vsini_sigma_all = specfits['es_vsini'].repeat(specfits['nepochs'])


#for the original per-epoch quantities, like vr, should be fine using the reduce method.
vrad_all = reduce(sp.append, specfits['em_vrad'])
print vrad_all.size
print ra_all.size


#vrad_sigma = sp.array([sp.std([vr]) for vr in specfits['em_vrad']])
#pipe_RV = sp.array([sp.median(vr) for vr in specfits['h_VHELIO']])
#pipe_RV_sigma = sp.array([sp.std([vr]) for vr in specfits['h_VHELIO']])
#pipe_RV_vscatter = sp.array([sp.median([vr]) for vr in specfits['h_VSCATTER']])
#vrad_residual = vrad - pipe_RV
#RV_sigma_ratio = vrad_sigma / pipe_RV_sigma
#pipe_RV_sigma_ratio = pipe_RV_vscatter / pipe_RV_sigma
#Teff = [sp.median(teff) for teff in specfits['em_Teff']]
#logg = [sp.median(logg) for logg in specfits['em_logg']]
#vsini = [sp.median(vsini) for vsini in specfits['em_vsini']]



#identify clusters in EPOCH space
ngc1333_all = (ra_all < 54) & (dec_all > 29.)
ic348_all = (ra_all > 54) & (ra_all < 60) & (dec_all > 29.)
control_all = (ra_all > 60) & (dec_all > 29.)
ngc2264_all = (ra_all > 95) & (dec_all < 13.)
pleiades_all = (ra_all < 60) & (dec_all < 29.)


        #    cluster_vrad = [sp.median(vr) for vr in specfits[cluster]['em_vrad']]
        #cluster_Teff = [sp.median(teff) for teff in specfits[cluster]['em_Teff']]
        #cluster_logg = [sp.median(logg) for logg in specfits[cluster]['em_logg']]
        #cluster_vsini = [sp.median(vsini) for vsini in specfits[cluster]['em_vsini']]

    #axes.plot(cluster['h_RA'], cluster['h_DEC'], 'o', label=name)
    #    scatter_object = axes.scatter(cluster_Teff, cluster_logg, 'o', label=name)
#axes.set_xlim(54., 51.)
#axes.set_ylim(29.75, 33.)
#plt.colorbar(scatter_object)
#fig.savefig('Teff_vs_logg.pdf')


#ax = plt.axes()

#ax.set_xlim()
#ax.scatter??

#numpy.load('fit_summary.npy')
# arr=_
#arr.dtype
#arr.dtype.names
#arr['em_vrad']
#--( em = median; es = sigma; h_ = header
#arr['em_Teff']
#arr.dtype
#arr['h_RA']
#arr['h_DEC']
# arr['h_DEC'][-39]
#arr[-39]['h_PLATE']
#plt.scatter([99,99,99],[99,99,0])
# plt.scatter?
#plt.scatter??
#ax = plt.axes()
#ax.scatter??
#dec = arr['h_DEC']
#dec[-39] = dec[-39][0]
#dec.astype('float')
# plt.scatter(arr['h_RA'], dec.astype('float'))
#plt.clf()
#plt.scatter(arr['h_RA'], dec.astype('float'))
#arr['h_Teff']
#arr['h_VHELIO']

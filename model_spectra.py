"""Module used to load in and modify theoretical spectra.

The option to read in four spectral libraries is currently provided:
- `coelho`: Reads in the Coelho spectral grid.
- `apogee`: Reads in the Apogee spectral grid.
- `btsettl_7` or `btsettl_hdf5`: Reads in the BT-Settl spectral grid from two different file types
- `phoenix`: Phoenix grid
See the documentation of these functions on where to find the data.

Alternatively a previously saved spectral grid can also be loaded:
- `pre_loaded`: loads in a spectral grid previously saved in a numpy file

This module also defines two classes:
- `TheoreticalSpectrumGrid`: represents a grid of model spectra, which can be interpolated with TheoreticalSpectrumGrid.get_spectrum(Teff=..., logg=..., ...) to give a `TheoreticalSpectrum`.
- `TheoreticalSpectrum`: single theoretical spectrum, which can be rotationally broadened, have its resolution degraded, shifted with an RV, etc. Note that all these conversions return a new TheoreticalSpectrum, rather than changing the old one.

Example usage to get a spectrum at some Teff, logg, metallicity and to degrade the spectrum to APOGEE resolution:
# load in a grid
>>> grid = model_spectra.apogee(path_to_apogee_fits_file) # or grid = model_spectra.pre_loaded(path_to_saved_btsettl_file)
# list the variables on which the grid depends
>>> print grid.variable_names
['Teff', 'logg', 'Z', 'alpha', 'C']
# Reduce the size of the array by setting alpha=0, C=0 (note that setting Z=0 is not a good idea as the hot stars have not been computed for Z=0, but for Z=-0.5 and Z=0.5).
>>> reduced_grid = grid.at_value('alpha', 0).at_value('C', 0)
>>> print reduced_grid.variable_names
['Teff', 'logg', 'Z']
# cubic interpolate to a given spectrum
>>> spectrum = reduced_grid.get_spectrum(Teff=4000., logg=3.7, Z=0.)
# From the original grid this spectrum could be retrieved by:
>>> spectrum = grid.get_spectrum(Teff=4000., logg=3.7, Z=0, C=0, alpha=0)
# Convolve the spectrum to APOGEE resolution and shift with 10 km/s
>>> new_spec = spectrum.convert(resolution=22500, vrad=10.)
# plot the spectrum using matplotlib.pyplot
>>> import matplotlib.pyplot as plt
>>> plt.plot(new_spec.wavelength, new_spec.flux)
# Optional in non-interactive mode:
>>> plt.show()
"""
import scipy as sp
import os
import glob
import re

grid_names = ('coelho', 'RVgrid', 'btsettl', 'phoenix')

def coelho(directory_name, normalized=True):
    """Returns a grid containing the Coelho spectra library.

    The spectra can be downloaded from http://www.mpa-garching.mpg.de/PUBLICATIONS/DATA/SYNTHSTELLIB/synthetic_stellar_spectra.html
    The spectra range from 0.3 to 1.3 micron with steps of 0.03 A (sampling resolution in H-band of 400k).
    Many hydrogen lines are missing from this spectrum.

    The pyfits module is needed to load this spectral library.

    Note: An updated, but unnormalized and too low resolution spectral library is available on http://sites.cruzeirodosulvirtual.com.br/nat/modelos.html .

    Arguments:
    - `directory_name`: string giving the directory containing the Coelho spectra.
    - `normalized`: If True load the normalized spectra, otherwise the flux-calibrated spectra are loaded (default: True)
    """
    import pyfits
    variables = [(name, []) for name in ('Teff', 'logg', 'FeH', 'alpha')]
    spectra = []
    for filename in glob.glob(os.path.join(directory_name, '*.ms.fits*')):
        final_part = os.path.split(filename)[-1]
        match = re.search('([0-9]*)_([0-9]*)_([pm])([0-9]*)p([0-9]*).ms.fits.*', final_part)
        if match != None:
            values = {'Teff': float(match.group(1)), 'logg': float(match.group(2)) / 10.,
                      'FeH': float(match.group(4)) / 10., 'alpha': float(match.group(5)) / 10.}
            if match.group(3) == 'm':
                values['FeH'] = -values['FeH']
            for name, var in variables:
                var.append(values[name])
            spectra.append(CoelhoFile(filename))
            spectra[-1].normalized_spectrum = normalized
    if len(spectra) == 0:
        raise IOError('Coelho grid not found in %s' % directory_name)
    return TheoreticalSpectrumGrid(spectra, variables).at_value('alpha', 0.)


def RVgrid(filename=os.path.join(os.path.dirname(__file__), 'apg_rvsynthgrid_v2.fits')):
    """Returns a grid containing the APOGEE spectral grid from the RV APOGEE pipeline

    The fits-file containing the spectral library can be downloaded from
    https://trac.sdss3.org/browser/repo/apogee/apogeereduce/trunk/lib/synthgrid

    The pyfits module is needed to load this spectral library.

    parameters:
    - `filename`: string giving the fits-file containing the APOGEE spectra.
    """
    import pyfits
    if not os.path.isfile(filename):
        raise IOError('%s is not a file' % filename)
    with pyfits.open(filename) as hrd:
        start = hrd[1].header['CRVAL2']
        npoints = hrd[1].header['NAXIS2']
        delta = hrd[1].header['CDELT2']
        wavelength = 10 ** sp.arange(start, start + npoints * delta, delta)
        spectra = [TheoreticalSpectrum(wavelength, sp.squeeze(flux), 22500., ['loaded in from RV APOGEE grid']) for flux in sp.array(hrd[1].data).T]
        variables = [(name1, sp.squeeze(hrd[2].data.field(name2))) for name1, name2 in [('Teff', 'TEFF'), ('logg', 'LOGG'), ('FeH', 'METALS'), ('alpha', 'ALPHA'), ('carbon', 'CARBON')]]
    return TheoreticalSpectrumGrid(spectra, variables)


def btsettl_7(directory_name):
    """Returns a grid containing the BT-Settl spectral libarary from the *.7.gz files.

    These spectra can be downloaded from  http://phoenix.ens-lyon.fr/Grids/BT-Settl/CIFIST2011/SPECTRA/ .

    Arguments:
    `directory_name`: string giving the directory, where the hdf5 files containing the BT-Settl spectra (can be downloaded from Allard's web page).
    """
    var_names = ('Teff', 'logg', 'FeH', 'alpha')
    variables = [(name, []) for name in var_names]
    spectra = []
    for filename in glob.glob(os.path.join(directory_name, 'lte*.BT-Settl.spec.7.bz2')):
        match = re.match('.*lte(?P<Teff>[0-9.]*)(?P<logg>[+-]{1}.{3})(?P<FeH>[+-].{3})a(?P<alpha>.{4})', filename)
        for ixkey, key in enumerate(var_names):
            variables[ixkey][1].append(float(match.groupdict()[key]))
            if key == 'logg':
                variables[ixkey][1][-1] = -variables[ixkey][1][-1]
            if key == 'Teff':
                variables[ixkey][1][-1] = 100. * variables[ixkey][1][-1]
        spectra.append(BTSettlFile7(filename))
    return TheoreticalSpectrumGrid(spectra, variables)

def btsettl_hdf5(directory_name):
    """Returns a grid containing the BT-Settl spectral library.

    The HDF5 files containing the spectra can be downloaded from http://phoenix.ens-lyon.fr/Grids/BT-Settl/AGSS2009/BIN_SPECTRA/

    The pytables module is needed to load this spectral library (to read the hdf5 files).

    Arguments:
    `directory_name`: string giving the directory, where the hdf5 files containing the BT-Settl spectra (can be downloaded from Allard's web page).
    """
    variables = [(name, []) for name in ('Teff', 'logg', 'alpha')]
    spectra = []
    try:
        import tables
    except:
        raise ImportError('Failed to load module `pytables`, needed to load the BT-Settl HDF5 data')
    for filename in glob.glob(os.path.join(directory_name, '*.BT-Settl.h5')):
        hdf = tables.openFile(filename)
        for name1, name2, value in sp.array(hdf.getNode('/Atmosphere/param')):
            for name, var in variables:
                if name == name1:
                    var.append(value)
        spectra.append(BTSettlFileHDF5(filename))
        hdf.close()
    if len(spectra) == 0:
        raise IOError('BTSettl grid not found in %s' % directory_name)
    return TheoreticalSpectrumGrid(spectra, variables).at_value('alpha', 0.).at_value('FeH', 0.)


def phoenix(directory_name):
    """Returns a grid containing the Phoenix spectral library.

    This grid can be downloaded from http://phoenix.astro.physik.uni-goettingen.de/index.php?page=medres

    - `directory_name`: string giving the directory, where the (unzipped) fits-files are stored.
    """
    variables = [(name, []) for name in ('Teff', 'logg')]
    spectra = []
    try:
        import pyfits
    except ImportError:
        import astropy.io.fits as pyfits
    for filename in glob.glob(os.path.join(directory_name, 'lte*2011-HiRes.fits')):
        spectra.append(PhoenixFile(filename))
        spectra[-1].wavelength_file = os.path.join(directory_name, 'WAVE_PHOENIX-ACES-AGSS-COND-2011.fits')
        with pyfits.open(filename) as hrd:
            for name, var_list in variables:
                var_list.append(hrd[0].header['phx' + name])
    if len(spectra) == 0:
        raise IOError('Phoenix grid not found in %s' % directory_name)
    return TheoreticalSpectrumGrid(spectra, variables)


def pre_loaded(filename):
    """Loads a previously saved model spectrum grid from a numpy .npz file

    Any spectra should be stored in an array under "spectra" with their resolution under "resolution".
    Any variables should be stored in arrays with similar sizes under their variable names.
    """
    specdict = dict(sp.load(filename))
    spectra = [spec.view(TheoreticalSpectrum) for spec in specdict.pop('spectra')]
    resolution = sp.atleast_1d(specdict.pop('resolution', sp.inf))
    if resolution.size == 1:
        resolution = sp.zeros(len(spectra)) + resolution[0]
    assert len(resolution) == len(spectra), "resolution in saved grid of model spectra should contain either one value or an array-like list of resolutions for all spectra"
    for spec, res in zip(spectra, resolution):
        spec._resolution = res
    return TheoreticalSpectrumGrid(spectra, specdict.items())


class SpectralReader(object):
    """Base class for objects that read in a spectrum, when needed.

    Used to represent an available spectrum in the model grid, which has not been read in yet."""
    def __init__(self, filename=None):
        self.filename = filename

    def read(self, ):
        """Overwrite with a method that reads in and returns the spectrum"""
        raise NotImplementedError('Reading in of unloaded spectra has not been implemented for type:', type(self))


class CoelhoFile(SpectralReader):
    """An unread Coelho spectrum.

    Used to represent an available spectrum in the Coelho model grid, which has not been read in to save memory"""

    normalized_spectrum = True

    def read(self, ):
        """Reads in and returns the Coelho spectrum"""
        import pyfits
        with pyfits.open(self.filename) as hrd:
            flux = sp.array(hrd[0].data)[not self.normalized_spectrum, :]
            return TheoreticalSpectrum(airtovac(hrd[0].header['CRVAL1'] + hrd[0].header['CDELT1'] * sp.arange(len(flux))),
                                       flux, sp.infty, ['loaded in from Coelho grid'])


class BTSettlFileHDF5(SpectralReader):
    """An unread BT-Settl spectrum from an HDF5 file.

    Used to represent an available spectrum in the BT-Settl model grid, which has not been read in to save memory"""
    def read(self, ):
        """Reads in and returns the BT-Settl spectrum"""
        import tables
        hdf = tables.openFile(self.filename)
        flux = sp.array(hdf.getNode('/Spectrum/flux'))
        wave_edges = sp.array(hdf.getNode('/Spectrum/cmtber'))
        wave_step = sp.array(hdf.getNode('/Spectrum/cmtdis'))
        nsteps = sp.sum(wave_step != 0.)
        arr = sp.zeros(0.)
        try:
            wavelength = sp.array(hdf.getNode('/Spectrum/wl'))
        except tables.NoSuchNodeError:
            for wave_start, wave_step, wave_end in zip(wave_edges[:nsteps], wave_step[:nsteps], wave_edges[1:nsteps + 1]):
                arr = sp.append(arr, sp.arange(wave_start, wave_end, wave_step))
            wavelength = sp.append(arr, [wave_edges[nsteps]])
        hdf.close()
        return TheoreticalSpectrum(wavelength, flux, sp.infty, ['loaded in from BT-Settl hdf5 file'])


class BTSettlFile7(SpectralReader):
    """An unread BT-Settle spectrum from a *.7.bz2 file.

    Used to represent an available spectrum in the BT-Settl model grid, which has not been read in to save memory"""
    def read(self, ):
        """Reads in and returns the BT-Settl spectrum"""
        converters = dict([(ix, lambda s:float(s.replace('D', 'E'))) for ix in (1, 2, 15, 16, 17, 19, 20, 21)])
        arr = sp.genfromtxt(self.filename, delimiter=(13, 14, 13, 4, 6, 9, 5, 7, 11, 2, 9, 10, 2, 5, 5, 13, 13, 15, 4, 13, 13, 13),
                            dtype='f8', converters=converters)
        return TheoreticalSpectrum(arr[:, 0], 10 ** (arr[:, 1] + 8.), sp.infty, ['loaded in from BT-Settl .7.bz2 file'])

class PhoenixFile(SpectralReader):
    """An unread Phoenix spectrum.

    Used to represent an available spectrum in the Phoenix model grid, which has not been read in to save memory"""
    def read(self, ):
        """Reads in and returns the Phoenix spectrum"""
        try:
            import pyfits
        except ImportError:
            import astropy.io.fits as pyfits
        flux = sp.array(pyfits.getdata(self.filename, 0))
        wavelength = sp.array(pyfits.getdata(self.wavelength_file, 0))
        return TheoreticalSpectrum(wavelength, flux, sp.infty, ['loaded in from Phoenix grid'])

class GridError(ValueError):
    """Error raised when the spectrum grid can not be interpolated"""


class TheoreticalSpectrum(sp.recarray):
    """A model spectrum at a given resolution.

    The spectrum can be convolved, shifted, and interpolated.
    Its wavelength and flux array can also be accessed directly.

    Properties:
    - `wavelength: Array giving the wavelength of the spectrum.
    - `flux`: Array giving the flux of the spectrum.
    - `resolution`: Current resolution of the spectrum.
    - `history`: list of previous alterations of the spectrum.

    Methods:
    - `convert`: Returns a new spectrum, which has been convolved, shifted, and/or interpolated.
    `convert` calls the other methods: `resolution`, `rotate`, `shift`, and `interpolation`.
    """
    _resolution = sp.infty
    def __new__(cls, wavelength, flux, resolution, history=None):
        arr = sp.zeros(wavelength.shape, dtype=[('wavelength', 'f8'), ('flux', 'f8')])
        arr['wavelength'] = wavelength
        arr['flux'] = flux
        obj = arr.view(cls)
        obj._resolution = resolution
        if history is not None:
            obj.history = history
        return obj


    def __array_finalize__(self, obj):
        """Called when a new ApogeeSpectrum object is created.

        If the new object has an ApogeeSpectrum has a template, copy the meta-data.
        """
        if isinstance(obj, TheoreticalSpectrum):
            self.history = obj.history
            self._resolution = obj.resolution
        else:
            self.history = []

    @property
    def resolution(self, ):
        """Returns the current resolution of the spectrum."""
        return self._resolution

    def shift(self, vrad):
        """Returns a new spectrum, which is shifted with radial velocity `vrad` in km/s"""
        #the speed of light = 299 792.458 kilometers / second
        return TheoreticalSpectrum(self.wavelength * (1 + vrad / 299792.458), self.flux, self.resolution, history=self.history + ['Shifted with velocity %f' % vrad])

    def convolve(self, function, message='Convolved spectrum'):
        """Convolves spectrum with unnormalized `function`.

        This convolution can handle any wavelength array, but due to this it is very slow.
        It might be better to simply assume an wavelength array with a constant step in log-space which will allow for a faster convolution, as implemented for TheoreticalSpectrum.rotate and TheoreticalSpectrum.resolution."""
        rel_dist = 1 - self.wavelength[None, :] / self.wavelength[:, None]
        contribution = function(rel_dist)
        contribution_normed = contribution / sp.sum(contribution, 0)[:, None]
        new_flux = sp.sum(contribution_normed * self.flux[:, None], 0)
        return TheoreticalSpectrum(self.wavelength, new_flux, self.resolution, history=self.history + [message])

    def rotate(self, vsini, epsilon=0.6):
        """Returns a new spectrum, which broadened due to a projected rotational velocity `vsini` in km/s

        This should match the observed rotational velocity

        WARNING: Assumes a constant sampling resolution."""
        vsini = abs(vsini)
        # sampling resolution
        sample_res = abs(sp.median((self.wavelength[1:] + self.wavelength[:-1]) / (self.wavelength[1:] - self.wavelength[:-1])) / 2.)

        # number of pixels within vsin(i) of each other
        steps = sample_res * vsini / 299792.458 + 1e-8
        oversample = int(sp.ceil(30. / steps))
        if oversample > 1000 or vsini == 0.:
            return TheoreticalSpectrum(self.wavelength, self.flux, self.resolution,
                    history=self.history + ['v sin(i) is approximately zero, so no rotational broadening.'])

        xkernel_highres = sp.append(sp.arange(0., -1., -1. / (oversample * steps))[::-1],
                                    sp.arange(0., 1., 1. / (oversample * steps))[1:])
        ykernel_highres = (2. * (1. - epsilon) / sp.pi * sp.sqrt(1. - xkernel_highres ** 2) +
                           epsilon / 2. * (1. - xkernel_highres ** 2))

        convolve_kernel = sp.append(sp.arange(oversample)[:-1], sp.arange(oversample)[::-1]) + 1
        full_kernel = sp.convolve(ykernel_highres, convolve_kernel, 'full')

        start_index = (full_kernel.size - 1) / 2 % oversample
        kernel = full_kernel[start_index::oversample]
        norm_kernel = kernel / sp.sum(kernel)

        #Compute the convolved flux and return new spectrum
        new_flux = sp.convolve(self.flux, norm_kernel, 'same')
        if self.flux.size != new_flux.size:
            raise ValueError('new flux size is larger than old one after rotational broadening with v sin(i) = %f' % vsini)
        return TheoreticalSpectrum(self.wavelength, new_flux, self.resolution,
                    history=self.history + ['Rotational broadening with %f km/s (epsilon = %f)' % (vsini, epsilon)])

    def veiling(self, flux_level):
        """Adds a constant flux to the theoretical spectrum.

        new flux = old_flux * (1 + flux_level * median(old flux)) / (1 + flux_level).
        """
        med_flux = sp.median(self.flux)
        return TheoreticalSpectrum(self.wavelength, (self.flux + flux_level * med_flux) / (1. + flux_level), self.resolution,
                history=self.history + ['Veiled the spectrum with flux of %f' % flux_level])

    def degrade(self, new_resolution):
        """Returns a new spectrum, which has been degraded to resolution `new_resolution`

        Uses a Gaussian convolution, where (new_resolution ** -2 - old_resolution ** -2) ** -1/2 = wavelength / FWHM (FWHM = Full Width at Half Maximum).
        WARNING: Assumes a constant sampling resolution.

        Raises a ValueError if the new_resolution is larger than the old one.
        """
        if new_resolution > self.resolution:
            raise ValueError('Deconvolution not implemented. So new resolution (%i) of synthetic spectrum can not be larger than old one (%i)!' % (new_resolution, self.resolution))
        if new_resolution == self.resolution:
            return TheoreticalSpectrum(self.wavelength, self.flux, self.resolution, history=self.history + ['Already at requested resolution of %i' % new_resolution])
        sample_res = sp.median((self.wavelength[1:] + self.wavelength[:-1]) / (self.wavelength[1:] - self.wavelength[:-1])) / 2.
        add_res = (new_resolution ** (-2) - self.resolution ** (-2)) ** (-0.5)
        sig_pix = sample_res / add_res / (2 * sp.sqrt(2 * sp.log(2)))
        steps = int(sig_pix * 3)
        xval_kernel = sp.arange(-steps, steps + 0.1, 1) / sig_pix
        kernel = sp.exp(- xval_kernel ** 2 / 2.)
        norm_kernel = kernel / sp.sum(kernel)
        new_flux = sp.convolve(self.flux, norm_kernel, 'same')
        return TheoreticalSpectrum(self.wavelength, new_flux, new_resolution, history=self.history + ['Reduced to resolution of %i' % new_resolution])

    def interpolate(self, wavelength):
        """Interpolate to given wavelength array using linear interpolation.

        Warning: Linear interpolation does not conserve flux. So narrow lines might disappear, if the spectrum has not been degraded to an appropriate spectral resolution beforehand.

        Arguments:
        - `wavelength`: new wavelength array
        """
        new_flux = sp.interp(wavelength, self.wavelength, self.flux)
        return TheoreticalSpectrum(wavelength, new_flux, self.resolution, history=self.history + ['Interpolated to new wavelength scale'])

    def poly_continuum(self, parameters):
        """Multiplies the flux with a polynomial defined by given `parameters`.

        1. Define x = wavelength
        2. Compute the polynomical: parameters[0] * x ** (N-1) + parameters[1] * x ** (N-2) + ... + parameters[N-2]*x + parameters[N-1]
        3. Multiply polynomial with the flux
        4. Return spectrum with the current flux
        """
        new_flux = sp.polyval(parameters, self.wavelength) * self.flux
        return TheoreticalSpectrum(self.wavelength, new_flux, self.resolution,
                    history=self.history + ['Multiplied with polynomial with parameters %s' % str(parameters)])

    def add_hydrogen(self, vrad=0., width=70., depth=0., upper_levels=sp.arange(11, 24, 1), lower_levels=4):
        """Add N hydrogen lines to the spectrum using Lorentzian profiles.

        For emission lines set a positive depth. The following flux is then added:
        added_flux = sp.asarray(depth) / (1 + ((wavelength - center) / width)^2)

        For absoprtion lines set a negative depth. The continuum flux is then multiplied by:
        mult_flux = exp[depth / (1 + ((wavelength - center) / width)^2)]

        Might be useful for Coelho grid, which lacks hydrogen lines.

        Arguments:
        - `vrad`: scalar or (N, )-array-like; radial velocity of the hydrogen lines.
        - `width`: scalar or (N, )-array-like; spectral width (in wavelength units) = 0.5 FWHM.
        - `depth`: scalar or (N, )-array-like; spectral depth of the hydrogen lines.
        - `upper_levels`: scalar or (N, )-array-like; upper energy levels of the hydrogen lines.
        - `lower_levels`: scalar or (N, )-array-like; lower energy levels of the hydrogen lines.
        """
        #Rydberg constant / (1 + (electron mass / proton mass)) = 0.00109677583 angstrom^(-1)
        base_hydrogen_wavelengths = (0.00109677583 * (1./sp.asarray(lower_levels) ** 2 - 1. / sp.asarray(upper_levels) ** 2)) ** -1
        center_wavelengths = base_hydrogen_wavelengths * (1 + vrad / 299792.458)
        added_flux = sp.asarray(depth) / (1 + ((self.wavelength[..., sp.newaxis] - center_wavelengths) / sp.asarray(width)) ** 2.)
        new_flux = self.flux + sp.sum(added_flux, -1)
        return TheoreticalSpectrum(self.wavelength, new_flux, self.resolution,
                            history=self.history + ['Adds %i hydrogen lines' % added_flux.shape[-1]])

    def convert(self, resolution=None, vsini=None, vrad=None, hydrogen_params=None, veiling=None, poly_params=None, new_wavelength=None):
        """Returns a new shifted, broadened, interpolated wavelength.

        In order, convert does:
        1. Degrade the resolution to `resolution` (if `resolution` is not None)
        2. Degrade the spectrum with rotational velocity `vsini` in km/s (if `vsini` is not None)
        3. Shift the spectrum with `vrad` in km/s (if `vrad` is not None).
        4. Add the hydrogen lines with `hydrogen_params` as a tuple with (`vrad`, `width`, `depth`, `upper_levels`, `lower_levels`).
        5. Increase flux with `veiling` (if `veiling` is not None).
        6. Multiply flux with a polynomial (if `poly_params` is not None).
        7. Linearly Interpolate to new wavelength scale (if `new_wavelength` is not None).
        """
        spec = self
        if resolution != None:
            spec = spec.degrade(resolution)
        if vsini != None:
            spec = spec.rotate(vsini)
        if vrad != None:
            spec = spec.shift(vrad)
        if hydrogen_params is not None and False:
            spec = spec.add_hydrogen(*hydrogen_params)
        if veiling != None:
            spec = spec.veiling(veiling)
        if poly_params != None:
            spec = spec.poly_continuum(poly_params)
        if new_wavelength != None:
            spec = spec.interpolate(new_wavelength,)
        return spec


class TheoreticalSpectrumGrid(object):
    """A grid of theoretical spectra.

    The grid can be iterated over with:
    for properties, spectrum in grid:
        # do somethig with spectrum
    properties will contain a dictionary with the values for Teff, logg, etc.
    spectrum will be a TheoreticalSpectrum object representing the spectrum.

    Properties:
    - `ndim`: number of dimensions of the grid.
    - `shape`: tuple giving the size of the grid along each dimension.
    - `size`: number of spectra in the grid, can be smaller than the number implied by `shape` as not every point in the grid will generally have a spectrum associated with it.
    - `variable_names`: list of the dimension names (e.g. Teff, logg, metallicity).
    - `interpolation`: default interpolation to use when retreiving spectra (set to `cubic`, but can be changed)

    Methods:
    - `get_spectrum`: Returns a TheoreticalSpectrum interpolated at given parameter values; requires that a value for all parameters is provided and that these values lie within the grid
    - `variable`: Returns the grid values along a certain dimension.
    - `at_value`: Reduces the grid size by one, by only including the spectra with a certain value along one of the dimensions.
    """
    interpolation = 'cubic'

    def __init__(self, spectra, variables, interpolation='cubic'):
        """Creates a new spectral grid from given array of spectra and equal-sized arrays of variable values.
        """
        self._variables = [(name, sp.array(var)) for name, var in variables]
        self._spectra = sp.zeros(len(spectra), dtype='object')
        self._spectra[:] = spectra
        self.interpolation = interpolation

    @property
    def ndim(self, ):
        return len(self._variables)

    @property
    def variable_names(self, ):
        return [name for name, var in self._variables]

    @property
    def shape(self, ):
        return tuple([len(self.variable(name)) for name in self.variable_names])

    @property
    def size(self, ):
        return self._spectra.size

    def variable(self, axis_name, all_values=False):
        """return the grid points along the axis named `axis_name`.

        If `all_values` is set to True, returns every grid point multiple times, depending on how many spectra reside on that axis.
        """
        for name, variables in self._variables:
            if name == axis_name:
                if all_values:
                    return variables.copy()
                return sp.sort(sp.unique(variables))
        raise GridError('%s is not a variable for Spectral Grid with variables %s ' % (str(axis_name), str(self.variable_names)))

    def at_value(self, axis_name, value):
        """Returns a SpectrumGrid (or Spectrum, if this grid is one-dimensional) with all the spectra with given value for axis_name.

        Example:
        grid.at_value('alpha', 0) will return a grid will only contain the non-alpha enhanced spectra and no longer contain 'alpha' as a variable.
        """
        var = self.variable(axis_name, True)
        if value not in var:
            raise GridError('Value %f not found for %s' % (value, axis_name))
        index = var == value
        if self.ndim == 1:
            if sp.sum(index) > 1:
                raise GridError('Multiple Spectra with value %i for %s' % (value, axis_name))
            if isinstance(self._spectra[index][0], SpectralReader):
                return self._spectra[index][0].read()
            else:
                return self._spectra[index][0]
        return TheoreticalSpectrumGrid(self._spectra[index], [(name, variable[index]) for name, variable in self._variables if name != axis_name], interpolation=self.interpolation)

    def __getitem__(self, index):
        result = TheoreticalSpectrumGrid(self._spectra[index], [(name, var[index]) for name, var in self._variables], interpolation = self.interpolation)
        if result.size == 1:
            return result._spectra[0]
        return result

    def __iter__(self, ):
        """Yields for every spectrum a tuple with a dictionary with the variables, and the spectrum"""
        for ixspec, spec in enumerate(self._spectra):
            yield dict([(name, var[ixspec]) for name, var in self._variables]), spec

    def possible_values(self, axis_name, **kwargs):
        """Returns the possible values for `axis_name`, where the values in kwargs can still be reached.

        For example possible_values('Teff', logg=1) will return all effective temperatures for which the grid contains values with both logg <= 1 and logg >=1.
        Multiple constraints can be added by including multiple keywords.

        Arguments:
        - `axis_name`: name of the axis of which the possible values should be returned.
        - `kwargs`: constraints on the other parameters.
        """
        if len(kwargs) == 0:
            return self.variable(axis_name)

        def test_value(value):
            subgrid = self.at_value(axis_name, value)
            for const_name, const_value in kwargs.items():
                if const_value < min(subgrid.variable(const_name)) or const_value > max(subgrid.variable(const_name)):
                    return False
            return True
        return sp.array([value for value in self.variable(axis_name) if test_value(value)])

    def get_spectrum(self, interpolation=None, axis_name=None, **kwargs):
        """Returns the spectrum at given Teff, logg, etc. using linear interpolation between the grid points.

        For every dimension in the grid a value has to be provided which is within the bounds.
        A GridError is raised when:
        - A value has not been provided for every grid dimension.
        - The value is not within bounds.
        - The interpolation tries to use a grid point, where there is no spectrum defined in the spectral library.
        - The wavelengths of the arrays used to interpolate are not the same.

        Arguments:
        - `interpolation`: Type of interpolation (default: set to class variable, which is `cubic` by default)
        - `axis_name`: Sets the variable with which the interpolation will start (default: try them all, untill one does not crash).
        """
        if interpolation is None:
            interpolation = self.interpolation
        if axis_name is None:
            for try_axis_name in self.variable_names:
                try:
                    return self.get_spectrum(interpolation, try_axis_name, **kwargs)
                except GridError:
                    if try_axis_name == self.variable_names[-1]:
                        raise GridError('Failed to find a fit for all permutations; Variables are outside the grid.')
        grid = self
        if len(kwargs) != self.ndim:
            raise GridError('Number of arguments given (%i) should match number of grid dimensions (%i)' % (len(kwargs), self.ndim))
        if axis_name not in kwargs.keys():
            raise KeyError('Value for variable %s not defined' % axis_name)
        value = kwargs.pop(axis_name)
        poss_values = grid.possible_values(axis_name, **kwargs)
        if len(poss_values) == 0:
            raise GridError('Grid does not contain correct values')
        if value in poss_values:
            # no interpolation needed
            spectrum = grid.at_value(axis_name, value)
            if len(kwargs) != 0:
                spectrum = spectrum.get_spectrum(interpolation, **kwargs)
        else:
            if value > max(poss_values) or value < min(poss_values):
                raise GridError('Value %f for %s is out of range' % (value, axis_name))

            # linear interpolation
            if interpolation == 'linear':
                lower_value = sp.amax(poss_values[poss_values < value])
                spec1 = grid.at_value(axis_name, lower_value)
                if len(kwargs) != 0:
                    spec1 = spec1.get_spectrum(interpolation, **kwargs)
                upper_value = sp.amin(poss_values[poss_values > value])
                spec2 = grid.at_value(axis_name, upper_value)
                if len(kwargs) != 0:
                    spec2 = spec2.get_spectrum(interpolation, **kwargs)
                if spec1.wavelength.shape != spec2.wavelength.shape or (spec1.wavelength != spec2.wavelength).all():
                    raise GridError('Spectra at lower value on grid (%s=%f) and at higher value on grid (%s=%f) have a different wavelength array' % (axis_name, lower_value, axis_name, upper_value))
                if spec1.resolution != spec2.resolution:
                    raise GridError('Spectra at lower value on grid (%s=%f) and at higher value on grid (%s=%f) have a different resolutions' % (axis_name, lower_value, axis_name, upper_value))
                spectrum = TheoreticalSpectrum(spec1.wavelength, ((value - lower_value) * spec2.flux + (upper_value - value) * spec1.flux) / (upper_value - lower_value), spec1.resolution, ['Spectrum extracted from grid using linear interpolation'])

            # cubic interpolation
            elif interpolation == 'cubic':
                ixhigh = sp.sum(poss_values < value)
                spectra = [grid.at_value(axis_name, poss_values[index]) for index in range(ixhigh - 2, ixhigh + 2, 1) if index >= 0 and index < poss_values.size]
                if len(kwargs) != 0:
                    spectra = [spec.get_spectrum(interpolation, **kwargs) for spec in spectra]
                for spec in spectra[1:]:
                    if spec.wavelength.shape != spectra[0].wavelength.shape or (spec.wavelength != spectra[0].wavelength).all():
                        raise GridError('Wavelength of spectra used in cubic interpolation of %s do not match up' % axis_name)
                    if spec.resolution != spectra[0].resolution:
                        raise GridError('Resolution of spectra used in cubic interpolation of %s do not match up' % axis_name)
                flux_values = [spec.flux for spec in spectra]

                if ixhigh == 1:
                    flux_prime_0 = (flux_values[1] - flux_values[0])
                else:
                    flux_prime_0 = (flux_values[2] - flux_values[0]) / (poss_values[ixhigh] - poss_values[ixhigh - 2]) * (poss_values[ixhigh] - poss_values[ixhigh - 1])
                    flux_values = flux_values[1:]
                if ixhigh == poss_values.size -1:
                    flux_prime_1 = (flux_values[-1] - flux_values[-2])
                else:
                    flux_prime_1 = (flux_values[-1] - flux_values[-3]) / (poss_values[ixhigh + 1] - poss_values[ixhigh - 1]) * (poss_values[ixhigh] - poss_values[ixhigh - 1])
                    flux_values = flux_values[:-1]
                assert len(flux_values) == 2
                poly_a = 2 * flux_values[0] - 2 * flux_values[1] + flux_prime_0 + flux_prime_1
                poly_b = -3 * flux_values[0] + 3 * flux_values[1] - 2 * flux_prime_0 - flux_prime_1
                xval = (value - poss_values[ixhigh - 1]) / (poss_values[ixhigh] - poss_values[ixhigh - 1])
                new_flux = poly_a * xval ** 3. + poly_b * xval ** 2 + flux_prime_0 * xval + flux_values[0]
                spectrum = TheoreticalSpectrum(spectra[1].wavelength, new_flux, spectra[1].resolution, ['Spectrum extracted from grid using cubic interpolation'])
            else:
                raise ValueError('Interpolation scheme %s not defined' % interpolation)
        return spectrum

    def find_valid(self, *args, **kwargs):
        """Returns close values for the parameters, that lie on the grid.

        This method is used after fitting, because some fitting procedures return solutions slightly off the grid. This method can be used to find the closest valid value.
        Currently only searches shifts in single dimension (if needed could be improved in future).

        For input parameters see self.get_spectrum.
        """
        try:
            self.get_spectrum(*args, **kwargs)
        except GridError:
            pass
        else:
            return kwargs
        valid_offsets = []
        for name, value in kwargs.items():
            replacement_values = self.possible_values(name)
            replacement_sorted = replacement_values[sp.argsort(abs(replacement_values - value))]
            for ixrep, replacement_value in enumerate(replacement_sorted):
                new_try = kwargs.copy()
                new_try[name] = replacement_value
                try:
                    self.get_spectrum(*args, **new_try)
                except GridError:
                    pass
                else:
                    valid_offsets.append((name, ixrep, replacement_value))
                    break
        if len(valid_offsets) == 0:
            raise GridError("No valid 1D offset to grid for parameter set %s found" % str(kwargs))
        ixclosest = sp.argmin([offset[1] for offset in valid_offsets])
        valid_kwargs = kwargs.copy()
        valid_kwargs[valid_offsets[ixclosest][0]] = valid_offsets[ixclosest][2]
        return valid_kwargs

    def load(self, wavelength_range=None):
        """Loads all the spectra into memory.

        This can really speed up the interpolation process, but the grid may be too large for local memory for some grids (e.g. Coelho grid).

        Setting `wavelength_range` to a tuple with (lower bound, upper bound) will cause only a limited wavelength range to be loaded in (any longer spectra will be cut to this wavelength range), which might save memory.
        """
        for ixspec, spec in enumerate(self._spectra):
            if isinstance(spec, SpectralReader):
                self._spectra[ixspec] = spec.read()
                spec = self._spectra[ixspec]
            if wavelength_range is not None:
                use = sp.logical_and(spec.wavelength > wavelength_range[0], spec.wavelength < wavelength_range[0])
                self._spectra[ixspec] = spec[use]

    def convert_spectra(self, **kwargs):
        """Returns a grid with modified spectra.

        Modification are made by calling convert on every spectrum.
        """
        variables = self._variables
        spectra = []
        for spectrum in self._spectra:
            if isinstance(spectrum, SpectralReader):
                spectrum = spectrum.read()
            spectra.append(spectrum.convert(**kwargs))
        return TheoreticalSpectrumGrid(spectra, variables)

    def save(self, filename):
        """Saves grid of model spectra in a numpy .npz file.

        This grid can afterwards be retreived by supplying the `filename` to model_spectra.pre_loaded(<filename>)
        """
        dict_save = self._variables
        spectra = [spec.view(sp.ndarray) for spec in self._spectra]
        resolution = [spec.resolution for spec in self.spectra]
        dict_save.update(spectra=spectra, resolution=resolution)
        sp.savez(filename, **dict_save)

def airtovac(air):
    """Helper function to convert air wavelength to vacuum wavelengths.

    Returns an array of the same shape as air with the corresponding vacuum wavelengths, based on the equation in Morton 1991, ApJS 77 119

    Arguments:
    - `air`: air wavelength in Angstrom
    """
    sigsq=(1e4 / air) ** 2
    vac = (1. + 6.4328e-5 + 2.94981e-2 / (146. - sigsq) +
            2.5540e-4 / ( 41. - sigsq)) * air
    return vac


def normalize(grid):
    """Returns list of normalized spectra

    Uses a cubic spline with two set knots (only works for APOGEE spectra).
    """
    new_spectra = []
    for spec in grid._spectra:
        use = sp.ones(spec.size, dtype='bool')
        for ix in range(10):
            spline_fit = sp.interpolate.LSQUnivariateSpline(spec['wavelength'][use], spec['flux'][use], [15700, 16300], k=3)(spec['wavelength'])
            use = spec['flux'] > sp.amin((0.98, sp.sort(spec['flux'] / spline_fit)[len(spline_fit) / 2])) * spline_fit
        new_spec = spec.copy()
        new_spec['flux'] = new_spec['flux'] / spline_fit
        new_spectra.append(new_spec)
    return new_spectra

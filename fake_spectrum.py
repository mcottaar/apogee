"""Module containing routines to create fake spectra with noise and subsequently fit them.
"""
import observed_spectrum, fit, model_spectra
import scipy as sp
import os
import copy

class FakeData(fit.SpectralFit):
    """Represents a fake dataset, useful for Monte Carlo simulations.

    All of the optimzation and MCMC routines from fit.SpectalFit are available.
    Additional functionality:
    - `from_grid`: Creates a new FakeData object drawn from a grid of theoretical spectra.
    - `add_noise`: Alters the flux by adding Gaussian noise. Because the noise is always added to the original flux (stored in a 'base_flux' field), the noise does not add up when `add_noise` is called multipe times.
    - `multifit`: Returns an array with the best-fit for multiple iterations of random noise.

    Also adds a 'base_flux' field to the observed spectrum, which contains the flux before the noise is added.
    """
    def __init__(self, observed, *args, **kwargs):
        """Creates a new fit to Monte Carlo generated data.

        Expects the same arguments as fit.Spectralfit.
        The observed array should be a structured array, containing at least the 'wavelength', 'error', and 'flux' fields.
        If a 'base_flux' field is not provided, the values from the 'flux' field are copied. When adding noise, the flux will be set to the base_flux + Gaussian noise.
        If a 'mask' field is not provided, no masking is assumed.
        """
        if not isinstance(observed, list):
            for new_name, dtype, default in [('base_flux', 'f8', observed['flux']), ('mask', 'bool', sp.zeros(observed.shape))]:
                if new_name not in observed.dtype.names:
                    new_dtype = observed.dtype.descr + [(new_name, dtype)]
                    new_observed = sp.zeros(observed.shape, new_dtype)
                    for name in observed.dtype.names:
                        new_observed[name] = observed[name]
                    new_observed[new_name] = default
                    observed = new_observed
        super(FakeData, self).__init__(observed, *args, **kwargs)
        self.add_noise()

    @classmethod
    def from_grid(cls, grid, snr=10., wavelength='apogee', resolution=20000, vrad=0., vsini=0., veiling=0., poly_params=sp.array([0, 0, 1]), nepochs=1, **kwargs):
        """Creates a new FakeData instance using a spectrum drawn from the grid.

        By default the parameters will be set to the input values.

        Parameters:
        - `grid`: grid of theoretical spectra, used to draw the spectrum.
        - `snr`: global signal-to-noise ratio.
        - `wavelength`: wavelength of the new spectrum (default: use wavelength array from APOGEE spectra).
        - `resolution`: spectral resolution of the new spectrum.
        - `vrad`: radial velocity in km/s.
        - `vsini`: rotational velocity in km/s.
        - `veiling`: flux veiling
        - `poly_params`: polynomial continuum profile
        - `kwargs`: all keywords expected when drawing a spectrum from the grid (e.g. Teff, logg, FeH) need to be provided.
        """
        if wavelength == 'apogee':
            sample_spec = sp.load( os.path.join(os.path.dirname(__file__), 'sample_apogee_spectrum.npy'))
            wavelength = sample_spec['wavelength']
            mask = sample_spec['mask']
        else:
            mask = sp.zeros(wavelength.size)
        temp_res = resolution * 10
        min_temp_wave = sp.amin(wavelength) * 0.99
        max_temp_wave = sp.amax(wavelength) * 1.01
        temp_wavelength = sp.exp(sp.arange(sp.log(min_temp_wave), sp.log(max_temp_wave), 1. / temp_res))
        observed = grid.get_spectrum(**kwargs).interpolate(temp_wavelength).convert(vrad=vrad, vsini=vsini, veiling=veiling, poly_params=poly_params, resolution=resolution).interpolate(wavelength)
        new_dtype = observed.dtype.descr + [('error', 'f8'), ('mask', 'bool')]
        new_observed = sp.zeros(observed.shape, new_dtype)
        for name in observed.dtype.names:
            new_observed[name] = observed[name]
        new_observed['error'] = new_observed['flux'] / snr
        new_observed['mask'] = mask
        if nepochs > 1:
            new_observed = [new_observed.copy() for ix in range(nepochs)]
        result = cls(new_observed, grid=grid, resolution=resolution, vrad=vrad, vsini=vsini, veiling=veiling, poly_params=poly_params, **kwargs)
        if nepochs > 1:
            result.parameters.make_variable('vrad')
            result.parameters.make_variable('poly_params')
        return result
        

    def add_noise(self, ):
        """Redraws Gaussain noise with a width set by the uncertainty array.

        The new Gaussain noise is added to the base_flux, so calling this routine multiple times will not lead to a noisier spectrum.
        """
        if len(self) == 0:
            self.observed()['flux'] = self.observed()['base_flux'] + sp.randn(self.observed().size).reshape(self.observed().shape) * self.observed()['error']
        else:
            for child in self:
                child.add_noise()

    def multifit(self, nfit=50, fit_local=True, fit_global=False, fit_mcmc=False, fit_params=('Teff', 'logg', 'vsini', 'veiling', 'vrad')):
        """Refit the fake spectrum multiple times, drawing a new set of noise every time.

        Returns a dictionary with the results from the local fit, global fit, and the MCMC (if available), as well as the best chi-squared found.

        Arguments:
        - `nfit`: number of fits to run.
        - `fit_local`: Fit the data starting from the current set of parameters using l_bfgs_b from scipy.
        - `fit_global`: Fit the data using a global search of the parameter space using differential evolution from openopt.
        - `fit_mcmc`: Fit the data using the MCMC from emcee, starting from the best-fit chi-squared so far (from local fit, global fit, or the initial set of parameters).
        - `fit_params`: parameter to fit and vary in the MCMC.
        """
        var_names = self.parameters.helper_variables(fit_params)[-1]
        store_names = ['chisq'] + var_names
        result = {}
        result['best'] = sp.zeros(nfit, dtype=[(name, 'f8') for name in store_names])
        if fit_local:
            result['local'] = sp.zeros(nfit, dtype=[(name, 'f8') for name in store_names])
        if fit_global:
            result['global'] = sp.zeros(nfit, dtype=[(name, 'f8') for name in store_names])
            result['global_all'] = sp.zeros((nfit, 2), dtype=[(name, 'f8') for name in store_names])
        if fit_mcmc:
            result['mcmc'] = sp.zeros((nfit, 100, 300), dtype=[(name, 'f8') for name in store_names])
        for ixfit in range(nfit):
            print ixfit
            self.add_noise()
            best_chisq = self.chisq()
            best_params = self.parameters.helper_variables(fit_params)[0]
            if fit_local:
                print self.optimize(fit_params, method='l_bfgs_b')
                result['local']['chisq'][ixfit] = self.chisq()
                for name, value in zip(var_names, self.parameters.helper_variables(fit_params)[0]):
                    result['local'][name][ixfit] = value
                if self.chisq() < best_chisq:
                    best_chisq = self.chisq()
                    best_params = self.parameters.helper_variables(fit_params)[0]
            if fit_global:
                global_chisq = sp.infty
                for ixrun in range(2):
                    self.optimize(fit_params, method='de', maxFunEvals=1e4, seed=int(sp.rand() * 1e7))
                    if self.chisq() < global_chisq:
                        gobal_chisq = sp.infty
                        global_params = self.parameters.helper_variables(fit_params)[0]
                    for name, value in zip(var_names, self.parameters.helper_variables(fit_params)[0]):
                        result['global'][name][ixfit] = value
                    result['global_all']['chisq'][ixfit, ixrun] = self.chisq()
                result['global']['chisq'][ixfit] = global_chisq
                for name, value in zip(var_names, global_params):
                    result['global'][name][ixfit] = value
                if global_chisq < best_chisq:
                    best_chisq = global_chisq
                    best_params = global_params
            if fit_mcmc:
                self.parameters.helper_variables(fit_params, best_params)
                self.emcee(fit_params, 100, 300, renormalize=True)
                result['mcmc']['chisq'][ixfit, ...] = self.res_emcee['chisq']
                for name in var_names:
                    result['mcmc'][name][ixfit, ...] = self.res_emcee[name]
                if sp.amin(self.res_emcee['chisq']) < best_chisq:
                    best_chisq = sp.amin(self.res_emcee['chisq'])
                    best_params = self.parameters.helper_variables(fit_params)[0]
            result['best']['chisq'][ixfit] = best_chisq
            for name, value in zip(var_names, best_params):
                result['best'][name][ixfit] = value
        return result

    def old_multifit(self, method='l_bfgs_b', prefit=False, reset_vsini=False, factr=1e7, niter=10, figure=None, verbose=False):
        """Refit the fake spectrum multiple time, drawing a new set of noise every time and returns an array will all best-fit values.

        Arguments:
        - `method`: Optmization method to be used (see self.optimize for the options).
        - `prefit`: If True, optimizes the Teff, logg, and vsini before optimizing all parameters.
        - `reset_vsini`: If vsini < 3 km/s after the main fit refit again with all parameters with the vsini set to 20 km/s (the optimization might get stuck at low vsini)
        - `factr`: Precision factor passed on to fmin_l_bfgs_b.
        - `niter`: number of Monte Carlo simulations to run (with a different noise every time).
        - `figure`: If provided plot histograms of the resulting parameters on the figure.
        - `verbose`: If True, provides status reports and summarizes the results.
        """
        if verbose:
            print 'initial parameters', self.parameters
            print 'fitting %i data points' % self.datasize
        params = dict(self.parameters)
        names = self.parameters.helper_variables(['Teff', 'logg', 'vsini', 'veiling', 'vrad', 'resolution'])[3]
        best_fit = sp.zeros(niter, dtype=[(name, 'f8') for name in names + ['chisq']])
        for ixrun in range(niter):
            if verbose:
                print 'running %i of %i' % (ixrun + 1, niter)
            self.add_noise()
            try:
                if prefit:
                    print 'pre-fitting Teff, logg, and vsini', self.optimize(('Teff', 'logg', 'vsini'), method=method, renormalize=False)
                res = self.optimize(method=method, renormalize=False, factr=factr)
                if reset_vsini and self.parameters['vsini'] < 3.:
                    if verbose:
                        print 'too low vsini. First fit:', res
                    self.parameters['vsini'] = 20.
                    res = self.optimize(method=method, renormalize=False, factr=factr)
                values = self.parameters.helper_variables(['Teff', 'logg', 'vsini', 'veiling', 'vrad', 'resolution'])[0]
                for name, value in zip(names, values):
                    best_fit[name][ixrun] = value
                best_fit['chisq'][ixrun] = self.compute_fit()[1]
                if verbose:
                    print 'best fit', self.parameters, 'with chi-squared', self.compute_fit()[1]
                    print 'report from optimizer', res
            finally:
                self.parameters.update(params)
        if verbose:
            for param_name in best_fit.dtype.names:
                print '%s: %f +/- %f' % (param_name, sp.mean(best_fit[param_name]), sp.std(best_fit[param_name]))
        if figure != None:
            ncolumns = int(sp.sqrt(len(best_fit.dtype.names)))
            nrows = (len(best_fit.dtype.names) - 1) / ncolumns + 1
            for ixparam, param_name in enumerate(best_fit.dtype.names):
                ax = figure.add_subplot(nrows, ncolumns, ixparam + 1)
                ax.hist(best_fit[param_name])
                if param_name == 'chisq':
                    ax.set_title('%i unmasked datapoints' % self.datasize)
                ax.set_xlabel(param_name)
                ax.set_ylabel('N')
        return best_fit


def test_optimization(grid, Teff, logg, vsini=30., veiling=0., snr=10., niter=100, method='l_bfgs_b', prefit=True, reset_vsini=True, reset_logg=True, **kwargs):
    """Tests how well the optimization routine works for a broad range of initial temperatures and radial velocity offsets.

    The optimization will start with:
    - a random temperature between 3600 and 6900 K
    - logg = 3
    - vsini = 10
    - veiling = 0.1
    - a radial velocity which is too large between 10 and 100 km/s.
    In every fit the noise will be redrawn.

    An array will be returned with the Teff, vrad offset that the optimization started with, the final Chi-squared, and the fitted parameters.
    If the fit succeeded the chi-squared will be within a few percent from the number of unmasked pixels.

    Arguments:
    - `grid`: Grid of theoretical spectra (with variables Teff and logg), used to draw the mock spectrum and fit it.
    - `Teff`: Effective temperature of spectrum to be fitted.
    - `logg`: log(g) of spectrum to be fitted.
    - `vsini`: rotational velocity in km/s of spectrum to be fitted.
    - `veiling` initial veiling.
    - `snr`: signal-to-noise ratio of the spectrum.
    - `niter`: number of iterations that will be run.
    - `factr`: The optimization routine will continue till a precision of factr * machine precision (~1e-16).
    - `prefit`: Fit the Teff, logg, and vsini seperately before the global fit.
    - `reset_vsini`: If vsini < 3 km/s, reset to 20 km/s. If vsini (or log(g)) are reset, rerun the fit afterwards.
    - `reset_logg`: If log(g) < 1.5, reset to log(g) = 4.5. If log(g) (or vsini) are reset, rerun the fit afterwards.
    """
    store_params = ['Teff', 'logg', 'vsini', 'veiling', 'vrad']
    result = sp.zeros(niter, dtype=[(name, 'f8') for name in ['Teffin', 'vradin', 'chisq'] + store_params])
    mockfit = FakeData.from_grid(grid, snr, Teff=Teff, logg=logg, vsini=vsini, veiling=veiling)
    method = 'l_bfgs_b'
    for index in range(niter):
        result[index]['Teffin'] = sp.rand() * 3300. + 3600.
        result[index]['vradin'] = 10 ** (sp.rand() + 1.)
        mockfit.add_noise()
        mockfit.parameters.update(Teff=result[index]['Teffin'], vrad=result[index]['vradin'], logg=3., vsini=10., veiling=0.1)
        if prefit:
            mockfit.optimize(('Teff', 'logg', 'vsini'), method=method, renormalize=False, **kwargs)
        mockfit.optimize(method=method, renormalize=False, **kwargs)
        if reset_vsini or reset_logg:
            rerun = False
            for base in mockfit.iter_spectra():
                if reset_vsini and base.parameters['vsini'] < 3.:
                    base.parameters['vsini'] = 20.
                    rerun = True
                if reset_logg and base.parameters['logg'] < 1.5:
                    base.parameters['logg'] = 4.5
                    rerun = True
            if rerun:
                mockfit.optimize(method=method, renormalize=False, **kwargs)
        for name in store_params:
            result[index][name] = mockfit.parameters[name]
        result[index]['chisq'] = mockfit.compute_fit()[1]
    return result
    
"""Module containing functions to fit observed spectra with theoretical model spectra.

The main fitting object can be obtained by calling `load_fit`.

An example usage case can be found in `fit_script.py`."""

import scipy as sp
from scipy import ndimage
import collections
import os
import cPickle
import re
from apogee import double_line, model_spectra, observed_spectrum


def load_fit(specdir, grid, star_name=None, split_chips=True, resultdir=None, specinfo=None, **kwargs):
    """Loads SpectralFit for all spectra in specdir.

    Returns a SpectralFit object, which contains several nested other SpectralFit objects:
    layer 1: hundreds of SpectralFit objects representing individual stars.
    layer 2: Every one of these will contain multiple SpectralFit objects representing individual epochs.
    layer 3: Each of these contains 3 SpectralFit objects represting individual chips.

    If `star_name` is set only a single star is loaded in, so layer 1 does not exist.
    If `split_chips` is set to False the individual chips will not have their own SpectralFit objects and layer 3 will not exist
    Provide a `specinfo` object (as provided by the cluster_list function) manually when you run this function often, because recomputing the `specinfo` object every time is slow.

    Be default the parameters are set to vary per star, but remain constant over the multiple epochs. The exceptions to this are:
    - `vrad`: Will be different for every epoch (but constant along multiple chips).
    - `veiling`: Will be different for every epoch (but constant across multiple chips).
    - `poly_params`: The normalization will be refitted for every chip.
    - `resolution`: Kept constant at 22.5K along all observations.

    The parameter values will by default be set to the values in the APOGEE pyfits headers.
    In case of conflicting default values, the last pyfits header decides.
    These defaults will be overriden by the results in `resultdir` if set.

    Arguments:
    - `specdir`: Directory containg the APOGEE spectra.
    - `grid`: Either a theoretical grid, or a string with the directory/filename containing a theoretical grid.
    - `star_name`: String; Only the spectra of the specified stellar identifier will be loaded (default: all stars are loaded).
    - `split_chips`: If True, include a subfit for every chip in the spectrum.
    - `resultdir`: Directory, containing the results of a previous fit to which the parameter values will be set.
    - `specinfo`: Header information from all apVisit files as loaded by cluster_list (will be loaded in if not provided, which may greatly slow down this routine)
    """
    if isinstance(grid, basestring):
        print 'Trying to load grid from %s' % grid
        for grid_name in model_spectra.grid_names:
            try:
                grid = getattr(model_spectra, grid_name)(grid).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.))
                print '%s grid found' % grid_name
                break
            except IOError:
                print '%s grid not found' % grid_name
        else:
            raise ValueError('No grid found in %s' % grid)

    if specinfo is None: # load header info from all apVisit files if not provided
        specinfo = observed_spectrum.cluster_list(specdir, 10)
    if star_name is None:
        full_observed = []
        for objid in sp.unique(specinfo['objid']):
            star = specinfo[specinfo['objid'] == objid]
            full_observed.append([observed_spectrum.read_apogee_spectrum(filename, split_chips=split_chips, **kwargs) for filename in star['full_filename']])
        star_depth = 1
    else:
        full_observed = [observed_spectrum.read_apogee_spectrum(filename, split_chips=split_chips, **kwargs) for filename in specinfo[specinfo['objid'] == star_name]['full_filename']]
        star_depth = 0
    fit = SpectralFit(full_observed, grid)
    for key in fit.parameters.possible_keys:
        fit.parameters.vary_till_depth(key, star_depth)

    # set the layers where the radial velocity, veiling, and polynomial will be varied
    if 'vrad' in fit.parameters.keys():
        fit.parameters.vary_till_depth('vrad', star_depth + 1)
    else:
        fit.parameters.vary_till_depth('vradA', star_depth + 1)
        fit.parameters.vary_till_depth('vradB', star_depth + 1)
    fit.parameters.vary_till_depth('veiling', star_depth + 1)
    fit.parameters.vary_till_depth('poly_params', star_depth + 99)
    fit.parameters.vary_till_depth('resolution', star_depth - 1)

    # set default parameter values (will be ignored)
    fit.set_default()

    # load parameters from previous fit, if a directory containing the previous fit is provided
    if resultdir is not None:
        print 'loading from', resultdir
        fit.load(resultdir)
    return fit


class Parameters(collections.MutableMapping):
    """Dict-like object containing the parameter values of the current fit.

    The values can be accessed through the normal dictionary operations (deletion of keys has been disabled).

    To ensure that all subfits will always have the same value, the parameter can be set to a non-list object directly or parameters.make_constant can be used.
    To ensure that during fitting subfits will be able to have different parameter values, the parameter can be set to a list (with the same length as the number of subfits) or parameters.make_variable can be used.
    `parameters.vary_till_depth` can be used to set the depth below which the parameters should not vary anymore with the subfits.

    `parameters.varies(<param_name>)` tells whether <param_name> can take different values along the subfits.
    """
    _data = None

    def __init__(self, fit, all_params=None):
        """Creates a new Parameters dictionary-like object for the given `fit`.

        Parameters:
        - `fit`: SpectralFit object representing all stars, a single star, a single epoch, or a single chip.
        - `all_params`: initial parameter values (otherwise they are set to None).
        """
        self.fit = fit
        if all_params is None:
            all_params = dict([(key, None) for key in self.possible_keys])
        self._data = all_params

    def from_emcee(self, index):
        """Set the Parameter value to a specific walker value in the emcee MCMC chain.

        Arguments:
        - `index`: index of the parameter settings in the emcee chain.
        """
        if self.fit.res_emcee is None:
            raise ValueError('Emcee chain has not been run, so can not extract value')
        for name in self.fit.res_emcee.dtype.names:
            match = re.search('^E([0-9]*)(.*)', name)
            if match is not None:
                self.fit[int(match.group(1))].parameters[match.group(2)] = self.fit.res_emcee[index][name]
            else:
                self[name] = self.fit.res_emcee[index][name]

    def move_to_grid(self, ):
        """Convert the grid parameters so that they lie on the grid, if they lie just outside it.
        """
        if any([self.varies(name) for name in self.grid_keys]):
            for subfit in self.fit:
                subfit.parameters.move_to_grid()
        else:
            self.update(self.fit.grid.find_valid(**self.grid_params()))

    # Several functions that change whether a parameter is varied across the subfits or not during the optimization and MCMC
    def varies(self, parameter):
        """Returns True if `parameter` varies along the subfits.

        Returns False if `parameter` is kept constant along the subfits.
        """
        return isinstance(self[parameter], list)

    def vary_till_depth(self, parameter, depth=0):
        """Make the parameter variable untill given depth.

        For example vary_till_depth('vsini', 1) will ensure that all subfits have the same value for 'vsini', but all sub-subfits have the same value.
        If applied to the top level this means that all stars will have different rotational velocities, which remain constant over multiple epochs.
        """
        if depth > 0 and len(self.fit) != 0:
            self.make_variable(parameter)
            for child in self.fit:
                child.parameters.vary_till_depth(parameter, depth-1)
        else:
            self.make_constant(parameter)

    def make_constant(self, parameter):
        """Ensures that all subfits have the same value for `parameter`.

        This new value is set to the first value.
        """
        first_child = self
        while first_child.varies(parameter):
            first_child = first_child.fit[0].parameters
        self[parameter] = first_child[parameter]

    def make_variable(self, parameter):
        """Ensures that all subfits will be able to have different values for `parameter`.

        All subfits will start out with the same value.
        Any parents are made variable as well.
        """
        if self.fit.parent is not None and not self.fit.parent.parameters.varies(parameter):
            self.fit.parent.parameters.make_variable(parameter)
        if not self.varies(parameter) and len(self.fit) != 0:
            self[parameter] = [self[parameter]] * len(self.fit)

    # Methods to cause dictionary-like behaviour
    def __getitem__(self, key):
        if key[0] == 'E':
            try:
                ixepoch = int(key[1])
                return self.fit[ixepoch].parameters[key[2:]]
            except ValueError, KeyError:
                pass
        if '_index' in key:
            main_key, str_index = key.split('_index')
            return self[main_key][int(str_index)]
        full_data = self._data[key]
        for index in self.fit.index:
            if isinstance(full_data, list):
                full_data = full_data[index]
        return full_data

    def __setitem__(self, key, value):
        """Sets parameter value (or values).

        When `value` is a list, the values of the subfits are set.

        This function is called with Parameters[key] = value.
        """
        if key not in self.possible_keys:
            raise KeyError('Parameter %s not defined, so can not be set' % key)
        if isinstance(value, list) and len(value) != len(self.fit):
            raise ValueError('Length of new values for %s (%i) should match number of subfits (%i)' % (key, len(value), len(self.fit)))
        if self.fit.parent is not None and not self.fit.parent.parameters.varies(key):
            self.fit.parent.parameters[key] = value
            return
        if self.fit.parent is None:
            self._data[key] = value
        else:
            to_set = self._data[key]
            for index in self.fit.index[:-1]:
                to_set = to_set[index]
            to_set[self.fit.index[-1]] = value

    def __iter__(self, ):
        for item in self._data:
            yield item

    def __delitem__(self, key):
        raise ValueError('Parameters class does not allow for the deletion of keys.')

    def __len__(self, ):
        return len(self._data)

    def __str__(self, ):
        return 'Parameter(%s)' % dict(self.items())

    # Define grid and convert keys
    @property
    def convert_keys(self, ):
        if isinstance(self.fit.grid, double_line.DoubleLineGrid):
            return ('vsiniA', 'vsiniB', 'veiling', 'vradA', 'vradB', 'resolution', 'poly_params', 'rel_flux')
        else:
            return ('vsini', 'veiling', 'vrad', 'resolution', 'poly_params')

    @property
    def grid_keys(self, ):
        """Tuple of the parameter names used to extract a spectrum from the theoretical grid.
        """
        return tuple(self.fit.grid.variable_names)

    @property
    def possible_keys(self, ):
        """Tuple of all possible parameter names.
        """
        return self.convert_keys + self.grid_keys

    def grid_params(self, ):
        """Returns dictionary containing the parameters that are used to draw a spectrum from the theoretical grid before post-processing.
        """
        return dict([(key, value) for key, value in self.items() if key in self.grid_keys])

    def convert_params(self, ):
        """Returns dictionary containing the parameters that are used to post-process the spectra.
        """
        return dict([(key, value) for key, value in self.items() if key in self.convert_keys])

    # helper function defining usefull bounds and default step sizes for the MCMC
    def helper_variables(self, param_names, new_values=None, box_bounds=False,):
        """Returns the values, bounds, initial emcee step size, and names of the parameters listed in `param_names`.

        If new values are set (i.e. `new_values` is not None) the method will still return the old values.

        Optionally sets the variables to new values in `new_values`.

        Parameters:
        - `param_names`: parameter names to include.
        - `new_values`: new values for the parameters.
        - `box_bounds`: If True, do not allow None in bounds (i.e. force parameters to be in finite box).
        """
        if new_values is not None and not isinstance(new_values, list):
            new_values = list(new_values)
        new_list = []
        bounds = []
        emcee = []
        names = []
        for name in param_names:
            if self.varies(name):
                for child in self.fit:
                    res = child.parameters.helper_variables((name, ), new_values, box_bounds)
                    new_list.extend(res[0])
                    bounds.extend(res[1])
                    emcee.extend(res[2])
                    names.extend(['E' + str(child.index[-1]) + subname for subname in res[3]])
            else:
                nrepeat = 1
                new_list.append(self[name])
                if new_values is not None:
                    self[name] = new_values.pop(0)
                default_bounds = {'veiling': [(0, None), (1e-8, 10.)],
                    'vsini': [(0, None), (1e-8, 300.)],
                    'vrad': [(None, None), (-200, 200)],
                    'resolution': [(1, None), (10, 100000)]}
                for index_repeat in range(nrepeat):
                    if name[-1] in 'AB':
                        index_name = name[:-1]
                    else:
                        index_name = name
                    if index_name in default_bounds.keys():
                        bounds.append(default_bounds[index_name][box_bounds])
                    elif name in self.fit.grid.variable_names:
                        bounds.append((min(self.fit.grid.variable(name)) + 0.01,
                                       max(self.fit.grid.variable(name)) - 0.01))
                    else:
                        raise ValueError('No bounds defined for %s' % name)
                    if index_name == 'Teff':
                        emcee.append(2e-1)
                    elif index_name == 'vrad':
                        emcee.append(5e-3)
                    elif index_name == 'vsini':
                        emcee.append(5e-3)
                    else:
                        emcee.append(1e-3)
                    names.append(name + ('' if nrepeat == 1 else '_index%i' % index_repeat))
        return new_list, bounds, emcee, names


class SpectralFit(object):
    """An object representing the fit to a single observed spectrum or to a group of spectra.

    In case the object represents a fit to a group of spectra, the fits to the individual spectra will also be SpectralFits, which can be found by:
    >>> for subfit in self:
    >>>     do something with subfit
    They can also be accessed by indexing:
    >>> subfit = self[0]  # returns the first subfit
    Note that subfits can themselves consist of multiple subfits, leading to a stacked structure.
    The number of subfits can be found by:
    >>> print len(self)

    Properties:
    - `grid`: grid of theoretical spectra, which are fitted to the observations.
    - `observed`: observed spectrum of list of observed spectra, to which the theoretical spectra are fitted.
    - `parameters`: Dict-like object containing the current values of the fit.
    - `index`: Tuple of indices used to get down the tree from the global fit to the present subfit.
    - `res_emcee`: result of the previous MCMC run by `emcee`

    Methods:
    - `renormalize`: Recomputes the polynomial offset in continua between the theoretical and observed spectra. Sets the 'poly_params' parameter.
    - `compute_fit`: Computes the current fit to the data.
    - `optimize`: Optimizes the current fit to the data by minimizing the chi-squared using a variety of global and local optimization methods.
    - `emcee`: run an MCMC and stores the result under `res_emcee`
    """
    grid = None
    _children = None
    res_emcee = None

    def __init__(self, observed, grid, parent=None, **kwargs):
        """Creates a new SpectralFit to fit the model spectra in `grid` to the `observed` spectra.

        Arguments:
        - `observed`: nested list of spectra.
        - `grid`: grid of model spectra.
        - `parent`: overlying SpectralFit from which this SpectralFit is a subfit (do not provide this parameter).
        Additional keywords will be set to the parameters.
        """
        self.grid = grid
        self.parent = parent
        if parent is None:
            self.parameters = Parameters(self)
        else:
            self.parameters = Parameters(self, parent.parameters._data)
        if isinstance(observed, list):
            self._children = [self.__class__(sub_obs, grid, self) for sub_obs in observed]
        else:
            self._observed = observed
        self.parameters.update(kwargs)

    def observed(self, normalized=False, shift_RV=False):
        """Returns observed spectrum or list of observed spectra from subfits.

        Arguments:
        - `normalized`: If True the continuum of the spectrum will be adjusted using the value in poly_params.
        - `shift_RV`: If True shift the wavelength array to stellar system (so that vrad=0).
        """
        if len(self) == 0:
            if not normalized and not shift_RV:
                return self._observed
            obs = self._observed.copy()
            if normalized:
                norm = sp.polyval(self.parameters['poly_params'], obs['wavelength'])
                obs['flux'] = obs['flux'] / norm
                obs['error'] = obs['error'] / norm
            if shift_RV:
                obs['wavelength'] = obs['wavelength'] * (1. - self.parameters['vrad'] / 299792.458)
            return obs
        else:
            return [child.observed(normalized, shift_RV) for child in self]

    def renormalize(self, npoly=None, theoretical=None):
        """Calculates the polynomial needed to match the theoretical and observed continua.

        Sets the `poly_params` value to the best-fit polynomial.

        Arguments:
        - `npoly`: set the degree of the polynomial used (default: keep the same).
        - `theoretical`: pre-computed value of compute_fit() without applying `poly_params` (providing this can speed things up slightly, but is not really needed).
        """
        convert_theoretical = theoretical is not None
        if theoretical is None:
            to_apply = list(self.parameters.convert_keys)
            to_apply.remove('poly_params')
            theoretical = self.compute_fit(to_apply)
        if self.parameters.varies('poly_params'):
            for child, theor in zip(self, theoretical):
                child.renormalize(npoly, theor)
            return
        if npoly is None:
            npoly = len(self.parameters['poly_params']) - 1
        if isinstance(theoretical, list):
            tofit = reduce(sp.append, flatten(theoretical))
        else:
            tofit = theoretical

        use_for_fit = ~tofit['mask'] & (tofit['theoretical'] != 0.)
        if sp.sum(use_for_fit) > npoly:
            self.parameters['poly_params'] = sp.polyfit(tofit['wavelength'][use_for_fit], (tofit['flux'] / tofit['theoretical'])[use_for_fit], npoly)
        else:
            self.parameters['poly_params'] = sp.array([0] * npoly + [1])
        if convert_theoretical:
            if isinstance(theoretical, list):
                for arr in theoretical:
                    arr['theoretical'] = sp.polyval(self.parameters['poly_params'], arr['wavelength']) * arr['theoretical']
                    arr['offset'] = arr['flux'] - arr['theoretical']
                    arr['chi'] = arr['offset'] / arr['error']
            else:
                theoretical['theoretical'] = sp.polyval(self.parameters['poly_params'], theoretical['wavelength']) * theoretical['theoretical']
                theoretical['offset'] = theoretical['flux'] - theoretical['theoretical']
                theoretical['chi'] = theoretical['offset'] / theoretical['error']

    def compute_fit(self, to_apply=None, from_grid=None, renormalize=False):
        """Main function to compute the current fit.

        Returns an array or list of arrays, containing the wavelength, flux, flux offset, and normalized flux offset with respect to the observed spectrum (or spectra).

        Arguments:
        - `to_apply`: List of adjustments to apply to the theoretical spectrum in the order in which they will be applied (default: everything is applied).
        - `from_grid`: TheoreticalSpectrum extracted from the grid. If provided grid parameters (e.g. Teff, log(g)) are ignored.
        - `renormalize`: renormalizes the spectrum during the fit computation (if True the variable `poly_params` will be ignored).
        """
        if to_apply is None:
            to_apply = self.parameters.convert_keys
        if renormalize and 'poly_params' in to_apply:
            to_apply = list(to_apply)
            to_apply.remove('poly_params')

        # Draw spectrum from grid, when Teff, logg, etc. are uniquely defined. If they are not uniquely defined, continue to the subspectra.
        if from_grid is None:
            for key in self.parameters.grid_keys:
                if self.parameters.varies(key):
                    res = [child.compute_fit(to_apply, from_grid, renormalize) for child in self]
                    if renormalize and (self.parent is None or self.parent.parameters.varies('poly_params')) and not self.parameters.varies('poly_params'):
                        self.renormalize(theoretical=res)
                    return res
            from_grid = self.grid.get_spectrum(**self.parameters.grid_params())

        # Apply transformations (e.g. vrad, vsini, veiling) to spectrum. If the next transformation is not uniquely defined continue.
        while len(to_apply) != 0:
            name = to_apply[0]
            if self.parameters.varies(name):
                break #
            from_grid = from_grid.convert(**{name: self.parameters[name]})
            to_apply = to_apply[1:]

        # Run the subspectra
        if len(self) != 0:
            pass_on_renormalize = (renormalize and self.parameters.varies('poly_params'))
            res = [child.compute_fit(to_apply, from_grid, pass_on_renormalize) for child in self]
            # renormalize the spectrum if this is the first spectrum with a unique 'poly_params'
            if renormalize and not self.parameters.varies('poly_params'):
                self.renormalize(theoretical=res)
            return res # returns list of subspectra

        # Return the observed + computed spectrum.
        arr = sp.zeros(self.observed().shape, dtype=[(dname, 'f8') for dname in ['wavelength', 'flux', 'error', 'theoretical', 'offset', 'chi', 'maxchi']] + [('mask', 'bool')])
        for dname in self.observed().dtype.names:
            if dname in arr.dtype.names:
                arr[dname] = self.observed()[dname]
        arr['theoretical'] = from_grid.convert(new_wavelength=self.observed()['wavelength']).flux
        arr['offset'] = self.observed()['flux'] - arr['theoretical']
        arr['chi'] = arr['offset'] / self.observed()['error']
        if renormalize:
            self.renormalize(theoretical=arr)
        return arr

    def chisq(self, renormalize=False, reduced=False):
        """Compute the chi-squared of the current fit.

        Arguments:
        - `renormalize`: renormalizes the spectrum while calculating the chi-squared.
        - `reduced`: If True, return the reduced chi-squared.
        """
        theoretical = self.compute_fit(renormalize=renormalize)
        if isinstance(theoretical, list):
            theoretical = reduce(sp.append, flatten(theoretical))
        chisq = sp.sum(sp.amin((abs(theoretical['chi']), theoretical['maxchi']), 0)[~theoretical['mask']] ** 2)
        if reduced:
            chisq = chisq / self.datasize
        return chisq

    def optimize(self, to_vary=('vsini', 'vrad', 'veiling', 'Teff', 'logg'), method='de', renormalize=True, maxFunEvals=5e3, **kwargs):
        """Optimizes the fit by minimizing the chi-squared over the non-masked region.

        To efficiently fit the parameters, the following steps are taken:
        1. Find the highest level at which the parameters are kept constant and optimize at that level. For example if we call multiple_stars.optimize(('vsini', 'vrad')) and 'vsini' can be different for every star and 'vrad' for every epoch, every star will be fitted individually.
        2. Collect all parameters that will be varied. In the example above this will be a single vsini from the star being fitted, and a list of vrad for every epoch.
        3. Fit these parameter to minimize the chi-squared over all subfits.

        Arguments:
        - `to_vary`: Names of the parameters that will be fitted.
        - `method`: Minimization routine to be used (from scipy.optimize). Provides support for ['tnc', 'l_bfgs_b', 'de']
        - `renormalize`: If True, renormalizes the spectra before computing the chi-squared.
        - `maxFunEvals`: maximum number of function evaluations for the differential evolution (method='de'). This will generally be the thing stopping the optimization
        """
        if all([self.parameters.varies(param_name) for param_name in to_vary]):
            for child in self:
                child.optimize(to_vary, method, renormalize, maxFunEvals, **kwargs)
            return
        variables, bounds = self.parameters.helper_variables(to_vary, box_bounds=(method == 'de'))[:2]
        print variables, bounds
        if method == 'tnc':
            res = sp.optimize.fmin_tnc(self.helper_fmin, variables, bounds=bounds, approx_grad=True, args=(to_vary, renormalize), **kwargs)
        elif method == 'l_bfgs_b':
            res = sp.optimize.fmin_l_bfgs_b(self.helper_fmin, variables, approx_grad=True, bounds=bounds, args=(to_vary, renormalize), **kwargs)
        elif method == '':
            res = sp.optimize.fmin(self.helper_fmin, variables, args=(to_vary, renormalize), **kwargs)
        elif method == 'de':
            from openopt import GLP
            lower_bounds, upper_bounds = zip(*bounds)
            solver = GLP(self.helper_fmin, lb=lower_bounds, ub=upper_bounds, maxIter=500, maxFunEvals=maxFunEvals, args=(to_vary, renormalize))
            res = solver.solve('de', baseVectorStrategy='best', seed=int(sp.rand() * 1e10), **kwargs)
        else:
            raise ValueError('Minimization routine %s not defined' % method)
        self.parameters.move_to_grid()
        return res

    def optimize_npoly(self, start_npoly=None, criterion='BIC', reoptimize=False, optimize_kwargs={}, max_npoly=sp.infty):
        """Sets the polynomial degree (`npoly`) to its best value and returns the value.

        The best value is defined by the Bayes (or Schwarz) Information Criterion ('BIC') or the Aikake Information Criterion ('AIC')
        Arguments:
        - `start_npoly`: polynomial dimension to start with.
        - `criterion`: The criterion to use to define the best `npoly` ('BIC' or 'AIC').
        - `reoptimize`: if True: refit the parameters after changing the  polynomial degree.
        - `optimize_kwargs`: keywords to pass on to SpectralFit.optimize (only used if `reoptimize` is True).
        - `max_npoly`: maximum polynomial degree.
        """
        if self.parameters.varies('poly_params'):
            return [child.optimize_npoly(start_npoly, criterion, reoptimize, optimize_kwargs) for child in self]
        if start_npoly is None:
            start_npoly = len(self.parameters['poly_params']) - 1
        npoly = start_npoly
        self.renormalize(npoly=npoly)
        old_chisq = self.chisq()
        for walk in (1, -1):
            if walk == -1 and npoly > start_npoly:
                break
            while True:
                if (npoly + walk < 0) or (walk + npoly > max_npoly):
                    break
                self.renormalize(npoly=npoly + walk)
                if reoptimize:
                    self.optimize(**optimize_kwargs)
                new_chisq = self.chisq()
                if criterion == 'BIC':
                    old_crit = old_chisq + npoly * sp.log(self.datasize)
                    new_crit = new_chisq + (npoly + walk) * sp.log(self.datasize)
                elif criterion == 'AIC':
                    old_crit = old_chisq + 2 * npoly
                    new_crit = new_chisq + 2 * (npoly + walk)
                if new_crit < old_crit:
                    old_chisq = new_chisq
                    npoly = npoly + walk
                else:
                    break
        self.renormalize(npoly)
        return npoly

    def hessian(self, param_names, renormalize=True):
        """Computes a matrix of second derivatives of the chi-squared with respect to the given parameters.

        Arguments:
        - `param_names`: list of parameters for which the derivative should be computed.
        - `renormalize`: if True renormalize the spectrum in every chi-squared calculation.
        """
        center, bounds, step, names = self.parameters.helper_variables(param_names)
        ndim = len(center)
        values = list(center)
        result = sp.zeros((ndim, ndim))

        for ix1, (val1, step1) in enumerate(zip(center, step)):
            for direction1 in (-1, 1):
                values[ix1] = val1 + step1 * direction1
                gradient = sp.zeros(ndim)
                reference = list(values)
                for ix2, (val2, step2) in enumerate(zip(center, step)):
                    if ix2 > ix1:
                        continue
                    for direction2 in (-1, 1):
                        values[ix2] = reference[ix2] + step2 * direction2
                        if direction2 == -1:
                            prev_value = self.helper_fmin(values, param_names, renormalize=renormalize)
                        else:
                            this_value = self.helper_fmin(values, param_names, renormalize=renormalize)
                            gradient[ix2] = (this_value - prev_value) / (2 * step2)
                    values[ix2] = reference[ix2]
                if direction1 == -1:
                    prev_gradient = gradient
                else:
                    result[ix1, :] = (gradient - prev_gradient) / (2 * step1)
                values[ix1] = val1
        self.parameters.helper_variables(param_names, new_values=center)
        for ix1 in range(ndim):
            for ix2 in range(ndim):
                if ix2 > ix1:
                    result[ix1, ix2] = result[ix2, ix1]
        return result

    def emcee(self, param_names, nwalkers=100, nsteps=100, renormalize=True):
        """Run a Markov Chain Monte Carlo simulation.

        Requires the `emcee` package to be installed.

        Arguments:
        `param_names`: name of the parameters to be varied in the MCMC.
        `nwalkers`: number of MCMC walkers to use (see emcee documentation).
        `nsteps`: Number of steps to compute.
        `renormalize`: If True renormalizes the spectra whenever the chi-squared is computed.
        """
        import emcee
        p0_1d, bounds, step, names = self.parameters.helper_variables(param_names)
        ndim = len(p0_1d)
        param0 = [sp.randn(ndim) * sp.array(step) + sp.array(p0_1d) for i in range(nwalkers)]
        # make sure initial values are inside the bounds
        for p0_walker in param0:
            for ixbound, bound in enumerate(bounds):
                if bound[0] is not None and p0_walker[ixbound] < bound[0]:
                    p0_walker[ixbound] = 2 * bound[0] - p0_walker[ixbound]
                if bound[1] is not None and p0_walker[ixbound] > bound[1]:
                    p0_walker[ixbound] = 2 * bound[1] - p0_walker[ixbound]

        lnprob = lambda params: -0.5 * self.helper_fmin(params, param_names, renormalize=renormalize)
        sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob)
        sampler.run_mcmc(param0, nsteps)
        try:
            result = sp.zeros(sampler.chain.shape[:-1], dtype=[(name, 'f8') for name in names + ['chisq']])
            for index in sp.ndindex(*result.shape):
                for ixname, name in enumerate(names):
                    result[index][name] = sampler.chain[index + (ixname, )]
                result[index]['chisq'] = -2. * sampler.lnprobability[index]
            self.res_emcee = result
        finally:
            self.parameters.helper_variables(param_names, new_values=p0_1d)
        return result

    def emcee_params(self, res_emcee=None, ixburnin=100):
        """Returns the mean, median, and standard deviation for the parameters from the last emcee run.

        Parameters:
        - `res_emcee`: Override the last emcee run with the given values.
        - `ixburnin`: number of initial emcee steps to ignore, when computing the parameter values and uncertainties.
        """
        if res_emcee == None:
            res_emcee = self.res_emcee
        return dict([(param_name,
                      dict([(oper_name, oper(res_emcee[param_name][:, ixburnin:].flatten())) for oper_name, oper in [('mean', sp.mean), ('median', sp.median), ('std', sp.std)]])
                      ) for param_name in res_emcee.dtype.names])

    def helper_fmin(self, params, param_names, renormalize=True):
        """Returns the chi-squared for a specific set of parameters.

        Used by self.optimize.

        Arguments:
        - `params`: The parameter values.
        - `param_names`: The name of the parameters being varied.
        - `renormalize`: If True renormalizes the spectrum before returning the fit.
        """
        self.parameters.helper_variables(param_names, new_values=list(params))
        for always_positive in ('veiling', 'vsini', 'vsiniA', 'vsiniB'):
            if always_positive not in self.parameters.keys():
                continue
            if self.parameters.varies(always_positive):
                if (sp.array(flatten(self.parameters[always_positive])) < 0.).any():
                    return sp.infty
                if (sp.array(flatten(self.parameters[always_positive])) > 1e4).any():
                    return sp.infty
            else:
                if self.parameters[always_positive] < 0.:
                    return sp.infty
                if self.parameters[always_positive] > 1e4:
                    return sp.infty
        try:
            if renormalize:
                self.renormalize()
            return self.chisq()
        except model_spectra.GridError:
            return sp.infty

    def cross_correlate(self, ):
        """Set the radial velocity to a very rough estimate from a cross-correlation with the current model spectrum.
        """
        if self.parameters.varies('vrad'):
            for subfit in self:
                subfit.cross_correlate()
        else:
            vrad = []
            for subfit in self.iter_spectra():
                spectrum = subfit.compute_fit(('vsini', ), renormalize=True)
                obs_flux = 1. - spectrum['flux'] / sp.median(spectrum['flux'])
                theor_flux = 1. - spectrum['theoretical'] / sp.median(spectrum['theoretical'])
                obs_flux[spectrum['mask']] = 0.
                correlation = sp.correlate(obs_flux, theor_flux, 'same')
                corrected = correlation - ndimage.filters.median_filter(correlation, 50)
                ixmax = sp.argmax(corrected)
                if ixmax == 0 or ixmax == len(corrected) - 1 or sp.sum(spectrum['mask']) > 2000:
                    continue
                else:
                    sp.polyfit(sp.arange(-1, 2, 1), corrected[ixmax - 1: ixmax+2], 2)
                    a, b, c = sp.polyfit(sp.arange(-1, 2, 1), corrected[ixmax - 1: ixmax+2], 2)
                    pix_off = ixmax - b / (2 * a) - corrected.size / 2.
                if abs(pix_off) > 100:
                    continue
                resolution = sp.median(spectrum[1:]['wavelength'] / (spectrum[1:]['wavelength'] - spectrum[:-1]['wavelength']))
                vrad.append(pix_off / resolution * 2.9979e5)
            self.parameters['vrad'] = sp.median(vrad) if len(vrad) != 0 else 0

    @property
    def index(self, ):
        """The index of the subfit in the total fit.

        Returns a tuple of indices which can be used to retrieve this subfit from the total fit:
        self = total_fit[index1][index2]...
        """
        if self.parent is None:
            return ()
        return self.parent.index + (self.parent._children.index(self), )

    @property
    def datasize(self, ):
        """Returns the number of data points in the spectrum (-a).
        """
        if len(self) == 0:
            return sp.sum(~self.observed()['mask'])
        else:
            return sp.sum([child.datasize for child in self])

    @property
    def depth(self, ):
        """Returns the depth of the subfit in the total fit.
        """
        return len(self.index)

    @property
    def npoly(self, ):
        """Degree of polynomial in continuum fit.
        """
        if self.parameters.varies('poly_params'):
            return [child.npoly for child in self]
        else:
            return len(self.parameters['poly_params'])

    def meta(self, name=None):
        """Returns the information from the meta files for `name`.

        If all subfits have the same value for `name`, only a single value is returned.

        Default: return dictionary with all meta data.
        """
        if name is None:
            name_list = []
            for spectrum in self.iter_spectra():
                for key in spectrum.observed().meta.keys():
                    if key not in name_list:
                        name_list.append(key)
            return dict([(single_name, self.meta(single_name)) for single_name in name_list])
        if len(self) == 0:
            if name not in self.observed().meta.keys():
                return sp.nan
            return self.observed().meta[name]
        meta_list = [subfit.meta(name) for subfit in self]
        if any([isinstance(sub, list) for sub in meta_list]):
            return meta_list
        if sp.unique(meta_list).size == 1:
            return meta_list[0]
        return meta_list

    def set_default(self, ):
        """Sets the parameters to the APOGEE pipeline values from the fits headers.

        Any parameters for which no value in the header is found (e.g. vsini and veiling) are set to zero.
        The parameter defining the spectrum to be extracted from the theoretical grid are adjusted to lie inside the grid.
        The resolution for APOGEE spectra will be set to 2.25e4
        The polynomial is renormalized.
        """
        if len(self) != 0:
            for child in self:
                child.set_default()
            return
        defaults = {'resolution': 2.25e4,
                    'poly_params': sp.array([1] + [0] * 2)}
        for name in self.parameters.possible_keys:
            if name.upper() in self.observed().meta.keys():
                self.parameters[name] = self.observed().meta[name.upper()]
            elif name == 'vrad' and 'VHELIO' in self.observed().meta.keys():
                self.parameters[name] = self.observed().meta['VHELIO']
            elif name in defaults.keys():
                self.parameters[name] = defaults[name]
            else:
                self.parameters[name] = 0.
            if name in self.grid.variable_names:
                self.parameters[name] = sp.median(self.grid.variable(name))
                if name == 'Teff':
                    self.parameters[name] = 4500.
                elif name == 'logg':
                    self.parameters[name] = 4.
                elif name in ['FeH', 'alpha']:
                    self.parameters[name] = 0.
        self.renormalize()
        return

    def plot(self, ):
        import plot
        if isinstance(self.meta('OBJID'), list):
            return plot.FullPlotter(self)
        elif isinstance(self.meta('HJD'), list) or len(self) == 1:
            return plot.StarPlotter(self)
        else:
            return plot.EpochPlotter(self)

    # Turn into a tuple-like object
    def __len__(self, ):
        """Returns the number of subfits.

        Return zero, if SpectralFit contains a single spectrum and no further subfits.

        Result is returned with len(self)
        """
        if self._children is None:
            return 0
        return len(self._children)

    def __iter__(self, ):
        """Iterate over the subfits.

        Do not call directly, rather use:
        >>> for subfit in self:
        >>>     do something
        """
        if len(self) == 0:
            return
        for child in self._children:
            yield child

    def __getitem__(self, index):
        """Returns a subfit defined by it's numeric index (if index is a number) or the stellar target name (if index is a string).

        Called with subfit = self[index]
        """
        if self._children is None:
            raise IndexError('SpectralFit object %s has no subfits' % str(self))
        if isinstance(index, basestring):
            index = self.meta('OBJID').index(index)
        return self._children[index]

    def iter_spectra(self, ):
        """iterate over all the sub-subfits at the lowest depths.

        run as:
        >>> for chip in SpectralFit.iter_spectra():
        >>>     do something
        """
        if len(self) == 0:
            yield self
        else:
            for subfit in self:
                for base in subfit.iter_spectra():
                    yield base

    # I/O functions
    def save(self, filename):
        """Saves the current fit to file.

        Arguments:
        - `filename`: Name of file to store the current fit. If the name matches an existing directory, the file will be stored in that directory with name <stellar-name>.pickle if all epochs of a star are fitted or <stellar_name>_<time_of_observation>.pickle if a single epoch is fitted.
        """
        if os.path.isdir(filename):
            if isinstance(self.meta('OBJID'), list):
                filename = os.path.join(filename, 'multiple_stars.pickle')
            elif isinstance(self.meta('JD-MID'), list) or len(self) == 1:
                filename = os.path.join(filename, '%s.pickle' % self.meta('OBJID'))
            else:
                filename = os.path.join(filename, '%s_%i.pickle' % (self.meta('OBJID'), 100 * self.meta('JD-MID')))
                
        ndata = sp.sum([sp.sum(~spec.observed()['mask']) for spec in self.iter_spectra()])
        with open(filename, 'w') as f:
            cPickle.dump({'parameters': dict(self.parameters),
                          'objid': self.meta('OBJID'),
                          'jd-mid': self.meta('JD-MID'),
                          'emcee': self.res_emcee,
                          'meta': self.meta(None),
                          'chisq': self.chisq(),
                          'npoly': self.npoly,
                          'ndata': ndata}, f)

    def load(self, filename):
        """Load the current fit from a file.

        Arguments:
        - `filename`: Name of file to be loaded. If the name matches an existing directory, the file will be loaded in that directory with name <stellar-name>.pickle or 'multiple_stars.pickle'.
        """
        if os.path.isdir(filename):
            if isinstance(self.meta('OBJID'), list):
                full_file = os.path.join(filename, 'multiple_stars.pickle')
                if os.path.isfile(full_file):
                    self.load(full_file)
                else:
                    for star_name, subfit in zip(self.meta('OBJID'), self):
                        try:
                            subfit.load(os.path.join(filename, '%s.pickle' % star_name))
                        except IOError as e:
                            print 'in SpectralFit.load: skipping load of %s due to IOError:' % star_name, e
            else:
                self.load(os.path.join(filename, '%s.pickle' % self.meta('OBJID')))
        else:
            with open(filename, 'r') as pickle_file:
                params = cPickle.load(pickle_file)
            if self.meta('OBJID') != params['objid']:
                raise IOError('Target name(s) do not match those in %s' % filename)
            if self.meta('JD-MID') != params['meta']['JD-MID']:
                if isinstance(params['meta']['JD-MID'], list) and len(self.meta('JD-MID')) == len(params['meta']['JD-MID']) and all([epoch in params['meta']['JD-MID']] for epoch in self.meta('JD-MID')):
                    self._children = [self[params['meta']['JD-MID'].index(epoch)] for epoch in self.meta('JD-MID')]
                    print 'SpectralFit.load: When loading previous fit results, had to change the order in which the epochs are listed for %s' % params['objid']
                    assert self.meta('JD-MID') == params['meta']['JD-MID'], 'after resorting the children the parameters should match the loaded values'
                else:
                    raise IOError('Epoch dates do not match those in %s' % filename)
            parameters = params['parameters']
            self.parameters.update(parameters)
            if params['emcee'] is not None:
                self.res_emcee = params['emcee'][:, 200::4]


def flatten(item_list):
    """flattens a list of list of arbitrary depth into a single list containing all the elements.
    """
    out = []
    for item in item_list:
        if isinstance(item, list):
            out.extend(flatten(item))
        else:
            out.append(item)
    return out

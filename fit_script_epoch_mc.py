"""Script to fit individual epochs

The spectra are read from specdir, the (Coelho) grid is read from griddir, and the results are saved in a pickle file in resultdir.

During optimization the effective temperature, log(g), veiling and v sin(i) are fitted, using a constant value across all epochs. For every epoch a different radial velocity is fitted.

This script can be called without options (python fit_script.py) to fit all the stars.
Calling the scipt with a stellar name (python fit_script.py <star_name>) will cause only the spectra observed for that star to be fit.
"""
import matplotlib
matplotlib.use('PDF')
import fit
import model_spectra
import scipy as sp
import os
import scipy.optimize
import scipy.stats
import time
import copy

specdir = '/cluster/work/scr2/mcottaar/apogee/spectra'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/coelho'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/apg_rvsynthgrid_v2.fits'
griddir = '/cluster/work/scr2/mcottaar/apogee/grid/BT-Settlgrid.npz'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/ESO-Gaiagrid.npz'
inputdir = '/cluster/work/scr2/mcottaar/apogee/fits'
resultdir = '/cluster/work/scr2/mcottaar/apogee/fits_mc'
pdfdir = '/cluster/work/scr2/mcottaar/apogee/pdf'

name_grid = 'Coelho'
#name_grid = 'RV'
#name_grid = 'ATLAS'
name_grid = 'BT-Settl'
#name_grid = 'ESO-GAIA'

def main(name=None, epoch=0):
    """Fit the given star and saves the result"""
    for ix in range(10):
        try:
            # retry loading 10 times, because loading the header files sometimes gives an error if the file is already in use due to bug(?) in sp.load
            tofit = fit.load_fit(specdir, grid, star_name=name, mask_hydrogen=0)
        except ValueError as error:
            time.sleep(10)
        else:
            break
    else:
        raise error
    print 'epoch %i from %i' % (epoch, len(tofit))
    tofit = tofit[epoch]
    tofit.load(os.path.join(inputdir, '%s_%s.pickle' % (name, str(epoch))))

    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='', renormalize=True, maxiter=10000, maxfun=10000)
    print 'chi-squared after local Nelder-Mead optimization', tofit.chisq(renormalize=True)

    print 'best number of polynomial dimensions', tofit.optimize_npoly(max_npoly=6)
    print 'chi-squared after adjusting polynomial dimension', tofit.chisq()
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='', renormalize=True, maxiter=10000, maxfun=10000)
    print 'chi-squared after local Nelder-Mead optimization', tofit.chisq()

    tofit.emcee(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), nwalkers=200, nsteps=300, renormalize=True)
    tofit.renormalize()
    tofit.save(os.path.join(resultdir, '%s_%s.pickle' % (name, str(epoch))))


if __name__ == '__main__':
    sp.random.seed(78961)
    print 'loading %s grid' % name_grid
    if name_grid == 'Coelho':
        grid = model_spectra.coelho(griddir).at_value('FeH', 0.).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.)).convert_spectra(resolution=22500, new_wavelength=15100. * sp.exp(sp.arange(36e3)/ 3e5))
    elif name_grid == 'RV':
        grid = model_spectra.RVgrid(griddir).at_value('alpha', 0).at_value('carbon', 0)
    elif name_grid in ['ESO-GAIA', 'BT-Settl']:
        grid = model_spectra.pre_loaded(griddir)
    import sys
    main(sys.argv[1], int(sys.argv[2]))



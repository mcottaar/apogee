"""Script to fit the multiple epoch spectra of one or all stars.

The spectra are read from specdir, the (Coelho) grid is read from griddir, and the results are saved in a pickle file in resultdir.

During optimization the effective temperature, log(g), veiling and v sin(i) are fitted, using a constant value across all epochs. For every epoch a different radial velocity is fitted.

This script can be called without options (python fit_script.py) to fit all the stars.
Calling the scipt with a stellar name (python fit_script.py <star_name>) will cause only the spectra observed for that star to be fit.
"""
import matplotlib
matplotlib.use('PDF')
import fit
import model_spectra
import scipy as sp
import os
import scipy.optimize
import scipy.stats
import time
import copy

specdir = '/cluster/work/scr2/mcottaar/apogee/spectra'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/coelho'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/apg_rvsynthgrid_v2.fits'
griddir = '/cluster/work/scr2/mcottaar/apogee/grid/BT-Settlgrid.npz'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/ESO-Gaiagrid.npz'
resultdir = '/cluster/work/scr2/mcottaar/apogee/fits'
pdfdir = '/cluster/work/scr2/mcottaar/apogee/pdf'

name_grid = 'Coelho'
#name_grid = 'RV'
#name_grid = 'ATLAS'
name_grid = 'BT-Settl'
#name_grid = 'ESO-GAIA'

def main(name=None):
    """Fit the given star and saves the result"""
    for ix in range(10):
        try:
            # retry loading 10 times, because loading the header files sometimes gives an error if the file is already in use due to bug(?) in sp.load
            tofit = fit.load_fit(specdir, grid, star_name=name, mask_hydrogen=0)
        except ValueError as error:
            time.sleep(10)
        else:
            break
    else:
        raise error

    if name_grid == 'RV':
        bounds = [(15180, 15780), (15895, 16400), (16510, 16925)]
        for subfit in tofit.iter_spectra():
            spectrum = subfit.observed()
            tomask = ~reduce(sp.logical_or, [(spectrum['wavelength'] > low) & (spectrum['wavelength'] < high) for low, high in bounds])
            spectrum['mask'] = tomask | spectrum['mask']
    #for parameter in ('vrad', 'Teff', 'logg', 'vsini', 'veiling'):
    #    tofit.parameters.make_variable(parameter)
    best_chisq = sp.infty
    for index in range(1):
        tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='de', maxFunEvals=2e4, renormalize=True)
        print 'chi-squared after global differential evolution (%i)' % index, tofit.chisq()
        if tofit.chisq() < best_chisq:
            best_fit = copy.deepcopy(dict(tofit.parameters))
            best_chisq = tofit.chisq()
    tofit.parameters.update(best_fit)
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='', renormalize=True, maxiter=10000, maxfun=10000)
    print 'chi-squared after local Nelder-Mead optimization', tofit.chisq(renormalize=True)
    best_fit = copy.deepcopy(dict(tofit.parameters))
    best_chisq = tofit.chisq(renormalize=True)
    tofit.cross_correlate()
    tofit.parameters['veiling'] = [0.1] * len(tofit)
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), renormalize=True, maxiter=10000, maxfun=10000)
    print 'chi-squared after cross-correlation', tofit.chisq(renormalize=True)
    if tofit.chisq(renormalize=True) < best_chisq:
        best_fit = copy.deepcopy(dict(tofit.parameters))
        best_chisq = tofit.chisq()
    else:
        tofit.parameters.update(best_fit)
    tofit.parameters['veiling'] = [0.1] * len(tofit)
    tofit.parameters['vrad'] = [sp.median(tofit.parameters['vrad'])] * len(tofit)
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), renormalize=True, maxiter=10000, maxfun=10000)
    print 'chi-squared after median velocity set', tofit.chisq(renormalize=True)
    if tofit.chisq(renormalize=True) < best_chisq:
        best_fit = copy.deepcopy(dict(tofit.parameters))
        best_chisq = tofit.chisq()
    else:
        tofit.parameters.update(best_fit)
    print 'best number of polynomial dimensions', tofit.optimize_npoly(max_npoly=6)
    print 'chi-squared after adjusting polynomial dimension', tofit.chisq()
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='', renormalize=True, maxiter=10000, maxfun=20000)
    print 'chi-squared after local Nelder-Mead optimization', tofit.chisq()

    tofit.emcee(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), nwalkers=200, nsteps=300, renormalize=True)
    tofit.renormalize()
    tofit.save(resultdir)
    toplot = tofit.plot()
    toplot.multiplot('plot_spectrum', os.path.join(pdfdir, '%s_spectrum.pdf' % name))
    if len(tofit) > 1:
        toplot.multiplot('plot_rms_binarity', os.path.join(pdfdir, '%s_rms.pdf' % name), chisq=True, normalized=True, fluxlim=(0., 1.5))
    toplot.multiplot('plot_emcee', os.path.join(pdfdir, '%s_emcee.pdf' % name))


if __name__ == '__main__':
    sp.random.seed(78961)
    print 'loading %s grid' % name_grid
    if name_grid == 'Coelho':
        grid = model_spectra.coelho(griddir).at_value('FeH', 0.).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.)).convert_spectra(resolution=22500, new_wavelength=15100. * sp.exp(sp.arange(36e3)/ 3e5))
    elif name_grid == 'RV':
        grid = model_spectra.RVgrid(griddir).at_value('alpha', 0).at_value('carbon', 0)
    elif name_grid in ['ESO-GAIA', 'BT-Settl']:
        grid = model_spectra.pre_loaded(griddir)
    import sys
    if len(sys.argv) == 1:
        main()
    else:
        main(sys.argv[1])

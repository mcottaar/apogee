import scipy as sp
import os
import scipy.ndimage
import fit
import model_spectra
import matplotlib.pyplot as plt
from scipy import stats


class RunNotFoundError(IOError):
    """raised when a run was not found"""


def allruns(directory='.', grids=['coelho', 'RV', 'btsettl', 'gaiaeso']):
    """Returns all runs stored in given directory.

    Only include stars which are included in all runs and ensure they are in the same order

    Arguments:
    - `directory`: Base directory, which contains a "grid" subdirectory with model spectral grids, a "spectra" directory with the APOGEE spectra, and several *.npz files containing the results of the spectral fits.
    """
    runs = []
    for grid in grids:
        for masked_hydrogen in [False, True]:
            for vary_params in [False, True]:
                try:
                    runs.append(load_run(grid, masked_hydrogen, vary_params, directory=directory))
                except RunNotFoundError:
                    pass
    all_objid = reduce(sp.intersect1d, [arr['objid'] for arr in runs])
    get_correct_arr = lambda full_arr: full_arr[[sp.where(objid == full_arr['objid'])[0][0] for objid in all_objid]]
    return MultiFits([get_correct_arr(arr) for arr in runs])


def load_run(grid_name='coelho', masked_hydrogen=False, vary_params=True, per_epoch=False, directory='.'):
    """Returns the results from a run of spectral fits.

    Arguments:
    - `grid_name`: used grid; choose from ['coelho', 'RV']
    - `masked_hydrogen`: True if hydrogen was masked.
    - `vary_params`: True if parameters were allowed to vary per epochs, otherwise all parameters were constant across multiple epochs (except the RV).
    - `per_epoch`: True to an array with one stellar spectrum per line, False to return an array with one star per line.
    - `directory`: directory containing the best fits
    """
    print grid_name, masked_hydrogen, vary_params
    filename = os.path.join(directory, '%s%s%s.npy' % (grid_name, '_maskedhydro' if masked_hydrogen else '', '_vary' if vary_params else ''))
    if not os.path.isfile(filename):
        raise RunNotFoundError('The run with grid %s (with %smasked hydrogen and parameters varied per %s) was not found. It either was not run or not stored in %s.' % (grid_name, '' if masked_hydrogen else 'no ', 'epoch' if vary_params else 'star', filename))

    if grid_name == 'coelho':
        grid = model_spectra.coelho(os.path.join(directory, 'grid')).at_value('FeH', 0).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.)).convert_spectra(resolution=22500, new_wavelength=15000 * sp.exp(sp.arange(1e4) / 70000.))
    elif grid_name == 'RV':
        grid = model_spectra.RVgrid(os.path.join(directory, 'grid/apg_rvsynthgrid_v2.fits')).at_value('alpha', 0).at_value('carbon', 0)
    elif grid_name == 'btsettl':
        grid = model_spectra.pre_loaded(os.path.join(directory, 'grid', 'BT-Settlgrid.npz'))
    elif grid_name == 'phoenix':
        grid = model_spectra.pre_loaded(os.path.join(directory, 'grid', 'Phoenix.npz'))
    elif grid_name == 'gaiaeso':
        grid = model_spectra.pre_loaded(os.path.join(directory, 'grid', 'ESO-Gaiagrid.npz'))
    else:
        grid = None

    arr = load_parameters(filename, per_epoch).view(KnownStellarParameters)
    arr.specdir = 'spectra'
    arr.grid = grid
    arr.grid_name = grid_name
    arr.masked_hydrogen = masked_hydrogen
    arr.vary_params = vary_params
    return arr


def load_parameters(filename='per_star.npy', per_epoch=False):
    """load the parameters from a 'per_star.npy'-file.

    Arguments:
    - `filename`: name of the file to be loaded.
    - `per_epoch`: returns array with one epoch per line, rather than one star.
    """
    arr_load = sp.load(filename)
    if per_epoch:
        nstars = [1 if isinstance(dates, float) else len(dates) for dates in arr_load['date']]
        arr = sp.zeros(sp.sum(nstars), arr_load.dtype)
        for name in arr_load.dtype.names:
            as_list = []
            for nstar, item in zip(nstars, arr_load[name]):
                if not isinstance(item, list):
                    item = [item] * nstar
                as_list.extend(item)
            arr[name] = as_list
    else:
        arr = arr_load
    result = arr.view(AllParameters)
    result.filename = filename
    #subgroup = {'NGC 1333': (arr['h_RA'] < 54) & (arr['h_DEC'] > 29.) & ((arr['h_TARG2'] & 2 ** 13) != 0),
    #            'IC 348': (arr['h_RA'] > 54) & (arr['h_RA'] < 60) & (arr['h_DEC'] > 29.) & ((arr['h_TARG2'] & 2 ** 13) != 0),
    #            'Control': ((arr['h_TARG2'] & 2 ** 13) == 0) & ((arr['h_TARG2'] & 2 ** 10) == 0),
    #            'NGC 2264': (arr['h_RA'] > 95) & (arr['h_DEC'] < 13.) & ((arr['h_TARG2'] & 2 ** 13) != 0),
    #            'Pleiades': (arr['h_RA'] < 60) & (arr['h_DEC'] < 29.) & ((arr['h_TARG2'] & 2 ** 10) != 0)}
    #result.cluster = sp.zeros(result.size, dtype='S20')
    #for cluster_name, cluster in subgroup.items():
    #    result.cluster[cluster] = cluster_name

    # corrections
    for name in ['a_Teff', 'a_logg', 'a_FTeff', 'a_Flogg']:
        if name in result.dtype.names:
            result[name][result[name] < 0] = sp.nan
    return result



class StellarParameters(sp.recarray):
    def get_subarray(self, catagory):
        """Get all columns in the array starting with `catagory` + '_'.

        For example: get_subarray('l') returns all the literature values
        """
        dtype_new = [(subdt[0][len(catagory) + 1:], ) + subdt[1:] for subdt in self.dtype.descr if subdt[0][:len(catagory) + 1] == catagory + '_']
        arr_new = sp.zeros(self.size, dtype=dtype_new)
        for name in arr_new.dtype.names:
            arr_new[name] = self[catagory + '_' + name]
        return arr_new

    @property
    def groups(self, ):
        return tuple(sp.unique([name.split('_')[0] for name in self.dtype.names if '_' in name]))

    def _as_star_arr(self, parameter):
        """Returns parameter as an array for every star.

        If parameters is a string, simply returns self[parameter]
        If parameter is an array, check if it matches the expected size and return.
        """
        if isinstance(parameter, basestring):
            return self[parameter]
        else:
            parameter = sp.asarray(parameter)
            if parameter.size == self.size:
                return parameter
            raise ValueError("Parameter array size %i does not match stellar array size %i" (parameter.size, self.size))

    def get_median(self, parameter):
        """Gets the median value of given parameter.
        """
        return sp.array([sp.median(sp.array(value, dtype='f8')) for value in self._as_star_arr(parameter)])

    def get_std(self, parameter):
        """Gets the standard deviation of the variability in given parameter.
        """
        return sp.array([sp.std(sp.array([value], dtype='f8'), ddof=1) for value in self._as_star_arr(parameter)])

    def get_mean(self, parameter):
        """Gets the mean value of given parameter.
        """
        return sp.array([sp.mean(sp.array(value, dtype='f8')) for value in self._as_star_arr(parameter)])

    def get_index(self, parameter, index=0):
        """Get the value of the epoch `index`
        """
        return sp.array([value[index] if isinstance(value, list) else value for value in self._as_star_arr(parameter)])

    def get_cluster(self, cluster_name):
        """Returns all stars belonging to given cluster

        cluster should be one of: ('Control', 'IC 348', 'NGC 1333', 'NGC 2264', 'Pleiades')
        """
        return self[self['cluster'] == cluster_name]

    def per_epoch(self, parameter=None):
        """Returns an array containing the values of a parameter sorted per epoch.

        Parameters:
        - `parameter`: Either the name of the parameter (string) or an array of the parameter. If not set returns array with all parameters per epoch.
        """
        if parameter is None:
            nepochs = sp.sum(self['nepochs'])
            parameters_all = [(name, self.per_epoch(name)) for name in self.dtype.names]
            parameters = [(name, values) for name, values in parameters_all if len(values) == nepochs]
            result = sp.zeros(nepochs, dtype=[(name, values.dtype) for (name, values) in parameters])
            for name, values in parameters:
                result[name] = values
            return result.view(type(self))
        return reduce(sp.append, [[param] * nep if (not isinstance(param, list) or len(param) != nep) else param for param, nep in zip(self._as_star_arr(parameter), self['nepochs'])])

    def per_star(self, ):
        """Returns an array containing the values of all parameters sorted per star
        """
        stars = sp.unique(self['objid'])
        dtype = self.dtype.descr.copy()
        for obj in stars:
            ixepochs = self['objid'] == obj
            for ixname, name in enumerate(self.dtype.names):
                if len(sp.unique(self[ixepochs][name])) > 1:
                    dtype[ixname] = 'object'
        result = sp.zeros(len(stars), dtype=dtype)
        for ixobj, obj in enumerate(stars):
            ixepochs = self['objid'] == obj
            for ixname, name in enumerate(self.dtype.names):
                if len(sp.unique(self[ixepochs][name])) > 1:
                    result[name][ixobj] = list(self[ixepochs][name])
                else:
                    result[name][ixobj] = self[ixepochs[0]][name]
            result['nepochs'][ixobj] = len(ixepochs)
        return result
        
    @property
    def plotter(self, ):
        return Plotter(self)

    def weighted(self, parameter):
        """Gets the weighted mean with uncertainty for parameter, that was a variable in MCMC.

        Argument:
        - `parameter`: variable in MCMC; one of ['veiling', 'vrad', 'vcorr', 'Teff', 'logg', 'vsini']
        """
        return self.weighted_helper(self['em_' + parameter], self['esc_' + parameter])

    @staticmethod
    @sp.vectorize
    def weighted_helper(value, uncertainty):
        value = sp.atleast_1d(value)
        uncertainty = sp.atleast_1d(uncertainty)
        weights = 1. / uncertainty ** 2.
        tot_weight = sp.sum(weights)
        return sp.sum(value * weights) / tot_weight, 1. / sp.sqrt(tot_weight)

    def norm_offset(self, parameter, corrected=False):
        """For every epoch compute the offset from the weighted mean of the other epochs normalized by the quadratic sum of the noise.

        Arguments:
        - `parameter`: parameter for which the offset is computed (without 'em_', 'es_', or 'esc_')
        - `corrected`: If True use the true rather than the MCMC uncertainties.
        """
        pre_noise = 'esc_' if corrected else 'es_'
        def extint_helper(elem):
            if isinstance(elem, tuple):
                elem = sp.array(elem, dtype=self.dtype)[0]
            if not isinstance(elem['em_%s' % parameter], list):
                return sp.nan
            weight = sp.array(elem[pre_noise + parameter]) ** -2
            weight_mean = sp.sum(weight)
            mean_param = sp.sum(weight * sp.array(elem['em_%s' % parameter])) / weight_mean
            mean_without = (mean_param * weight_mean - weight * sp.array(elem    ['em_%s' % parameter])) / (weight_mean - weight)
            var_without = 1. / (weight_mean - weight)
            offset = elem['em_%s' % parameter] - mean_without
            return list(offset / sp.sqrt(var_without + sp.array(elem[pre_noise + parameter]) ** 2))
        return sp.array(map(extint_helper, self), dtype='object')

    def epoch_to_epoch(self, parameter, corrected=False):
        """Calculates for given `parameter` the chi-squared for a model of a constant stellar paramter over all epochs.
        """
        pre_noise = 'esc_' if corrected else 'es_'
        def chisq_helper(elem):
            if isinstance(elem, tuple):
                elem = sp.array(elem, dtype=self.dtype)[0]
            if not isinstance(elem['em_%s' % parameter], list):
                return sp.nan
            weight = sp.array(elem[pre_noise + parameter]) ** -2
            weight_mean = sp.sum(weight)
            mean_param = sp.sum(weight * sp.array(elem['em_%s' % parameter])) / weight_mean
            return sp.sum((elem['em_%s' % parameter] - mean_param) ** 2 * weight)
        return sp.array(map(chisq_helper, self), dtype='f8')

    def prob_epoch_to_epoch(self, parameter, corrected=False):
        """Calculates for given `parameter` the probability of having such a high epoch-to-epoch variability due to noise"""
        return stats.chisqprob(self.epoch_to_epoch(parameter, corrected), self['nepochs'] - 1)

class AllParameters(StellarParameters):
    filename = None

    def __array_finalize__(self, obj):
        self.filename = getattr(obj, 'filename', None)

    literature = dict([('L03', 'Luhman et al. (2003)'), ('S93', 'Soderblom et al. (1993)'), ('S09', 'Soderblom et al. (2009)'),
          ('F06', 'Furesz et al. (2006)'), ('M09', 'Mermilliod et al. (2009)'), ('N06', 'Nordhagen et al. (2006)'),
          ('T00', 'Terndrup et al. (2000)'), ('2MA', '2 MASS'), ('B13', 'Bell et al. (2013)'),
          ('M07', 'Mayne et al. (2007)'), ('D08', 'Dahm (2008)'), ('B12', 'Bell et al. (2012)'),
          ('D13', 'Da Rio')])

    @property
    def best_fit(self, ):
        return self.get_subarray('v').view(StellarParameters)

    @property
    def mcmc_mean(self, ):
        return self.get_subarray('em').view(StellarParameters)

    @property
    def mcmc_std(self, ):
        return self.get_subarray('es').view(StellarParameters)

    @property
    def fits_header(self, ):
        return self.get_subarray('h').view(StellarParameters)

    @property
    def aspcap(self, ):
        return self.get_subarray('a').view(StellarParameters)

    def all_values(self, parameter):
        """Returns the measured + literature values for given parameter.

        Possible parameters are ('vrad', 'vsini')
        """
        possible_names = {'vrad': (('vrad', 'v_r', 'RV', 'VHELIO', 'HRV1'), ('evrad', 'sigRV', 'e_RV', 'eRV')),
                          'vsini': (('vsini', 'v_sini', 'Vsini'), ('evsini', 'e_Vsini')),
                          'Teff': (('Teff', 'T_eff'), ()),
                          'logg': (('logg', ), ()),
                          'veiling': (('veiling', ), ())}
        groups = ['ASPCAP', 'IN-SYNC', 'ESTVHELIO', 'SYNTHVHELIO', 'VHELIO', 'RV header'] + self.literature.values()
        result = {}
        for group in groups:
            result[group] = sp.zeros(self.size, dtype=[('value', 'f8'), ('uncertainty', 'f8')])
            result[group]['value'] = sp.nan
            result[group]['uncertainty'] = sp.nan
        for tostore, possible_names in zip(['value', 'uncertainty'], possible_names[parameter]):
            for name in possible_names:
                if 'em_' + name in self.dtype.names:
                    result['IN-SYNC']['value'], result['IN-SYNC']['uncertainty'] = self.weighted(name)
                if 'a_F' + name in self.dtype.names:
                    result['ASPCAP'][tostore] = self.get_median('a_F' + name)
                if 'h_' + name in self.dtype.names:
                    result['RV header'][tostore] = self.get_median('h_' + name)
                for literature, ixstore in self.literature.items():
                    if 'l_%s_%s' % (literature, name) in self.dtype.names:
                        use = self['l_%s' % literature]
                        result[ixstore][tostore][use] = self['l_%s_%s' % (literature, name)][use]
        if parameter == 'vrad':
            for name in ['ESTVHELIO', 'SYNTHVHELIO', 'VHELIO']:
                result[name]['value'] = self.get_median('v_' + name)
        return dict([(key, value) for key, value in result.items() if sp.sum(sp.isfinite(value['value'])) > 0])

    def combined_literature(self, name_list):
        """Returns array with all literature values available in name_list.
        """
        arr = sp.zeros(self.shape)
        arr[...] = sp.nan
        sources = sp.unique([name.split('_')[1] for name in self.dtype.names if name[:2] == 'l_'])
        for name in name_list:
            for source in sources:
                param_name = 'l_%s_%s' % (source, name)
                if param_name in self.dtype.names:
                    use = self['l_%s' % source]
                    arr[use] = self[param_name][use]
        return arr



    def photometry(self, band, intrinsic=False, offset=False, uncertainty=False):
        """Returns the measured photometry in given band.

        If `intrinsic` is True the intrinsic magnitude for a star with the same temperature and age at a distance and extinction level of the Pleiades is returned.
        """
        if isinstance(band, tuple):
            parts = [self.photometry(ba, intrinsic=intrinsic, offset=offset, uncertainty=uncertainty) for ba in band]
            if uncertainty:
                return sp.sqrt(sp.sum(sp.array(parts) ** 2, 0))
            else:
                return parts[0] - parts[1]
        if offset:
            return self.photometry(band, intrinsic=False) - self.photometry(band, intrinsic=True)
        if intrinsic:
            name = 'i_%s' % band
            arr = self[name]
        else:
            arr = sp.zeros(self.size)
            arr[:] = sp.nan
            conversion = {'U': ('l_B12_U_%s', 'l_B13_U_%s'),
                          'g': ('l_B12_g_%s', 'l_B13_g_%s'),
                          'r': ('l_B12_r_%s', 'l_B13_r_%s'),
                          'i': ('l_B12_i_%s', 'l_B13_i_%s'),
                          'Z': ('l_B12_Z_%s', 'l_B13_Z_%s'),
                          'J': ('s_J%s', 'l_2MA_%sJmag'),
                          'H': ('s_H%s', 'l_2MA_%sHmag'),
                          'Ks': ('s_K%s', 'l_2MA_%sKmag')}[band]
            for label in conversion:
                if label[:3] == 'l_B':
                    use = self[label[:5]]
                    label = label % ('UNCERT' if uncertainty else 'MAG')
                else:
                    if label[0] == 'l':
                        use = self['l_2MA']
                        label = label % ('e_' if uncertainty else '')
                    else:
                        label = label % ('_ERR' if uncertainty else '')
                        use = self[label] != 0
                arr[use] = self[use][label]
        return arr

    def as_table(self, include=('cluster', 'object ID', '# epochs', 'RA (deg)', 'Dec (deg)', 'Teff (K)', 'err Teff (K)', 'log(g)', 'err log(g)', 'v sin i (km/s)', 'err v sin i (km/s)', '<veiling>', 'err veiling', 'scatter veiling', '<vrad> (km/s)', 'corrected <vrad> (km/s)', 'err vrad (km/s)', 'scatter vrad (km/s)', 'S/N', 'chi-squared', 'A_J', 'extinction-corrected J')):
        convert = {'object ID': self['objid'],
                   'cluster': self['cluster'],
                   '# epochs': self['nepochs'],
                   'RA (deg)': self.get_index('h_RA'),
                   'Dec (deg)': self.get_index('h_DEC'),
                   'Teff (K)': self.weighted('Teff')[0],
                   'err Teff (K)': self.weighted('Teff')[1] * 3,
                   'log(g)': self.weighted('logg')[0],
                   'err log(g)': self.weighted('logg')[1] * 3,
                   'v sin i (km/s)': self.weighted('vsini')[0],
                   'err v sin i (km/s)': self.weighted('vsini')[1] * 3,
                   '<veiling>': self.weighted('veiling')[0],
                   'err veiling': self.weighted('veiling')[1] * 3,
                   'scatter veiling': self.get_std('em_veiling'),
                   '<vrad> (km/s)': self.weighted('vrad')[0],
                   'corrected <vrad> (km/s)': self.weighted('vcorr')[0],
                   'err vrad (km/s)': self.weighted('vrad')[1] * 3,
                   'scatter vrad (km/s)': self.get_std('em_vrad'),
                   '<S/N ratio>': self.get_mean('h_SNR'),
                   'S/N': sp.array([sp.sqrt(sp.sum(sp.array(snr) ** 2)) if isinstance(snr, list) else snr for snr in self['h_SNR']]),
                   'chi-squared': self['p_chisq'],
                   'A_J': self['e_AJ'],
                   'extinction-corrected J': self['e_relJ']
                   }
        from astropy import table
        from collections import OrderedDict
        return table.Table(OrderedDict([(name, convert[name]) for name in include]))


class KnownStellarParameters(AllParameters):
    """A class representing all measured objects with a link to the spectra.
    """
    specdir = None
    grid = None
    grid_name = None
    masked_hydrogen = None
    vary_params = None

    def __array_finalize__(self, obj, grid=None):
        for name in ['specdir', 'grid', 'grid_name', 'masked_hydrogen', 'vary_params', 'filename']:
            setattr(self, name, getattr(obj, name, None))

    def spectrum(self, objid, specinfo=None):
        """Return best-fit spectrum for given star name.
        """
        data_obj = self[self['objid'] == objid][0]
        mask_hydrogen = 50 if self.masked_hydrogen else 0
        tofit = fit.load_fit(self.specdir, self.grid, star_name=objid, mask_hydrogen=mask_hydrogen, specinfo=specinfo)
        for name in self.dtype.names:
            if 'p_' == name[:2] and 'hydrogen' not in name and name[2:] not in ('chisq', 'vcorr', 'poly_params') and (not sp.iterable(data_obj[name]) or len(data_obj[name]) != 0):
                param = name[2:]
                tofit.parameters[param] = data_obj[name]
        tofit.renormalize()
        if self.grid_name == 'RV':
            bounds = [(15180, 15780), (15895, 16400), (16510, 16925)]
            for subfit in tofit.iter_spectra():
                spectrum = subfit.observed()
                tomask = ~reduce(sp.logical_or, [(spectrum['wavelength'] > low) & (spectrum['wavelength'] < high) for low, high in bounds])
                spectrum['mask'] = tomask | spectrum['mask']
                #tofit.renormalize()
        return tofit

    @property
    def label(self, ):
        return '%s grid%s%s' % (self.grid_name, '; masked hydrogen' if self.masked_hydrogen else '', '; fit per epoch' if self.vary_params else '')


class MultiFits(object):
    def __init__(self, runs):
        """Initialize using spectral_parameters.allruns()
        """
        self.runs = runs

    def summarize(self, ):
        """Describes the parameters used to produce the runs.
        """
        row = '%11s' * 4
        print row % ('index', 'grid name', 'hydrogen', 'param per')
        for ixrun, run in enumerate(self.runs):
            print row % (str(ixrun), run.grid_name, 'masked' if run.masked_hydrogen else 'fitted', 'epoch' if run.vary_epoch else 'star')

    def __getitem__(self, index):
        return self.runs[index]

    def __len__(self, ):
        return len(self.runs)

    @property
    def objid(self, ):
        return self.runs[0]['objid']

    def plot_spectra(self, dir_name=os.path.join('plots', 'spectra')):
        if not os.path.isdir(dir_name):
            os.mkdir(dir_name)
        for objid in self.objid:
            nepochs = self.runs[0][self.objid == objid][0]['nepochs']
            fig = plt.figure(figsize=(14, 6 * nepochs))
            fig.subplots_adjust(hspace=0., wspace=0.)
            mask_no_hydrogen = sp.zeros((len(self), nepochs), dtype='object')
            for ixrun, arr, col in zip(xrange(len(self.runs)), self.runs, ['g', 'r', 'c', 'm']):
                full_tofit = arr.spectrum(objid)
                for ixfit, tofit in zip(xrange(nepochs), full_tofit):
                    if ixfit == 0:
                        ax = fig.add_subplot(nepochs * 2, 1, 2 * ixfit + 1)
                        axshare = ax
                    else:
                        ax = fig.add_subplot(nepochs * 2, 1, 2 * ixfit + 1, sharex=axshare)
                    plt.setp(ax.get_xticklabels(), visible=False)
                    spectrum = sp.array(tofit.compute_fit(renormalize=True))
                    if ixrun == 0:
                        use = ~spectrum['mask']
                        ax.plot(spectrum['wavelength'][use], spectrum['flux'][use], 'b')
                    ax.plot(spectrum.T['wavelength'], spectrum.T['theoretical'] + 0.2 * (ixrun + 1) * sp.median(spectrum['flux'][use]), col)
                    yl = ax.get_ylim()[1]
                    for subspec in spectrum:
                        start_mask = list(sp.where(~subspec[:-1]['mask'] & subspec[1:]['mask'])[0])
                        if subspec[0]['mask']:
                            start_mask = [0] + start_mask
                        end_mask = list(sp.where(subspec[:-1]['mask'] & ~subspec[1:]['mask'])[0])
                        if subspec[-1]['mask']:
                            end_mask = end_mask + [-1]
                        dwv = (subspec[1:]['wavelength'] - subspec[:-1]['wavelength']) / 2.
                        ax.hlines((sp.zeros(len(start_mask)) + ixrun * 0.03 + 0.03) * yl, subspec[start_mask]['wavelength'] - dwv[start_mask], subspec[end_mask]['wavelength'] + dwv[end_mask], colors=col)
                    ax.set_ylabel('flux')

                    axchi = fig.add_subplot(nepochs * 2, 1, 2 * ixfit + 2)
                    if ixfit != nepochs - 1:
                        plt.setp(axchi.get_xticklabels(), visible=False)
                    else:
                        axchi.set_xlabel('wavelength (A)')
                    use = ~spectrum['mask']
                    axchi.plot(spectrum['wavelength'][use], spectrum['chi'][use] + ixrun * 5, col)
                    axchi.set_ylabel(r'$\chi$')
            fig.savefig(os.path.join(dir_name, '%s.pdf' % objid))
            plt.close(fig)

    def get_run(self, grid_name='coelho', masked_hydrogen=False, vary_params=False):
        match = lambda arr: (arr.grid_name == grid_name) and (arr.masked_hydrogen == masked_hydrogen) & (arr.vary_params == vary_params)
        match_list = filter(match, self.runs)
        if len(match_list) == 0:
            raise RunNotFoundError('Requested run not in list')
        return match_list[0]

    def all_values(self, param):
        """returns dictionary with all values measured for parameter.
        """
        result = self.runs[0].all_values(param)
        result[self.runs[0].label] = result['IN-SYNC']
        del result['IN-SYNC']
        for arr in self.runs[1:]:
            result[arr.label] = arr.all_values(param)['IN-SYNC']
        return result

    def plot_offset(self, xparam, param, ysource2, ysource=None, xsource='coelho grid', errorbar=True, toplot=None, axes=None, label=None, nmed=0):
        """Plots `xparam` from `xsource` against the `param from `ysource2` (or Delta `param` between `ysource2` and `ysource`, if `ysource` is not None).
        """
        if axes is None:
            axes = plt.axes()
        if label is None:
            label = ysource2
        if isinstance(ysource2, list):
            [self.plot_offset(xparam, param, single_source, ysource, xsource, errorbar, toplot, axes) for single_source in ysource2]
            axes.legend(loc='best')
            if ysource is None:
                axes.set_ylabel('%s (from [see legend])' % param)
            elif ysource is None:
                axes.set_ylabel(r'$\Delta$ %s (between [see legend] and %s)' % (param, ysource))
            return
        xval = self.all_values(xparam)[xsource]
        yval2 = self.all_values(param)[ysource2]
        yplot = yval2.copy()
        if ysource is not None:
            yval = self.all_values(param)[ysource]
            yplot['value'] = yval2['value'] - yval['value']
            yplot['uncertainty'] = sp.sqrt(yval2['uncertainty'] ** 2 + yval['uncertainty'] ** 2)
        if toplot is not None:
            xval = xval[toplot]
            yplot = yplot[toplot]
        if errorbar:
            col = axes.errorbar(xval['value'], yplot['value'], yplot['uncertainty'], xval['uncertainty'], label=label, fmt='.')[0].get_color()
        else:
            col = axes.plot(xval['value'], yplot['value'], '.', label=label)[0].get_color()
        if nmed > 0:
            sor = sp.argsort(xval['value'])
            y_filtered = sp.ndimage.filters.median_filter(yplot['value'][sor], nmed)
            axes.plot(xval['value'][sor], y_filtered, col + '-')
        axes.set_xlabel('%s (from %s)' % (xparam, xsource))
        if ysource is None:
            axes.set_ylabel('%s (from %s)' % (param, ysource2))
        else:
            axes.set_ylabel(r'$\Delta$ %s (%s)' % (param, ysource2))
        return axes

    def hrd_plots(self, dir_name=os.path.join('plots', 'offset')):
        """Plots HR diagrams.
        """
        fig1 = plt.figure(figsize=(8, 6))
        fig2 = plt.figure(figsize=(8, 6))

        #all HRD diagrams
        fig2.set_figheight(9); fig2.set_figwidth(12)
        for errorbar in [False, True]:
            axall = fig1.add_subplot(111)
            for ixplot, source in enumerate(['RV grid', 'coelho grid', 'btsettl grid',
                                             'ASPCAP']):
                axhrd = fig2.add_subplot(2, 2, ixplot + 1)
                for ax in [axhrd, axall]:
                    for cluster in sp.unique(self[0]['cluster']):
                        self.plot_offset('Teff', 'logg', source, xsource=source, errorbar=errorbar, toplot=self[0]['cluster'] == cluster, axes=ax)
                    ax.set_xlim(2500, 7000)
            axall.set_ylabel('log(g) (from [see legend])')
            axall.set_xlabel('Teff (from [see legend])')
            axall.legend(loc='best')
            fig1.savefig(os.path.join(dir_name, 'HRD_1plot%s.pdf' % ('_errorbar' if errorbar else '')))
            fig2.savefig(os.path.join(dir_name, 'HRD_4plots%s.pdf' % ('_errorbar' if errorbar else '')))
            fig1.clf()
            fig2.clf()

    def all_offset_plots(self, dir_name=os.path.join('plots', 'offset')):
        """Plots offsets between measured parameter using different grids and literature.
        """
        fig1 = plt.figure(figsize=(8, 6))
        fig2 = plt.figure(figsize=(8, 6))
        sources = {'Teff': (['RV grid', 'btsettl grid', 'ASPCAP', 'Luhman et al. (2003)',
                            'Soderblom et al. (1993)', 'Soderblom et al. (2009)'], [(-1000, 1000), (-200, 200)]),
                    'vrad': (['RV grid', 'btsettl grid', 'RV pipeline',
                              'Mermilliod et al. (2009)', 'Furesz et al. (2006)', 'Dahm et al. (2008)'],
                              [(-50, 50), (-5, 5)])}
        fig2.set_figheight(12); fig2.set_figwidth(12)
        for errorbar in [False, True]:
            for xsource, xlim in [('btsettl grid', (2500, 7000)), ('coelho grid', (3500, 7000))]:
                for param, (source_list, poss_ylim) in sources.items():
                    for zoomed, ylim in zip([False, True], poss_ylim):
                        yref = 'btsettl grid'
                        axall = fig1.add_subplot(111)
                        for ixplot, source in enumerate(source_list):
                            axhrd = fig2.add_subplot(3, 2, ixplot + 1)
                            self.plot_offset('Teff', param, source, yref, xsource, errorbar=errorbar, axes=axhrd)
                            axhrd.set_ylim(ylim)
                            axhrd.set_xlim(xlim)
                        self.plot_offset('Teff', param, source_list, yref, xsource, errorbar=errorbar, axes=axall)
                        axall.set_ylim(ylim)
                        axall.set_ylim(ylim)
                        filename = os.path.join(dir_name, 'comp_Teff_%s_Delta_%s%s%s_%splot.pdf')
                        fig1.savefig(filename % (xsource.split()[0], param, '_errorbar' if errorbar else '', '_zoomed' if zoomed else '', '1'))
                        fig2.savefig(filename % (xsource.split()[0], param, '_errorbar' if errorbar else '', '_zoomed' if zoomed else '', '6'))
                        fig1.clf()
                        fig2.clf()

    def Teff_vrad(self, dir_name=os.path.join('plots', 'offset')):
        """Effective temperature vs radial velocity plots for various clusters.
        """
        fig2 = plt.figure(figsize=(15, 9))
        fig2.set_figheight(12); fig2.set_figwidth(12)
        for errorbar in [False, True]:
            for zoomed, xlim in [(False, (2600, 7000)), (True, (2600, 4000))]:
                xparam, xsource = 'Teff', 'btsettl grid'
                for ixplot, cluster in enumerate(['IC 348', 'NGC 1333', 'NGC 2264', 'Pleiades']):
                    toplot = self[0]['cluster'] == cluster
                    axes = fig2.add_subplot(2, 2, ixplot + 1)
                    for ysource in ['btsettl grid', 'coelho grid', 'RV grid', 'ESTVHELIO', 'SYNTHVHELIO', 'VHELIO']:
                        self.plot_offset(xparam, 'vrad', ysource, xsource=xsource, axes=axes, errorbar=errorbar, toplot=toplot, nmed=20)
                    axes.set_xlim(xlim)
                    axes.set_ylabel('Radial velocity (km/s)')
                    vmed = sp.median(self.all_values('vrad')['btsettl grid']['value'][toplot])
                    axes.set_title(cluster)
                    axes.set_ylim(vmed - 10, vmed + 10.)
                    if ixplot == 2:
                        axes.legend(loc='upper left')
                fig2.savefig(os.path.join(dir_name, 'Teff_vrad_cluster%s%s.pdf' % ('_errorbar' if errorbar else '', '_zoomed' if zoomed else '')))
                fig2.clf()

class Plotter(object):
    literature_labels = {'IN-SYNC': 'IN-SYNC',
                         'RV Pipeline': 'RV Pipeline',
                         'M09': 'Mermilliod et al. (2009)',
                         'T00': 'Terndrup et al. (2000)',
                         '2MA': '2MASS',
                         'N06': 'Nordhagen et al. (2006)',
                         'D08': 'Dahm (2008)',
                         'F06': 'Furesz et al. (2006)',
                         'B13': 'Bell et al. (2013)',
                         'M07': 'Mayne et al. (2007)'}

    def __init__(self, parameters, directory_plots=None):
        self.parameters = parameters
        if directory_plots is not None:
            self.directory_plots = directory_plots

    def get_value(self, name):
        if isinstance(name, basestring) and name[:4] == 'log_':
            return sp.log10(self.get_value(name[4:]))
        elif name in self.parameters.dtype.names:
            return self.parameters.get_median(name)
        elif isinstance(name, basestring) and name[:4] == 'std_':
            return self.parameters.get_std(name[4:])
        else:
            return name

    def get_label(self, name):
        if isinstance(name, tuple):
            return ' - '.join([self.get_label(subname) for subname in name])
        translation = {'h_RA': r'Right Ascension (deg)',
                   'h_DEC': r'Declination (deg)',
                   'h_VHELIO': r'RV pipeline radial velocity (km/s)',
                   'em_vrad': r'IN-SYNC radial velocity (km/s)',
                   'esc_vrad': r'MCMC 1-sigma uncertainty in RV (km/s)',
                   'a_Teff': r'ASPCAP effective temperature (K)',
                   'a_FTeff': r'uncalibrated ASPCAP effective temperature (K)',
                   'em_Teff': r'IN-SYNC effective temperature (K)',
                   'esc_Teff': r'MCMC 1-sigma uncertainty in $T_{\mathrm{eff}}$ (K)',
                   'a_logg': r'ASPCAP surface gravity (log(cgs))',
                   'a_Flogg': r'uncalibrated ASPCAP surface gravity (log(cgs))',
                   'em_logg': r'IN-SYNC surface gravity (log(cgs))',
                   'esc_logg': r'MCMC 1-sigma uncertainty in $\log(g)$ (log(cgs))',
                   'em_veiling': r'IN-SYNC H-band veiling (A$_{\mathrm{H}}$)',
                   'esc_veiling': r'MCMC 1-sigma uncertainty in A$_{\mathrm{H}}$',
                   'em_vsini': r'IN-SYNC rotational velocity (km/s)',
                   'esc_vsini': r'MCMC 1-sigma uncertainty in $v \sin(i)$ (km/s)',
                   'a_FeH': r'ASPCAP metallicity [Fe/H]',
                   'a_FFeH': r'uncalibrated ASPCAP metallicity [Fe/H]',
                   'U': 'INT-WFC U (Bell et al. 2012, 2013)',
                   'g': 'INT-WFC g (Bell et al. 2012, 2013)',
                   'r': 'INT-WFC r (Bell et al. 2012, 2013)',
                   'i': 'INT-WFC i (Bell et al. 2012, 2013)',
                   'Z': 'INT-WFC Z (Bell et al. 2012, 2013)',
                   'J': '2MASS J',
                   'H': '2MASS H',
                   'Ks': '2MASS Ks'
                   }
        if name in translation.keys():
            return translation[name]
        elif isinstance(name, basestring):
            return name
        else:
            return ''

    def scatter(self, axes, xvalue, yvalue, as_color='b', as_size=20, legend_loc='best', fmt='.', no_colorbar=False, **kwargs):
        """Creates a color plot on given axes object.

        If a parameter has multiple values per star, by default the median is plotted.

        Arguments:
        - `axes`: axes object on which the plot is created.
        - `xvalue`: name of parameter on x-axis.
        - `yvalue`: name of parameter on y-axis.
        - `as_color`: name of parameter as color.
        - `as_size`: name of parameter as size.
        """
        x_array = self.get_value(xvalue)
        y_array = self.get_value(yvalue)
        color_array = self.get_value(as_color)
        size_array = self.get_value(as_size)

        if as_color == 'per_cluster':
            result = []
            for cluster in sp.unique(self.parameters.cluster):
                use_cluster = self.parameters.cluster == cluster
                result.append(axes.plot(x_array[use_cluster], y_array[use_cluster], fmt, label=cluster, **kwargs))
            axes.legend(loc=legend_loc)
        else:
            result = axes.scatter(x_array, y_array, c=color_array, s=size_array, **kwargs)
        if isinstance(color_array, sp.ndarray) and not no_colorbar:
            cbar = axes.figure.colorbar(result)
            cbar.set_label(self.get_label(as_color))

        if xvalue in ['h_RA', 'em_Teff', 'a_Teff', 'a_logg', 'em_logg', 'a_FTeff', 'a_Flogg']:
            axes.set_xlim((max(axes.get_xlim()), min(axes.get_xlim())))
        if yvalue in ['h_RA', 'em_Teff', 'a_TEff', 'a_logg', 'em_logg', 'a_FTeff', 'a_Flogg']:
            axes.set_ylim((max(axes.get_ylim()), min(axes.get_ylim())))

        if xvalue in ['em_veiling', 'em_vsini']:
            axes.set_xscale('log')
        if yvalue in ['em_veiling', 'em_vsini']:
            axes.set_yscale('log')

        axes.set_xlabel(self.get_label(xvalue))
        axes.set_ylabel(self.get_label(yvalue))
        return result

    def literature_offset(self, axes, parameter, as_x=None):
        """Plot the offset between APOGEE and literature values for `parameter`.
        """
        names, values = self.parameters.all_values(parameter)
        if as_x is None:
            as_x = 'em_%s' % parameter
        x_array = self.get_value(as_x)
        for lit_name, y_array in zip(names[1:], values[1:, :]['value']):
            if sp.isfinite(y_array).any():
                axes.plot(x_array, values[0, :]['value'] - y_array, '.', label=self.literature_labels[lit_name])
        axes.legend(loc='best')
        axes.set_xlabel(self.get_label(as_x))
        axes.set_ylabel('%s from IN-SYNC - %s from ...' % (parameter, parameter))

    def tosave(self, filename):
        """Returns the full path, where the plot should be saved.
        """
        return os.path.join(self.directory_plots, filename)

    @property
    def directory_plots(self, ):
        """Directory containing the plots (is automatically created if it does not exist)
        """
        dir_name = os.path.join('plots', self.parameters.filename[:-4])
        if not os.path.isdir(dir_name):
            if not os.path.exists('plots'):
                os.mkdir('plots')
            os.mkdir(dir_name)
        return dir_name

    def photometry(self, axes, asx, asy, as_color='em_Teff', offset=False, **kwargs):
        """Scatter plot of the photometry.
        """
        if not offset:
            xvalue = self.parameters.photometry(asx, intrinsic=True)
            yvalue = self.parameters.photometry(asy, intrinsic=True)
            asort = sp.argsort(self.parameters['em_Teff'])
            self.scatter(axes, xvalue[asort], yvalue[asort], as_color=self.parameters['em_Teff'][asort], as_size=15, marker='d', edgecolors='none', no_colorbar=True)

        xvalue = self.parameters.photometry(asx, offset=offset)
        yvalue = self.parameters.photometry(asy, offset=offset)
        self.scatter(axes, xvalue, yvalue, as_color=as_color, as_size=30, edgecolors='none', **kwargs)

        axes.set_xlabel(self.get_label(asx))
        axes.set_ylabel(self.get_label(asy))
        if offset:
            axes.set_xlabel('excess %s ' % axes.get_xlabel())
            axes.set_ylabel('excess %s ' % axes.get_ylabel())

        xl = axes.get_xlim()
        axes.set_xlim(max(xl), min(xl))

        yl = axes.get_ylim()
        axes.set_ylim(max(yl), min(yl))


def scatter_plots(arr):
    """creates several simple scatter plots.
    """
    for name, params in [('distribution.pdf', ('h_RA', 'h_DEC', 'per_cluster')),
                         ('HR_diagram_INSYNC.pdf', ('em_Teff', 'em_logg', 'per_cluster')),
                         ('HR_diagram_ASPCAP.pdf', ('a_FTeff', 'a_Flogg', 'per_cluster')),
                         ('vsini_veiling.pdf', ('em_vsini', 'em_veiling', 'per_cluster'))]:
        axes = plt.axes()
        arr.plotter.scatter(axes, *params)
        axes.figure.savefig(arr.plotter.tosave(name))
        axes.figure.clf()

    for cluster in sp.unique(arr.cluster):
        for name, params in [('distribution_RV_%s.pdf', ('h_RA', 'h_DEC', 'em_vrad')),
                         ('HR_chisq_%s.pdf', ('em_Teff', 'em_logg', 'log_chisq'))]:
            axes = plt.axes()
            arr.get_cluster(cluster).plotter.scatter(axes, *params)
            axes.figure.savefig(arr.plotter.tosave(name) % cluster.replace(' ', '_'))
            axes.figure.clf()

    for parameter in ['vsini', 'vrad']:
        for as_x in ('em_Teff', 'em_vsini', 'chisq', 'em_vrad'):
            axes = plt.axes()
            arr.plotter.literature_offset(axes, parameter, as_x)
            axes.set_ylim(-3, 3)
            if parameter == 'vsini':
                axes.set_ylim(-20, 20)
            if as_x == 'chisq':
                axes.set_xscale('log')
            elif as_x in ['em_vsini', 'em_vrad']:
                axes.set_xlim((-100, 100))
            axes.figure.savefig(arr.plotter.tosave('offset_literature_%s_%s.pdf' % (parameter, as_x)))
            axes.figure.clf()

    for cluster in ['Pleiades', 'IC 348', 'NGC 1333', 'NGC 2264']:
        axes = plt.axes()
        arrc = arr.get_cluster(cluster)
        for y_name, y_full in zip(*arrc.all_values('vrad')):
            if sp.sum(sp.isfinite(y_full['value'])) > 0:
                xarr = arrc.get_median('em_Teff')[sp.isfinite(y_full['value'])]
                yarr = y_full[sp.isfinite(y_full['value'])]['value']
                color = axes.plot(xarr, yarr, '.', label=arrc.plotter.literature_labels[y_name])[0].get_color()
                sort_x = sp.argsort(xarr)
                nmed = max((20, int(yarr.size / 5.)))
                y_filtered = sp.ndimage.filters.median_filter(yarr[sort_x], nmed)
                axes.set_xlabel(arr.plotter.get_label('em_Teff'))
                axes.set_ylabel('radial velocity (km/s)')
                axes.plot(xarr[sort_x], y_filtered, color + '-', label=arrc.plotter.literature_labels[y_name] + ' median filtered over %i RVs' % nmed)
        med_rv = sp.median(arrc.get_median('em_vrad'))
        axes.set_ylim(med_rv - 5., med_rv + 5.)
        axes.legend(loc='best')
        axes.figure.savefig(arr.plotter.tosave('literature_%s_vrad_Teff.pdf' % cluster.replace(' ' , '_')))
        axes.figure.clf()


def plot_runs(runs, dir_name='plots/multi_fit'):
    """make some useful plots with all runs
    """
    labels = ['%s; %s%s' % (arr.grid_name, 'masked hydrogen; ' if arr.masked_hydrogen else '', 'fit per epoch;' if arr.vary_params else 'fit per star;') for arr in runs]

    cluster_fig = plt.figure(figsize=(19, 9))
    for xval, xname, xlabel, xzoomed in [(runs[-1].weighted('Teff')[0], 'Teff', 'effective temperature from RV grid (K)', (2500, 6000))]:
        for yname, ylabel, yzoomed, ylog in [('vrad', 'radial velocity (km/s)', (0, 30), False), ('vsini', 'rotational velocity (km/s)', (1e-1, 1e3), True), ('Teff', 'effective temperature (K)', (3500, 7000), False),
                                       ('veiling', 'veiling', (1e-1, 1e2), True), ('logg', 'surface gravtity (log(cgs))', (3, 5), False)]:
            for tozoom in (False, True):
                for plot_unc in (False, True):
                    for ixcluster, cluster in enumerate(sp.unique(runs[-1]['cluster'])):
                        axes = cluster_fig.add_subplot(2, 3, ixcluster + 1)
                        for arr, label, col in zip(runs, labels, ['g', 'r', 'c', 'm']):
                            use = arr['cluster'] == cluster
                            axes.plot(xval[use], arr[use].weighted(yname)[plot_unc], col + '.', label=label)
                        axes.set_xlabel(xlabel)
                        axes.set_ylabel(ylabel)
                        axes.set_title(cluster)
                        if tozoom:
                            axes.set_ylim(yzoomed)
                            axes.set_xlim(xzoomed)
                        if ylog:
                            axes.set_yscale('log')
                        if plot_unc:
                            axes.set_yscale('log')
                    filename = os.path.join(dir_name, '%s_%s%s_%spercluster.pdf' % (xname, 'err_' if plot_unc else '', yname, 'zoomed_' if tozoom else ''))
                    cluster_fig.savefig(filename)
                    cluster_fig.clf()

    vrad_runs = [arr.get_cluster('IC 348').weighted('vrad')[0] for arr in runs] + [runs[0].get_cluster('IC 348').get_median('v_SYNTHVHELIO')]
    for ixvrad, vrad, title in zip(xrange(len(vrad_runs)), vrad_runs, labels + ['from cross-correlation (pipeline)']):
        axes = cluster_fig.add_subplot(2, 3, ixvrad + 1)
        med_vrad = sp.median(vrad_runs[-2])
        use = runs[-1].get_cluster('IC 348')['em_Teff'] < 2800
        axes.hist(vrad, range=(med_vrad - 10, med_vrad + 10), bins=30)
        axes.hist(vrad[use], range=(med_vrad - 10, med_vrad + 10), bins=30)
        axes.set_xlabel('radial velocity in IC 348 (km/s)')
        axes.set_ylabel('N (blue: all stars, green: Teff < 2800)')
        axes.set_title(title)
    filename = os.path.join(dir_name, 'vrad_distribution.pdf')
    cluster_fig.savefig(filename)
    cluster_fig.clf()


def plot_extinction(arr):
    import matplotlib.pyplot as plt
    from scripts import sed

    axes = plt.axes()
    sed.plot_distances(arr, axes=axes)
    axes.figure.savefig(arr.plotter.tosave('SED_calibration_photometric_distance.pdf'))
    axes.figure.clf()

    axes = plt.axes()
    sed.plot_sequence(arr, axes=axes)
    axes.figure.savefig(arr.plotter.tosave('SED_calibration_Teff.pdf'))
    axes.figure.clf()

    for cluster in ['Pleiades', 'IC 348', 'NGC 1333', 'NGC 2264']:
        axes = plt.axes()
        arrc = arr.get_cluster(cluster)
        for xname, label in [('em_Teff', 'Coelho grid'), ('a_FTeff', 'uncorrected ASPCAP')]:
            axes.plot(arrc.get_median(xname), arrc['a_H'], '.', label=label)
        yl = axes.get_ylim()
        axes.set_ylim(yl[1], yl[0])
        axes.set_ylim(15, 7)
        xl = axes.get_xlim()
        axes.set_xlim(xl[1], xl[0])
        axes.set_xlim(7500, 3500)
        axes.set_xlabel('effective temperature (K)')
        axes.set_ylabel('2MASS H-band (mag)')
        if cluster == 'Pleiades':
            try:
                from photometry import intrinsic
                mod = intrinsic.tada_mod(sp.linspace(3500, 7500, 50), age=1.5e8)
                axes.plot(10 ** mod()['log_Teff'], mod()['2mass_h'] + 5.63, 'r-', label='BT-Settl isochrone')
                axes.plot(10 ** mod()['log_Teff'], mod()['2mass_h'] + 5.63 - 0.75, 'r--', label='BT-Settl equal-mass binary')
            except ImportError:
                pass
        axes.legend(loc='center left')
        axes.figure.savefig(arr.plotter.tosave('Teff_H_%s.pdf' % cluster.replace(' ', '_')))
        axes.figure.clf()

    arrc = arr.get_cluster('IC 348')
    for xname in  [('r', 'i'), ('i', 'Z')]:
        for yname in [('g', 'r'), ('g', 'i'), ('i', 'Z'), ('i', 'J'), ('J', 'H'), ('H', 'Ks'), 'i']:
            for offset in [False, True]:
                axes = plt.axes()
                arrc.plotter.photometry(axes, xname, yname, offset=offset, vmin=3500, vmax=5000)
                if isinstance(yname, tuple):
                    axes.figure.savefig(arrc.plotter.tosave('CCD%s_%s-%s_%s-%s.pdf' % ('_excess' if offset else '', xname[0], xname[1], yname[0], yname[1])))
                else:
                    axes.figure.savefig(arrc.plotter.tosave('CMD%s_%s-%s_%s.pdf' % ('_excess' if offset else '', xname[0], xname[1], yname)))
                axes.figure.clf()

    arrc = arr.get_cluster('IC 348')
    extinction = sp.load('/Users/mcottaar/Dropbox/python/APOGEE_shared/scripts/extinction_law.npy')
    for normalization_color in [('r', 'i'), ('i', 'Z')]:
        for relative_color in ['g', 'r', 'Z', 'J', 'H', 'Ks']:
            axes = plt.axes()
            extinction_direction = arrc.photometry(('i', relative_color), offset=True) / arrc.photometry(normalization_color, offset=True)
            axes.hist(extinction_direction, range=(-10, 10), bins=50)
            axes.set_xlabel('E(i - %s) / E(%s - %s)' % (relative_color, normalization_color[0], normalization_color[1]))
            axes.set_ylabel('N')
            for group in ('intwfc_%s', '2mass_%s'):
                if group % relative_color.lower() in extinction.dtype.names:
                    for single_rv, color in zip(extinction, ('g', 'r', 'c', 'y', 'm')):
                        ratio = ((single_rv['intwfc_i'] - single_rv[group % relative_color.lower()]) /
                                 (single_rv['intwfc_%s' % normalization_color[0]] - single_rv['intwfc_%s' % normalization_color[1].lower()]))
                        axes.axvline(ratio, ls='--', label='Rv = %.1f' % single_rv['Rv'], color=color)
            axes.legend(loc='upper left')
            axes.figure.savefig(arrc.plotter.tosave('extinction_law_E_i-%s_E_%s-%s.pdf' % ((relative_color, ) + normalization_color)))
            axes.figure.clf()

    arrc, fit = sed.sed_fit(arr)
    photfit = fit[sp.arange(arrc.size), sp.argmin(fit['goodness_of_fit'], 1)]
    specfit = photfit.copy()
    for name in specfit.dtype.names:
        specfit[name] = sp.interpolate.interp1d(fit[0, :]['Teff'], fit[name], bounds_error=False)(arrc['em_Teff'])[sp.eye(arrc.size, dtype='bool')]

    toplot = [  ('DM', lambda fit: fit['DM'], 'Excess flux (mag)'),
                ('logL', lambda fit: -fit['DM'] / 2.5, 'L / L(8 Myr)'),
                ('logg', lambda fit: arrc['em_logg'], 'spectrosopic log($g$)'),
                ('Av', lambda fit: fit['Av'], 'Visual extinction ($A_V$)'),
                ('Teff', lambda fit: fit['Teff'], 'effective temperature (K)'),
                ('vsini', lambda fit: arrc['em_vsini'] + 1., 'rotational velocity (km/s)')]
    for xname, xval, xlabel in toplot[1:-1]:
        for yname, yval, ylabel in toplot[1:]:
            for sname, sval, slabel in toplot[3:]:
                if sname != xname and xname != yname and sname != yname:
                    axes = plt.axes()
                    for plotfit, color, label in [(photfit, 'b', 'photometric effective temperature'),
                                                  (specfit, 'g', 'spectroscopic effective temperature')]:
                        use = sp.isfinite(sval(plotfit)) & all_to_plot
                        axes.scatter(xval(plotfit)[use], yval(plotfit)[use], s=sval(plotfit)[use] / sp.nanmax(sval(plotfit)) * 40, c=color, label=label, edgecolor='none')
                    axes.set_xlabel(xlabel)
                    axes.set_ylabel(ylabel)
                    axes.legend(loc='best')
                    axes.set_title('size set by %s' % slabel)
                    axes.figure.savefig(arrc.plotter.tosave('extinction_%s_%s_%s.pdf' % (xname, yname, sname)))
                    axes.figure.clf()

    for plotfit, title in [(photfit, 'phot'), (specfit, 'spec')]:
        for toplot, xlabel in (('Av', 'Extinction Av'), ('DM', 'distance modulus')):
            axes = plt.axes()
            axes.hist(plotfit[toplot], bins=30, range=[-1, 7], label='all stars')
            axes.hist(plotfit[toplot][plotfit['goodness_of_fit'] < 1e-1], bins=30, range=[-1, 7], label='stars with good fits')
            axes.set_xlabel(xlabel)
            axes.set_ylabel('N')
            axes.figure.savefig(arrc.plotter.tosave('extinction_hist_%s_%sTeff.pdf' % (toplot, title)))
            axes.figure.clf()

    for plotfit, title in [(photfit, 'phot'), (specfit, 'spec')]:
        all_to_plot = (plotfit['Teff'] < 4900) & (plotfit['Teff'] > 3520) & sp.isfinite(specfit['DM'])
        axes = plt.axes()
        use = ~(plotfit['goodness_of_fit']< 1e-1) & all_to_plot
        axes.scatter(arrc[use]['em_logg'], -plotfit['DM'][use] / 2.5, c=plotfit[use]['Teff'], s=sp.log10(arrc[use]['em_vsini'] + 1) * 20, marker='^', edgecolor='none', vmin=3500, vmax=5000.)
        use = (plotfit['goodness_of_fit']< 1e-1) & all_to_plot
        sca = axes.scatter(arrc[use]['em_logg'], -plotfit['DM'][use] / 2.5, c=plotfit[use]['Teff'], s=sp.log10(arrc[use]['em_vsini'] + 1) * 20, marker='s', edgecolor='none', vmin=3500, vmax=5000.)
        axes.arrow(4.5, -0.35, -0.5, 0.5, head_length=.02, head_width=.02)
        cbar = axes.figure.colorbar(sca)
        cbar.set_label('effective temperature (K)')
        axes.set_ylabel('log (L/L(8 Myr at 300 pc)')
        axes.set_xlabel('log(g)')
        axes.set_title('size is set by log(rotational velocity)')
        axes.set_xlim(3, 5)
        axes.set_ylim(-1.5, 1)
        print 'logg_phot_excess_%sTeff.pdf' % (title)
        axes.figure.savefig(arrc.plotter.tosave('logg_phot_excess_%sTeff.pdf' % (title)))
        axes.figure.clf()

    for yval, ylabel, title in ((DM, 'color-independent photometric excess', 'DM'),
                          (arrc.get_median('em_logg'), 'log(g) (cgs)', 'logg')):
        axes = plt.axes()
        good = sp.sum(goodness_of_fit ** 2, 0) < 1e-1
        axes.plot(Av, yval, '.', label='bad photometric fits')
        axes.plot(Av[good], yval[good], '.', label='good photometric fits')
        axes.legend(loc='best')
        axes.set_xlabel(r'Extinction A$_{\mathrm{V}}$ (mag)')
        axes.set_ylabel(ylabel)
        if title == 'logg':
            axes.set_ylim(3, 5)
        axes.figure.savefig(arrc.plotter.tosave('Av_%s.pdf' % title))
        axes.figure.clf()

    arrc = arr.get_cluster('IC 348')
    extinction = sp.load('/Users/mcottaar/Dropbox/python/APOGEE_shared/scripts/extinction_law.npy')
    single_extinction = extinction[2]
    bands = ['g', 'r', 'i', 'Z', 'J', 'H', 'Ks']
    phot = sp.array([arrc.photometry(band, offset=True) for band in bands])
    AxAv = sp.array([single_extinction['intwfc_%s' % band.lower() if 'intwfc_%s' % band.lower() in single_extinction.dtype.names
                              else '2mass_%s' % band.lower()] for band in bands]) / single_extinction['johnson_v']
    Av, DM = sp.polyfit(AxAv, phot, 1)
    goodness_of_fit = AxAv[:, None] * Av[None, :] + DM[None, :] - phot

    for label, use in (('all', sp.ones(arrc.size, dtype='bool')),
                       ('good', sp.sum(goodness_of_fit ** 2, 0) < 1e-1),
                       ('bad', sp.sum(goodness_of_fit ** 2, 0) > 1e-1)):
        axes = plt.axes()
        axes.plot(AxAv, phot[:, use], '-')
        axes.plot(AxAv, phot[:, use], '.')
        axes.set_xlabel('A(X) / A(V)')
        axes.set_ylabel('excess flux (DM + A(X) (mag))')
        axes.figure.savefig(arrc.plotter.tosave('extinction_offset_%s.pdf' % label))
        axes.figure.clf()

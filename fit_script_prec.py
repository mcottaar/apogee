"""Script to fit the multiple epoch spectra of one or all stars.

The spectra are read from specdir, the (Coelho) grid is read from griddir, and the results are saved in a pickle file in resultdir.

During optimization the effective temperature, log(g), veiling and v sin(i) are fitted, using a constant value across all epochs. For every epoch a different radial velocity is fitted.

This script can be called without options (python fit_script.py) to fit all the stars.
Calling the scipt with a stellar name (python fit_script.py <star_name>) will cause only the spectra observed for that star to be fit.
"""
import matplotlib
matplotlib.use('PDF')
import fit
import model_spectra
import scipy as sp
import os
import scipy.optimize
import scipy.stats
import time
import copy
import scipy.ndimage

specdir = '/cluster/work/scr2/mcottaar/apogee/spectra'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/coelho'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/apg_rvsynthgrid_v2.fits'
griddir = '/cluster/work/scr2/mcottaar/apogee/grid/BT-Settlgrid.npz'
#griddir = '/cluster/work/scr2/mcottaar/apogee/grid/ESO-Gaiagrid.npz'
resultdir = '/cluster/work/scr2/mcottaar/apogee/fits'
pdfdir = '/cluster/work/scr2/mcottaar/apogee/pdf'

name_grid = 'Coelho'
#name_grid = 'RV'
#name_grid = 'ATLAS'
name_grid = 'BT-Settl'
#name_grid = 'ESO-GAIA'

def main(name=None):
    """Fit the given star and saves the result"""
    for ix in range(10):
        try:
            # retry loading 10 times, because loading the header files sometimes gives an error if the file is already in use due to bug(?) in sp.load
            tofit = fit.load_fit(specdir, grid, star_name=name, mask_hydrogen=0)
        except ValueError as error:
            time.sleep(10)
        else:
            break
    else:
        raise error

    if name_grid == 'RV':
        bounds = [(15180, 15780), (15895, 16400), (16510, 16925)]
        for subfit in tofit.iter_spectra():
            spectrum = subfit.observed()
            tomask = ~reduce(sp.logical_or, [(spectrum['wavelength'] > low) & (spectrum['wavelength'] < high) for low, high in bounds])
            spectrum['mask'] = tomask | spectrum['mask']
    for parameter in ('Teff', 'logg', 'vsini', 'veiling'):
        tofit.parameters.make_constant(parameter)
    tofit.parameters.make_variable('vrad')
    min_chisq = sp.inf
    tofit.parameters['vsini'] = 20
    tofit.parameters['veiling'] = 0.1
    for ixspec in sp.arange(grid.size):
        tofit.parameters['Teff'] = grid._variables[1][1][ixspec]
        tofit.parameters['logg'] = grid._variables[0][1][ixspec]
        tofit.cross_correlate()
        chisq = tofit.chisq(renormalize=True)
        if chisq < min_chisq:
            ix_best = ixspec
            min_chisq = chisq
    tofit.parameters['Teff'] = grid._variables[1][1][ix_best]
    tofit.parameters['logg'] = grid._variables[0][1][ix_best]
    for name, size in [('Teff', 20), ('logg', 0.1)]:
        for direction in [-1, 1]:
            value = tofit.parameters[name]
            try:
                tofit.parameters[name] = value + direction * size
                tofit.compute_fit(())
            except model_spectra.GridError:
                tofit.parameters[name] = value - direction * size
            else:
                tofit.parameters[name] = value
    if tofit.parameters['Teff'] == 7000:
        tofit.parameters['Teff'] = 6980
    if tofit.parameters['logg'] >= 5.:
        tofit.parameters['logg'] = 4.9
    if tofit.parameters['logg'] <= 0.:
        tofit.parameters['logg'] = 0.1
    tofit.cross_correlate()
    print 'velocities', tofit.parameters['vrad']
    print 'best initial fit, Teff = %i, logg = %.1f with chisq = %i' % (tofit.parameters['Teff'], tofit.parameters['logg'], min_chisq)
    tofit.optimize(('Teff', 'logg', 'vsini', 'veiling'), method='', renormalize=True, maxiter=10000, maxfun=20000)
    print 'after optimization', tofit.parameters
    tofit.cross_correlate()
    print 'velocities', tofit.parameters['vrad']
    print 'best re-fit, Teff = %i, logg = %.2f with chisq = %i' % (tofit.parameters['Teff'], tofit.parameters['logg'], tofit.chisq(renormalize=True))
    print 'best number of polynomial dimensions', tofit.optimize_npoly(max_npoly=6)
    print 'chi-squared after adjusting polynomial dimension', tofit.chisq(renormalize=True)
    tofit.parameters['veiling'] = 0.2
    tofit.parameters['vsini'] = 20.
    for parameter in ('veiling', 'vrad'):
        tofit.parameters.make_variable(parameter)
    print tofit.parameters
    print 'starting fit'
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='', renormalize=True, maxiter=10000, maxfun=20000)
    print 'velocities', tofit.parameters['vrad']
    print 'final fit, Teff = %i, logg = %.2f with chisq = %i' % (tofit.parameters['Teff'], tofit.parameters['logg'], tofit.chisq(renormalize=True))
    print 'best number of polynomial dimensions', tofit.optimize_npoly(max_npoly=6)
    print 'chi-squared after adjusting polynomial dimension', tofit.chisq(renormalize=True)
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='', renormalize=True, maxiter=10000, maxfun=20000)
    print 'final fit, Teff = %i, logg = %.2f with chisq = %i' % (tofit.parameters['Teff'], tofit.parameters['logg'], tofit.chisq(renormalize=True))
    print 'best number of polynomial dimensions', tofit.optimize_npoly(max_npoly=6)
    print 'chi-squared after adjusting polynomial dimension', tofit.chisq(renormalize=True)
    best_chisq, parameters = tofit.chisq(renormalize=True), dict(tofit.parameters)
    tofit.parameters['vrad'] = [sp.median(tofit.parameters['vrad'])] * len(tofit)
    tofit.parameters['veiling'] = [0.2] * len(tofit)
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='', renormalize=True, maxiter=10000, maxfun=20000)
    print 'final fit after reset of vrad/veiling, Teff = %i, logg = %.2f with chisq = %i' % (tofit.parameters['Teff'], tofit.parameters['logg'], tofit.chisq(renormalize=True))
    print 'best number of polynomial dimensions', tofit.optimize_npoly(max_npoly=6)
    print 'chi-squared after adjusting polynomial dimension', tofit.chisq(renormalize=True)
    if best_chisq < tofit.chisq():
        tofit.parameters.update(parameters)


    tofit.emcee(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), nwalkers=100, nsteps=10, renormalize=True)
    tofit.renormalize()
    tofit.save(resultdir)
    toplot = tofit.plot()
    toplot.multiplot('plot_spectrum', os.path.join(pdfdir, '%s_spectrum.pdf' % name))
    if len(tofit) > 1:
        toplot.multiplot('plot_rms_binarity', os.path.join(pdfdir, '%s_rms.pdf' % name), chisq=True, normalized=True, fluxlim=(0., 1.5))
    toplot.multiplot('plot_emcee', os.path.join(pdfdir, '%s_emcee.pdf' % name))


if __name__ == '__main__':
    sp.random.seed(78961)
    print 'loading %s grid' % name_grid
    if name_grid == 'Coelho':
        grid = model_spectra.coelho(griddir).at_value('FeH', 0.).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.)).convert_spectra(resolution=22500, new_wavelength=15100. * sp.exp(sp.arange(36e3)/ 3e5))
    elif name_grid == 'RV':
        grid = model_spectra.RVgrid(griddir).at_value('alpha', 0).at_value('carbon', 0)
    elif name_grid in ['ESO-GAIA', 'BT-Settl']:
        grid = model_spectra.pre_loaded(griddir)
    import sys
    if len(sys.argv) == 1:
        main()
    else:
        main(sys.argv[1])

"""Module to read in APOGEE spectra.

Two methods and one class are defined:
- `load_spectra`: Reads in and returns a set of spectra which match a chosen set of search criteria.
- `cluster_list`: returns an array with all apVisit files in a given directory with some header information for each spectrum.
- `read_apogee_spectrum`: reads in a given filename and returns the corresponding spectrum. Automatically shifts the spectra to a heliocentric wavelength system and adds masking.
- `ObservedSpectrum`: class for the observed APOGEE spectra. Implemented a method to shift the wavelengths from telluric to heliocentric system or vice versa.

Usage:
>>> import observed_spectrum
>>> file_list = observed_spectrum.cluster_list('/directory/containing/APOGEE/spectra', 2)
>>> print file_list.dtype.names  # prints the parameters in the array, which can be used for filtering the file list.
>>> # Selecting spectra taken for star 2M03401184+3155233
>>> stellar_file_list = file_list[file_list['objid'] == '2M03401184+3155233']
>>> # loading in the spectra
>>> stellar_spectra = [observed_spectrum.read_apogee_spectrum(filename) for filename in stellar_file_list['long_filename']]
>>> # plot the first spectrum
>>> import matplotlib.pyplot as plt
>>> plt.plot(stellar_spectra[0]['wavelength'], stellar_spectra[0]['flux'])
"""

import scipy as sp
import os.path
import pyfits
import glob
import heliocentric


def load_spectra(directory='.', ixrecursive=0, **kwargs):
    """Search and return all spectra which match certain criteria in the provided directory.

    Returns a list of spectra, which match the search criteria, unless `index` is set in which case a single spectrum is returned.

    Arguments:
    - `directory`: directory to search for spectra (default: current directory)
    - `ixrecursive`: number of subdirectories to search through for spectra (default: not recursive)
    - `location`: int; Only return spectra that match the location keyword.
    - `plate`: int; Only return spectra on given plate id.
    - `fiberid`: int; Only return spectra observed through given fiber id.
    - `ncombine`: int; Only return spectra that match the ncombine keyword.
    - `glon`: (float, float); Only return spectra in given galactic longitude range.
    - `glat`: (float, float); Only return spectra in given galactic latitude range.
    - `ra`: (float, float); Only return spectra in given right ascension range
    - `dec`: (float, float); Only return spectra in given declination range.
    - `jmag`: (float, float); Only return spectra in given J magnitude range.
    - `hmag`: (float, float); Only return spectra in given H magnitude range.
    - `kmag`: (float, float); Only return spectra in given K magnitude range.
    - `jd`: (float, float); Only return spectra in given Julian date range.
    - `hjd`: (float, float); Only return spectra in given heliocentric Julian date range.
    - `vrad`: (float, float); Only return spectra in given radial velocity range (as measured by APOGEE pipeline).
    - `vraderr`: (float, float); Only return spectra in given range of radial velocity measurement uncertainty (as measured by APOGEE pipeline).
    - `feh`: (float, float); Only return spectra in given metallicity range (as measured by APOGEE pipeline).
    - `teff`: (float, float); Only return spectra in given effective temperature range (as measured by APOGEE pipeline).
    - `logg`: (float, float); Only return spectra in given log surface gravity range (as measured by APOGEE pipeline).
    - `chisq`: (float, float); Only return spectra in given chi-squared range (as measured by APOGEE pipeline).
    - `exptime`: (float, float); Only return spectra in given exposure time range.
    - `objid`: str; Only return spectra from given object name.
    - `objtype`: str; Only return spectra of given object type.
    - `filename`: str; Only return spectra with given filename.
    - `index`: Only return the (index + 1)th spectrum of the spectra, that match the search constraints.
    """
    all_spec = cluster_list(directory, ixrecursive)
    if len(all_spec) == 0:
        raise ValueError('No spectra found in directory "%s" with depth %i' % (directory, ixrecursive))
    for key, value in kwargs.items():
        if key != 'index':
            if all_spec.dtype[key] == 'float64':
                all_spec = all_spec[sp.logical_and(all_spec[key] > min(value), all_spec[key] < max(value))]
            else:
                all_spec = all_spec[all_spec[key] == value]
    if len(all_spec) == 0:
        raise ValueError('No spectra found for given search criteria')
    if 'index' in kwargs.keys():
        return read_apogee_spectrum(all_spec[kwargs['index']]['full_filename'])
    return ApogeeSpectrum.from_list([read_apogee_spectrum(filename) for filename in sp.atleast_1d(all_spec['full_filename'])])


def cluster_list(directory='.', ixrecursive=0):
    """Search for APOGEE spectra in apVisit-*-*-*.fits format.

    Returns a list of all spectra found with target name, epochs, etc.

    Paremeters:
    - `directory`: base directory where the APOGEE spectra are stored
    - `ixrecursive`: sets the maximum number of recursions to look in subdirectories (default: not recursive)
    """
    filelist = glob.glob(os.path.join(directory, 'apVisit-*-*-*.fits'))
    file_global = os.path.join(os.path.dirname(__file__), 'spectra_headers.npy')

    trimmed_filelist = sp.unique(filelist)
    dtype = ( [(name, 'i4') for name in ['location', 'plate', 'fiberid', 'ncombine']] +
        [(name, 'f8') for name in ['glon', 'glat', 'ra', 'dec', 'jmag', 'hmag', 'kmag', 'jd', 'hjd', 'vrad', 'vraderr',
                                   'feh', 'teff', 'logg', 'chisq', 'exptime']] +
        [('objid', 'S30'), ('objtype', 'S10'), ('filename', 'S30'), ('full_filename', 'S200')])
    try:
        arr_headers = sp.load(file_global)
    except IOError:
        arr_headers = sp.zeros(0, dtype=dtype)

    # read in all arrays in this directory
    result = sp.zeros(trimmed_filelist.size, dtype=dtype)
    for ixfile, filename in enumerate(filelist):
        if os.path.split(filename)[1] in arr_headers['filename']:
            ixsame = sp.where(arr_headers['filename'] == os.path.split(filename)[1])[0][0]
            result[ixfile] = arr_headers[ixsame]
            result[ixfile]['full_filename'] = filename
        else:
            head = pyfits.getheader(filename, 0)
            for label in result.dtype.names:
                if 'filename' not in label:
                    if label.upper() not in head.keys():
                        if label in ['location', 'plate', 'fiberid', 'ncombine']:
                            result[ixfile][label] = -1
                        else:
                            result[ixfile][label] = sp.nan
                    else:
                        result[ixfile][label] = head[label]
                elif label == 'filename':
                    result[ixfile]['filename'] = os.path.split(filename)[1]
                else:
                    result[ixfile]['full_filename'] = filename
            arr_headers = sp.append(arr_headers, result[ixfile])
    sp.save(file_global, arr_headers)

    # append all arrays in subdirectories
    if ixrecursive:
        for indir in os.listdir(directory):
            subdir = os.path.join(directory, indir)
            if os.path.isdir(subdir):
                result = sp.append(result, cluster_list(subdir, ixrecursive=ixrecursive - 1))
    return result


def read_apogee_spectrum(filename, split_chips=False, to_mask=((15802, 15820), ), mask_hydrogen=0, n_hydrogen=5):
    """reads in a given APOGEE spectrum.

    Arguments:
    - `filename`: Filename of fits-file to read.
    - `split_chips`: If True returns a list of 3 ApogeeSpectrum objects for every chip. If False returns a single two-dimensional ApogeeSpectrum object.
    - `to_mask`: additional wavelength regimes to mask.
    - `mask_hydrogen`: Wavelength distance from center of hydrogen lines that should be masked (in units of the wavelength).
    - `n_hydrogen`: number of hydrogen lines to mask.
    """
    with pyfits.open(filename) as hrd:
        columns = ['flux', 'error', 'wavelength', 'sky_flux', 'sky_error', 'telluric_flux', 'telluric_error']
        pixmask = ['BADPIX', 'CRPIX', 'SATPIX', 'UNFIXABLE', 'BADDARK', 'BADFLAT', 'BADERR', 'NOSKY', 'LITTROW_GHOST', 'PERSIST_HIGH', 'PERSIST_MED',
                   'PERSIST_LOW', 'SIG_SKYLINE', 'SIG_TELLURIC'] # from https://sdss3.org/internal/branches/v5/dr10/algorithms/bitmask_apogee_pixmask.php
        spectrum = sp.zeros(hrd[1].data.shape, [(name, 'f8') for name in columns] + [('mask', 'bool'), ('maxchi', 'f8')] + [(name, 'bool') for name in pixmask])
        for ixlabel, label in enumerate(columns):
            if ixlabel >= 2:
                ixlabel = ixlabel + 1
            spectrum[label] = hrd[ixlabel + 1].data
        for ixmask, label in enumerate(pixmask):
            spectrum[label] = hrd[3].data & (2 ** ixmask)

        # remove bad pixels and emission sky lines
        spectrum['mask'] = (spectrum['BADPIX'] | spectrum['UNFIXABLE'] | spectrum['SIG_SKYLINE'])
        spec = spectrum.T.view(ApogeeSpectrum)
        hrd[0].verify('fix')  # fix any errors in header fits-file (was needed for apVisit-r-6225-56236-105.fits, but probably not needed anymore)

        spec.meta = dict(hrd[0].header)
        if 'SNR' not in spec.meta.keys() or (isinstance(spec.meta['SNR'], basestring) and 'NAN' in spec.meta['SNR']):
            spec.meta['SNR'] = sp.nan  # dealing with bad definition of SNR
        if 'JD-MID' not in spec.meta.keys():
            spec.meta['JD-MID'] = spec.meta['JD'] # dealing with inconsistencies between fits-files
        for special_header in ('HISTORY', 'COMMENT'): 
            spec.meta[special_header] = [str(hist) for hist in spec.meta[special_header]] # dealing with multi-line history and comment

        # shift to heliocentric system
        spec.telluric_shift()

        # define my own mask over skylines if non is provided
        if not spectrum['SIG_SKYLINE'].any():
            spec['mask'] = spec['mask'] | (spec['sky_flux'] > sp.median(spec['sky_flux']) * 2.)

        # add custom mask
        for single_mask in to_mask:
            spec['mask'] = spec['mask'] | ((spec['wavelength'] >= min(single_mask)) & (spec['wavelength'] <= max(single_mask)))

        spec['maxchi'] = sp.infty

        # mask hydrogen lines
        #Rydberg constant / (1 + (electron mass / proton mass)) = 0.00109677583 angstrom^(-1)
        hydrogen_wavelengths = [(0.00109677583 * (1./16. - 1. / nlevel ** 2)) ** -1 for nlevel in range(11, 11 + n_hydrogen)]
        for wavelength in hydrogen_wavelengths:
            spec['mask'] = spec['mask'] | (abs(spec['wavelength'] - wavelength) < mask_hydrogen)
    if split_chips:
        return list(spec.T)
    return spec


class ApogeeSpectrum(sp.recarray):
    """An observed Apogee spectrum

    This is an array with the wavelength, flux, error, sky emission flux, telluric spectrum, etc.
    Properties:
    - `meta`: all the APOGEE header file data.
    - `heliocentric`: if True wavelength of spectrum is in heliocentric system otherwise it is in telluric system.
    """
    meta = None

    def __array_finalize__(self, obj):
        """Called when a new ApogeeSpectrum object is created.

        If the new object has an ApogeeSpectrum has a template, copy the meta-data.
        """
        if isinstance(obj, ApogeeSpectrum):
            self.meta = obj.meta
            self._heliocentric = obj._heliocentric

    @classmethod
    def from_list(cls, spectra_list):
        """Creates a single ApogeeSpectrum out of a list of ApogeeSpectra.

        The new spectrum will have the same meta information as the first input spectrum. Note that this means that the switching between heliocentric and telluric systems will be bugged after merging the spectra.
        Not used anymore.

        Arguments:
        - `spectra_list`: A list of ApogeeSpectra.
        """
        new_spec = sp.concatenate([spectrum[..., None] for spectrum in spectra_list], -1).view(cls)
        new_spec.meta = spectra_list[0].meta
        new_spec._heliocentric = spectra_list[0]._heliocentric
        return new_spec

    _heliocentric = False
    @property
    def heliocentric(self, ):
        return self._heliocentric

    @heliocentric.setter
    def heliocentric(self, value):
        if bool(self.heliocentric) != bool(value):
            self.telluric_shift()
        self.heliocentric = value

    def shift_wavelength(self, dvel):
        """Sets the wavelength to a new value, which is shifted with given velocity shift

        Used to apply telluric shifts.
        """
        self['wavelength'] = self['wavelength'] * (1 + dvel / 299792.458)

    def telluric_shift(self, ):
        """shifts between the telluric wavelength system and the heliocentric wavelength system.

        Uses the heliocentric correction from the APOGEE header files.
        Setting the heliocentric prameter directly changes this value.
        """
        dvel = heliocentric.vhelio(self.meta['RA'], self.meta['DEC'], self.meta['JD-MID'])
        if not self.heliocentric:
            dvel = -dvel
        self._heliocentric = not self.heliocentric
        self.shift_wavelength(dvel)

"""Bugged, old code, but does illustrate some plotting capabilities of the SpectralFit objects.
"""
import matplotlib.pyplot as plt
import scipy as sp
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
import fit
from inspect import getargspec
from matplotlib.backends.backend_pdf import PdfPages


class BasePlotter(object):
    """Basic plotter object."""
    def __init__(self, spectralfit, arr_fit=None):
        """Create these objects by calling .plot() on a SpectralFit object.
        """
        self.spectralfit = spectralfit
        if arr_fit is None:
            self.arr_fit = spectralfit.compute_fit()[0]
        else:
            self.arr_fit = arr_fit

    @property
    def subplots(self, ):
        """Plot objects for the subfits.

        Note that the fit is not recomputed for the subplots.
        """
        for subfit, arr_fit in zip(self.spectralfit, self.arr_fit):
            if isinstance(self, FullPlotter):
                yield StarPlotter(subfit, arr_fit)
            else:
                yield EpochPlotter(subfit, arr_fit)

    @property
    def parentplot(self, ):
        """Plot of the parent of the fit object.
        """
        return self.spectralfit.parent.plot()

    def multiplot(self, plot_name, pdf_file, fig_kwargs={'figsize': (8.27, 11.69)}, ax_kwargs={}, **kwargs):
        """Creates multiple plots as a multi-page pdf.

        This method can be used to create a certain plot for every epoch or star within a larger SpectralFit object.

        Arguments:
        - `plot_name`: name of the plotting routine that should be called.
        - `pdf_file`: Either filename to which the multi-page pdf will be saved or an open PdfPages object.
        - `fig_kwargs`: Keywords to create matplotlib Figure that will be saved to pdf (default: set pdf-size to A4).
        - `ax_kwargs`: Keywords to create matplotlib Axes on the figure using Figure.add_subplot. Will only be called if plotting routine takes an axes object.
        - `kwargs`: Keyword arguments to be passed on to plot_name
        """
        toclose = not isinstance(pdf_file, PdfPages)
        if toclose:
            pdf_file = PdfPages(pdf_file)
        func = getattr(self, plot_name, None)
        if func is not None:
            fig = plt.figure(**fig_kwargs)
            if 'figure' in getargspec(func)[0]:
                kwargs.update(figure=fig)
            if 'axes' in getargspec(func)[0]:
                ax = fig.add_subplot(**ax_kwargs)
                kwargs.update(axes=ax)
            func(**kwargs)
            pdf_file.savefig(fig)
            plt.close(fig)
        else:
            if len(self.spectralfit) == 0:
                raise ValueError('Plotting method %s not found in recursive search' % plot_name)
            for subplot in self.subplots:
                subplot.multiplot(plot_name, pdf_file, fig_kwargs, ax_kwargs, **kwargs)
        if toclose:
            pdf_file.close()

    # emcee
    def emcee_linevolution(self, parameter, plot_std=True, ixburnin=100, nwalkers=None, axes=None, **kwargs):
        """Plots the evolution of the emcee walkers over the chain for given parameter.

        Arguments:
        - `parameter`: Parameter name that will be plotted.
        - `plot_std`: If True plots the evolution of the standard deviation of all walkers, rather than the individual walkers.
        - `ixburnin`: number of initial emcee steps to ignore, when computing the parameter values and uncertainties.
        - `nwalkers`: Number of walkers to plot (default: all). Ignored if plot_std is True.
        - `axes`: Matplotlib axes object on which the emcee walkers will be plotted
        - `kwargs`: Keywords to be passed on to the axes.plot method.
        """
        if parameter not in self.spectralfit.res_emcee.dtype.names:
            raise ValueError('Parameter %s was not included in the last emcee run.')
        if axes is None:
            axes = plt.axes()
        if plot_std:
            axes.plot(sp.std(self.spectralfit.res_emcee[parameter], 0))
        else:
            axes.plot(self.spectralfit.res_emcee[parameter][:nwalkers, :].T, **kwargs)
        axes.axvline(ixburnin, color='k', linestyle='--')

    def emcee_dist(self, param1, param2=None, ixburnin=100, plot_sig=(1., 2.), axes=None, **kwargs):
        """Plots the 1-sigma uncertainties of parameter `param1` vs `param2.

        If `param2` is not set or `param1` is equal to `param2` a 1D distribution (histogram) is plotted instead.

        Arguments:
        - `param1`: parameter on the x-axis.
        - `param2`: parameter on the y-axes (if None or equal to `param1` a histogram will be plotted).
        - `ixburnin`: number of initial emcee steps to ignore, when computing the parameter values and uncertainties.
        - `axes`: Matplotlib axes object on which the distribution will be plotted.
        - `kwargs`: keyword arguments to pass on to plotting method.
        """
        if axes is None:
            axes = plt.axes()
        arrx = self.spectralfit.res_emcee[param1][:, ixburnin:].flatten()
        if param2 is None or param2 == param1:
            axes.hist(arrx, bins=50, normed=True, histtype='step', **kwargs)
        else:
            arry = self.spectralfit.res_emcee[param2][:, ixburnin:].flatten()
            x = sp.linspace(min(arrx), max(arrx), 50)
            y = sp.linspace(min(arry), max(arry), 50)
            xx, yy = sp.broadcast_arrays(x[:, sp.newaxis], y[sp.newaxis, :])
            z = sp.stats.gaussian_kde((arrx, arry)).evaluate((xx.flatten(), yy.flatten())).reshape(xx.shape)
            total_z = sp.sum(z)
            sort_z = sp.argsort(z, None)
            cumsum_z = sp.cumsum(z.flat[sort_z]) / total_z
            ixlevels = [sp.where(cumsum_z > sp.special.erf(sig / sp.sqrt(2)))[0][0] for sig in plot_sig]
            levels = [z.flat[sort_z[ix]] for ix in ixlevels]
            axes.contour(x, y, z, levels, **kwargs)

    def plot_emcee(self, parameters=None, nwalkers=None, ixburnin=100, figure=None):
        """Plot the uncertainties from the last emcee run.

        Arguments:
        - `parameters`: list of parameters to include in the plot (default: all).
        - `nwalkers`: Of how many walkers the evolution should be plotted in the first row (default: all)
        - `ixburnin`: number of initial emcee steps to ignore, when computing the parameter values and uncertainties.
        - `figure`: Matplotlib figure object on which the plots will be made.
        """
        if parameters is None:
            parameters = self.spectralfit.res_emcee.dtype.names
        if figure is None:
            figure = plt.figure()
        grid = GridSpec(len(parameters), len(parameters) + 2, hspace=0., wspace=0.)
        for ixrow, rowparam in enumerate(parameters):
            if ixrow == 0:
                axwalkers = figure.add_subplot(grid[ixrow, 0])
                axstd = figure.add_subplot(grid[ixrow, 1])
                sharex_ax = [axwalkers, axstd]
            else:
                axwalkers = figure.add_subplot(grid[ixrow, 0], sharex=sharex_ax[0])
                axstd = figure.add_subplot(grid[ixrow, 1], sharex=sharex_ax[1])
            self.emcee_linevolution(rowparam, axes=axwalkers, ixburnin=ixburnin, plot_std=False)
            axwalkers.set_ylabel(rowparam)
            rowval = self.spectralfit.res_emcee[rowparam]
            rowlim = (min(rowval.flat), max(rowval.flat))
            axwalkers.set_ylim(rowlim)
            plt.setp(axstd.get_yticklabels(), visible=False)
            self.emcee_linevolution(rowparam, axes=axstd, ixburnin=ixburnin, plot_std=True)
            if ixrow == len(parameters) - 1:
                axwalkers.set_xlabel('chain')
                axstd.set_xlabel('chain')
            if ixrow == 0.:
                axwalkers.set_title('chain evolution')
                axstd.set_title('evolution of std')
            for ixcol, colparam in enumerate(parameters):
                if ixcol <= ixrow:
                    if ixrow > ixcol:
                        axdist = figure.add_subplot(grid[ixrow, ixcol + 2], sharey=axwalkers, sharex=sharex_ax[ixcol + 2])
                    else:
                        axdist = figure.add_subplot(grid[ixrow, ixcol + 2])
                        sharex_ax.append(axdist)
                    plt.setp(axdist.get_yticklabels(), visible=False)
                    if ixrow != len(parameters) - 1:
                        plt.setp(axdist.get_xticklabels(), visible=False)
                        pass
                    else:
                        axdist.set_xlabel(colparam)
                    self.emcee_dist(colparam, rowparam, ixburnin=ixburnin, axes=axdist)


class EpochPlotter(BasePlotter):
    """Contains plot commands for a single epoch.
    """

    def title(self, include_name=True, include_epoch=True, include_variable=False, include_parameters=False):
        """String giving a suggested title for the plots.

        - `include_name`: Include the stellar name in the plot.
        - `include_epoch`: Include the epoch number in the plot.
        - `include_variable`: Include the parameter values that vary between epochs.
        - `include_parameters`: Include all parameter values (if True `include_variable` is ignored).
        """
        to_include = []
        if include_name:
            to_include.append(self.spectralfit.meta('OBJID'))
        if include_epoch:
            to_include.append('Epoch (%i/%i)' % (self.spectralfit.index[-1] + 1, len(self.spectralfit.parent)))
        if include_parameters or include_variable:
            for name in self.spectralfit.parameters.possible_keys:
                if name not in 'poly_params' and (include_parameters or self.spectralfit.parent.parameters.varies(name)):
                    to_include.append('%s: %.1f' % (name, self.spectralfit.parameters[name]))
        return '; '.join(to_include)

    def plot_spectrum(self, normalized=False, shift_RV=False, chilim=None, fluxlim=None, wavelim=None, figure=None):
        """Plots the observed and fitted spectrum and the Chi-distribution.

        Produces three panels with:
        - upper left: the observed (blue: used in fitting, green: masked out) and fitted (red) spectrum (optionally normalized and in rest wavelength).
        - lower left: the chi distribution over wavelength (optionally in rest wavelength).
        - lower right: Histogram of the chi-distribution (blue: used in fitting, green: all).

        Arguments:
        - `normalized`: If True plots the normalized spectrum (in upper left panel).
        - `shift_RV`: If True plots the spectrum in rest wavelength (left panels).
        - `chilim`: tuple: range of chi-values to plot (lower panels).
        - `fluxlim`: tuple: range of flux-values to plot (upper left panel).
        - `wavelim`: tuple: range of wavelengths to plot (left panels).
        """
        if figure == None:
            figure = plt.figure()
        grid = GridSpec(4, 4, wspace=0., hspace=0.)
        axmain = figure.add_subplot(grid[:3, :3])
        axchi = figure.add_subplot(grid[-1, :3], sharex=axmain)
        axhist = figure.add_subplot(grid[-1, -1], sharey=axchi)

        self.plot_observed(True, normalized, shift_RV, axmain, 'g')
        self.plot_observed(False, normalized, shift_RV, axmain, 'b')
        self.plot_theoretical(normalized, shift_RV, axmain, 'r')
        self.shade_mask(shift_RV, axes=axmain)

        self.plot_chi(shift_RV, axchi)
        self.shade_mask(shift_RV, axes=axchi)

        theor = self.arr_fit
        if isinstance(theor, list):
            theor = reduce(sp.append, theor)
        theor_mask = theor[~theor['mask']]

        if fluxlim is None:
            fluxlim = [sp.median(theor_mask['flux']) / 3., sp.median(theor_mask['flux']) * 2.]
        if chilim is None:
            chilim = [-4 * sp.std(theor_mask['chi']), 4 * sp.std(theor_mask['chi'])]
        if wavelim is not None:
            axmain.set_xlim(wavelim)
        axmain.set_ylim(fluxlim)
        axchi.set_ylim(chilim)

        axhist.hist(theor['chi'][sp.isfinite(theor['chi'])], range=chilim, bins=100, histtype='step', orientation='horizontal', color='g')
        axhist.hist(theor_mask['chi'], range=chilim, bins=100, histtype='step', orientation='horizontal', color='b')

        axmain.set_title(self.title())
        axmain.set_ylabel('Flux')
        axchi.set_ylabel(r'$\chi$')
        axchi.set_xlabel('Wavelength')
        axhist.set_xlabel('N')
        return axmain, axchi, axhist

    def plot_theoretical(self, normalized=False, shift_RV=False, axes=None, color=None, **kwargs):
        """Plots the current theoretical spectrum.

        Arguments:
        - `normalized`: If True, do not shift the continuum to the continuum of the observed spectrum.
        - `shift_RV`: If True, do not shift the wavelength with the radial velocity.
        - `axes`: matplotlib axes object on which the theoretical spectrum is plotted.
        - `kwargs`: Any additional keywords are passed on to axes.plot.
        """
        if axes is None:
            axes = plt.axes()
        if color is None:
            color = next(axes._get_lines.color_cycle)
        if len(self.spectralfit) != 0:
            for subplot in self.subplots:
                subplot.plot_theoretical(normalized, shift_RV, axes, color, **kwargs)
            return
        to_apply = list(self.spectralfit.parameters.convert_keys)
        if normalized:
            to_apply.remove('poly_params')
        if shift_RV:
            to_apply.remove('vrad')
        toplot = self.spectralfit.compute_fit(to_apply)[0]
        axes.plot(toplot['wavelength'], toplot['theoretical'], color=color, **kwargs)

    def plot_observed(self, masked=True, normalized=False, shift_RV=False, axes=None, color=None, **kwargs):
        """Plots the observed spectrum

        Arguments:
        - `masked`: If False only plot the unmasked parts of the spectrum.
        - `normalized`: If True, shift the continuum to that of the theoretical spectrum.
        - `shift_RV`: If True, shift the wavelength to rest wavelength (according to the current RV).
        - `axes`: matplotlib axes object on which the observed spectrum is plotted.
        - `color`: Matplotlib color for the plot.
        - `kwargs`: Any additional keywords are passed on to axes.plot.
        """
        if axes is None:
            axes = plt.axes()
        if color is None:
            color = next(axes._get_lines.color_cycle)
        if len(self.spectralfit) != 0:
            for subplot in self.subplots:
                subplot.plot_observed(masked, normalized, shift_RV, axes, color, **kwargs)
            return
        observed = self.spectralfit.observed(normalized=normalized, shift_RV=shift_RV)
        if masked:
            axes.plot(observed['wavelength'], observed['flux'], color=color, **kwargs)
        else:
            start_mask = list(sp.where(sp.logical_and(observed['mask'][:-1], ~observed['mask'][1:]))[0] + 1)
            end_mask = list(sp.where(sp.logical_and(observed['mask'][1:], ~observed['mask'][:-1]))[0] + 1)
            if end_mask[0] < start_mask[0]:
                start_mask.insert(0, None)
            if end_mask[-1] < start_mask[-1]:
                end_mask.append(None)
            for ixstart, ixend in zip(start_mask, end_mask):
                axes.plot(observed['wavelength'][ixstart:ixend], observed['flux'][ixstart:ixend], color=color, **kwargs)

    def shade_mask(self, shift_RV=False, color='gray', alpha=0.5, axes=None, **kwargs):
        """Shades the area covered by the mask.

        Uses axes.axvspan to shade the area.

        Arguments:
        - `shift_RV`: If True, shift the wavelength to rest wavelength (according to the current RV).
        - `color`: Color of the shaded region.
        - `alpha`: Transparancy factor of the shaded region.
        - `axes`: Matplotlib axes object, which will be shaded.
        """
        if axes is None:
            axes = plt.axes()
        if len(self.spectralfit) != 0:
            for subplot in self.subplots:
                subplot.shade_mask(shift_RV, color, alpha, axes, **kwargs)
            return
        observed = self.spectralfit.observed(shift_RV=shift_RV)
        start_mask = list(sp.where(sp.logical_and(observed['mask'][:-1], ~observed['mask'][1:]))[0] + 1)
        end_mask = list(sp.where(sp.logical_and(observed['mask'][1:], ~observed['mask'][:-1]))[0] + 1)
        if start_mask[0] < end_mask[0]:
            end_mask.insert(0, 0)
        if start_mask[-1] < end_mask[-1]:
            start_mask.append(-1)
        for ixstart, ixend in zip(start_mask, end_mask):
            axes.axvspan(observed['wavelength'][ixend], observed['wavelength'][ixstart], color=color, alpha=alpha, **kwargs)
        return axes

    def plot_chi(self, shift_RV=False, axes=None, color=None, marker='+', **kwargs):
        """Plots the distribution of chi

        Arguments:
        - `shift_RV`: Shifts the wavelength to rest wavelength (according to current RV)
        - `axes`: Matplotlib axes object on which the observed spectrum is plotted.
        - `color`: Matplotlib color for the plot.
        """
        if axes is None:
            axes = plt.axes()
        if color is None:
            color = next(axes._get_lines.color_cycle)
        if len(self.spectralfit) != 0:
            for subplot in self.subplots:
                subplot.plot_chi(shift_RV, axes, color, **kwargs)
            return
        theor = self.arr_fit
        if shift_RV:
            wavelength = self.spectralfit.observed(shift_RV=shift_RV)['wavelength']
        else:
            wavelength = theor['wavelength']
        axes.plot(wavelength, theor['chi'], color=color, **kwargs)


class StarPlotter(BasePlotter):
    """Contains the plotting routines for a single star with one or more ecochs of observations.

    Many plotting routines raise ValueErrors when only a single epoch is available.
    """
    def title(self, include_name=True, include_nepoch=True, include_constant=True, include_parameters=False):
        """Returns a proposed title for plots.

        Arguments:
        - `include_name`: Include the stellar name.
        - `include_nepoch`: Include the number of epochs.
        - `include_constant`: Include the parameter values that do nor vary along the epochs.
        - `include_parameters`: Includes all parameter values (if True overrides include_constant).
        """
        to_include = []
        if include_name:
            to_include.append(self.spectralfit.meta('OBJID'))
        if include_nepoch:
            to_include.append('%i epoch(s)' % len(self.spectralfit))
        if include_parameters or include_constant:
            for name in self.spectralfit.parameters.possible_keys:
                if name != 'poly_params' and (include_parameters or not self.spectralfit.parameters.varies(name)):
                    to_include.append('%s: %s' % (name, str(self.spectralfit.parameters[name])))
        return '; '.join(to_include)

    def plot_rms(self, shift_RV=False, normalized=False, chisq=False, pfalse=(1e-3, 1e-4), as_hist=False, log=True, axes=None, **kwargs):
        """Plot the RMS between multiple epochs as a function of wavelength or as a histogram (if as_hist is True).

        No comparison with the fitted spectra is used, although the fits are used to optionally correct for RV shifts and normalize the spectra.
        Note that the wavelengths of the observed spectra will not match up, so interpolation will take place between pixels. Thus neigbouring RMS values are not independent.

        Arguments:
        - `shift_RV`: Shifts the wavelengths to rest wavelength, so that simple single-line spectroscopic binaries should not show variability.
        - `normalized`: Normalizes the spectra to the continuum of the theoretical spectrum.
        - `chisq`: If True plots the chi-squared relative to the weighted mean (essentially taken into account the uncertainties), rather than the RMS.
        - `pfalse`: TODO; Mark the boundaries above which there will be `pfalse` false detection probability. The first will be marked with a dotted line, the second with a dashed line.
        - `as_hist`: Plots the histogram, rather than the individual points as a function of wavelength
        - `log`: if True the RMS axis will be logarithmic.
        - `axes`: Matplotlib axes object on which the plot is made.
        - `kwargs`: Additional keywords passed on to axes.hist(if as_hist is True) or axes.plot (if as_hist is False).
        """
        if axes is None:
            axes = plt.axes()
        toplot = fit.flatten(self.spectralfit.observed(shift_RV=shift_RV, normalized=normalized))
        if len(toplot) == 1:
            raise ValueError('Can not plot RMS, as only a single epoch is available for star %s' % self.spectralfit.meta('OBJID'))
        if any([len(chip_obs.shape) > 1 for chip_obs in toplot]):
            raise ValueError('Plotting RMS only works if all observed spectra are one-dimensional (ensure that split_chips was True when loading the spectra)')
        wavelength = reduce(sp.append, toplot)['wavelength']
        wavelength.sort()
        flux = sp.zeros((wavelength.size, len(toplot)))
        error = sp.zeros((wavelength.size, len(toplot)))
        for ixchip, chip_obs in enumerate(toplot):
            unmasked = chip_obs.copy()
            unmasked['flux'][unmasked['mask']] = sp.nan
            unmasked['error'][unmasked['mask']] = sp.nan
            unmasked.sort(order=['wavelength'])
            flux[:, ixchip] = sp.interp(wavelength, unmasked['wavelength'], unmasked['flux'], sp.nan, sp.nan)
            error[:, ixchip] = sp.interp(wavelength, unmasked['wavelength'], unmasked['error'], sp.nan, sp.nan)
        nvalid = sp.sum(sp.isfinite(flux), -1)
        if chisq:
            weights = error ** (-2.)
        else:
            weights = sp.isfinite(flux)
        weighted_mean = sp.nansum(weights * flux, -1) / sp.nansum(weights, -1)
        rms = sp.nansum((flux - weighted_mean[:, sp.newaxis]) ** 2. * weights, -1)
        rms[nvalid <= 1] = sp.nan
        if as_hist:
            bins = kwargs.pop('bins', 10)
            bin_range = kwargs.pop('range', (max((min(rms[nvalid > 1]), 1e-3)), max(rms[nvalid > 1])))
            if log:
                try:
                    bins = sp.linspace(sp.log(min(bin_range)), sp.log(max(bin_range)), bins)
                except TypeError:
                    pass
            axes.hist(rms[nvalid > 1], bins=bins, **kwargs)
            if log:
                axes.set_xscale('log')
        else:
            axes.plot(wavelength, rms, **kwargs)
            if log:
                axes.set_yscale('log')

    def plot_parameter_evolution(self, asy='vrad', asx='HJD', ascolor=None, axes=None, marker='x', colorbar=True, **kwargs):
        """Plots the evolution of a parameter over multiple epochs.

        Arguments:
        - `asy`: To plot on the y-axis (can be a parameter or a value from the pyfits header) (default: radial velocity).
        - `asx`: To plot on the x-axis (can be a parameter or a value from the pyfits header) (default: Heliocentric Julian Date).
        - `ascolor`: To plot as color (can be a parameter or a value from the pyfits header) (default: no color variation)
        - `axes`: matplotlib axes object on which the data will be plotted.
        - `marker`: Symbol type to use in plot.
        - `colorbar`: If True, plot a color bar next to the axes.
        - `kwargs`: additional keywords to pass on to axes.scatter.
        """
        if axes is None:
            axes = plt.axes()
        toplot = []
        for name_toplot in [asx, asy, ascolor]:
            if name_toplot is None:
                continue
            elif name_toplot in self.spectralfit.parameters.possible_keys:
                if not self.spectralfit.parameters.varies(name_toplot):
                    raise ValueError('Parameter %s has not been varied across epochs, so its evolution can not be plotted' % name_toplot )
                toplot.append(self.spectralfit.parameters[name_toplot])
            elif name_toplot == 'chisq':
                toplot.append([subfit.compute_fit()[1] / subarr.datasize for subfit, subarr in zip(self.spectralfit, self.arr_fit)])
            else:
                toplot.append(self.spectralfit.meta(name_toplot))
            toplot[-1] = sp.atleast_1d(toplot[-1])
            if toplot[-1].size == 1:
                toplot[-1] = sp.zeros(len(self.spectralfit)) + toplot[-1][0]
        if len(toplot) == 3:
            kwargs.update(c=toplot[2])
            axes.set_title('color set by %s' % ascolor)
        sc = axes.scatter(toplot[0], toplot[1], marker=marker, **kwargs)
        if len(toplot) == 3 and colorbar:
            plt.colorbar(sc)
        axes.set_xlabel(asx)
        axes.set_ylabel(asy)

    def plot_rms_binarity(self, normalized=True, chisq=False, figure=None, wavelim=None, fluxlim=None, rmslim=None):
        """Plots the reduction of the RMS due to corrections from binarity.

        Arguments:
        - `normalized`: If True, normalize the spectra to theoretical continuum before plotting.
        - `chisq`: If True, plot the RMS normalized by the measurement uncertainty.
        - `figure`: Matplotlib Figure object on which the plots will be made.
        - `wavelim`: Wavelength range to plot.
        - `fluxlim`: Flux range to plot.
        - `rmslim`: RMS range to plot.
        """
        if len(self.spectralfit) == 1:
            raise ValueError('Can not plot effect of binarity on RMS, if only a single epoch has been observed.')
        if figure is None:
            figure = plt.figure()
        inigrid = GridSpec(13, 4)
        axvel = figure.add_subplot(inigrid[:3, :2])
        axhist = figure.add_subplot(inigrid[:3, 2:])
        subgrid = GridSpecFromSubplotSpec(10, 1, inigrid[3:, :], hspace=0)
        axmainbefore = figure.add_subplot(subgrid[:3, 0])
        axrmsbefore = figure.add_subplot(subgrid[3:5, 0], sharex=axmainbefore)
        axmainafter = figure.add_subplot(subgrid[5:8, 0], sharex=axmainbefore)
        axrmsafter = figure.add_subplot(subgrid[8:, 0], sharex=axmainbefore)
        axrmsafter.set_xlabel('Wavelength')
        if wavelim is not None:
            axmainbefore.set_xlim(wavelim)

        self.plot_parameter_evolution('vrad', 'HJD', axes=axvel, c='b', label='Own analysis')
        self.plot_parameter_evolution('VHELIO', 'HJD', axes=axvel, c='g', marker='x', label='APOGEE pipeline')
        axvel.set_ylabel('radial velocity')
        axvel.set_xlabel('Heliocentric Julian Date')
        #axvel.legend()
        axvel.set_title(self.title())

        for axmain, axrms, shift_RV in [(axmainbefore, axrmsbefore, False), (axmainafter, axrmsafter, True)]:
            for subplot, hjd in zip(self.subplots, self.spectralfit.meta('HJD')):
                subplot.plot_observed(normalized=normalized, shift_RV=shift_RV, masked=False, axes=axmain)
            self.plot_rms(shift_RV=shift_RV, normalized=normalized, chisq=chisq, axes=axrms, as_hist=False)
            if chisq:
                axrms.set_ylabel(r'$\chi^2$')
            else:
                axrms.set_ylabel('RMS')
            if rmslim is not None:
                axrms.set_ylim(rmslim)
            axmain.set_ylabel('Flux')
            if fluxlim is not None:
                axmain.set_ylim(fluxlim)
        ylim = axrmsbefore.get_ylim()
        axrmsafter.set_ylim(ylim)
        self.plot_rms(shift_RV=False, normalized=normalized, chisq=chisq, axes=axhist, as_hist=True, bins=100, range=ylim, histtype='step', label='Before RV shift')
        self.plot_rms(shift_RV=True, normalized=normalized, chisq=chisq, axes=axhist, as_hist=True, bins=100, range=ylim, histtype='step', label='After RV shift')
        if chisq:
            axhist.set_xlabel(r'$\chi^2$')
        else:
            axhist.set_xlabel('RMS')
        axhist.set_ylabel('N')
        #axhist.legend()
        figure.tight_layout()

        return axvel, axhist, axmainbefore, axrmsbefore, axmainafter, axrmsafter

class FullPlotter(BasePlotter):
    """Contains plotting commands for SpectralFits which contain spectra of multiple stars.
    """

    def summary(self, figure=None):
        """Creates various scatter plots, summarizing the measured values.

        WARNING: Colors might be assigned inconsistently.
        """
        for ixsubplot, (asx, asy, ascolor, keywords) in enumerate([('Teff', 'logg', 'eTeff', {'marker': '-'}), ('Teff', 'vrad', 'evrad', {}), ('SNR', 'evrad', 'Teff', {}),
                                                                   ('Teff', 'veiling', 'SNR', {'marker': '-'}), ('vrad', 'VHELIO', 'evrad', {}), ('Hmag', 'evrad', 'Teff', {})]):
            axes = figure.add_subplot(3, 2, ixsubplot + 1)
            if 'marker' not in keywords.keys():
                keywords['marker'] = '.'
            for subplot in self.subplots:
                subplot.plot_parameter_evolution(asx, asy, ascolor, colorbar=False, axes=axes, **keywords)

"""Adds literature data to the table with fits results
"""
import os
import scipy as sp
from astroquery import simbad
try:
    import pyfits
except ImportError:
    from astropy.io import fits as pyfits

directory = os.path.join(os.path.split(__file__)[0], 'literature')


def get_data(specfits):
    """Adds literature data to structured array.
    """
    ra = sp.array([sp.median(coord) for coord in specfits['h_RA']])
    dec = sp.array([sp.median(coord) for coord in specfits['h_DEC']])
    result = specfits.copy()

#    for input_func in [ic348_Alexander12]:
    for input_func in [ic348_Luhman2003, pleiades_Soderblom93, pleiades_Soderblom09, ngc2264_Furesz2006, pleiades_Mermilliod2009, ic348_Nordhagen2006, pleiades_Terndrup2000, ic348_twomass, ic348_Bell2013, ic348_Mayne2007, ic348_Dahm2008, pleiades_Bell2012, orion_DaRio, ic348_Cieza06, ic348_Cohen04, ic348_Littlefair05, ic348_Alexander12]:
        label, rasub, decsub, data, max_dist = input_func()
        rasub = sp.asarray(rasub)
        decsub = sp.asarray(decsub)
        distance = distance(ra[:, sp.newaxis], dec[:, sp.newaxis], rasub[sp.newaxis, :], decsub[sp.newaxis, :])
        ixclosest = sp.argmin(distance, 0)
        mutual= sp.argmin(distance, 1)[ixclosest] == sp.arange(distance.shape[1])
        distance_close = distance[ixclosest, sp.arange(distance.shape[1])][mutual]
        new_result = sp.zeros(specfits.size, dtype=result.dtype.descr + [('l_%s_%s' % (label, name), dt) for name, dt in data.dtype.descr] + [('l_%s_distance' % label, 'f8'), ('l_%s' % label, 'bool')])
        for name in new_result.dtype.names:
            if name in result.dtype.names:
                new_result[name] = result[name]
            elif name == 'l_%s_distance' % label:
                new_result[name] = sp.nan
                new_result[name][ixclosest[mutual]] = distance_close
            elif name == 'l_%s' % label:
                new_result[name][ixclosest[mutual]] = distance_close < max_dist
        for name in data.dtype.names:
            new_result['l_%s_%s' % (label, name)][ixclosest[mutual]] = data[name][mutual]
        result = new_result

    full_names = list(specfits['h_OBJID'])
    for input_func in []:
        label, data, twomass_names = input_func()
        new_result = sp.zeros(specfits.size, dtype=result.dtype.descr + [('l_%s_%s' % (label, name), dt) for name, dt in data.dtype.descr] + [('l_%s' % label, 'bool')])
        for name in new_result.dtype.names:
            if name in result.dtype.names:
                new_result[name] = result[name]
        for ixobjid, objid in enumerate(twomass_names):
            print objid, objid in full_names, full_names[0] in full_names
            if objid in full_names:
                specfits_index = full_names.index(name)
                for name in data.dtype.names:
                    new_result['l_%s_%s' % (label, name)][specfits_index] = data[name][ixobjid]
        result = new_result
    return new_result


def get_ra_dec(names):
    connection = simbad.Simbad()
    position = []
    for name in names:
        simbad_obj = connection.query_object(name)
        position.append((strToDeg(simbad_obj['RA'].data[0]) * 15., strToDeg(simbad_obj['DEC'].data[0]) ))
    return zip(*position)


def pleiades_Terndrup2000():
    """Loads in the radial and rotational velocities from Tendrup (2000) for 59 Pleiades stars.
    """
    arr = sp.genfromtxt(os.path.join(directory, 'pleiades', 'Terndrup2000.dat'), delimiter='\t', names=True, dtype=['S3'] + ['f8'] * 4 +  ['S3'] * 3 + ['S10'])
    objid = ['Cl* Melotte 22 HCG %s' % identifier for identifier in arr['HCG'] if identifier != '-']
    objid.extend(arr['Other_designation'][-3:])
    ra, dec = get_ra_dec(objid)
    return 'T00', ra, dec, arr, 1e-3


def pleiades_Mermilliod2009():
    """Loads in the radial and rotational velocities from Mermilliod (2009) for 275 Pleiades stars.
    """
    arr = sp.genfromtxt(os.path.join(directory, 'pleiades', 'Mermilliod2009.dat'), delimiter='|', skip_header=62, names=True, dtype=['i4', 'S9', 'i4', 'S1'] + ['S11'] * 3 + ['f8'] * 4 +  ['i4'] + ['f8'] * 2 + ['i4', 'f8', 'S8'])
    subarr = arr[sp.array([cluster_name == 'Pleiades ' for cluster_name in arr['Cluster']], dtype='bool')]
    print subarr.size, arr.size
    ra = [strToDeg(str_angle) * 15. for str_angle in subarr['RAJ2000']]
    dec = [strToDeg(str_angle) for str_angle in subarr['DEJ2000']]
    return 'M09', ra, dec, subarr, 10 ** (-2.5)


def ic348_Nordhagen2006():
    """Loads in the periods, radial, and rotational velocities from Nordhagen (2006) for 30 IC 348 stars
    """
    arr = sp.genfromtxt(os.path.join(directory, 'ic348', 'Nordhagen2006.dat'), delimiter='\t', names=True, dtype=['S3'] + ['S4'] + ['f8'] * 7)
    objid = ['Cl* IC 348 HMW %s' % identifier for identifier in arr['HMW'] if identifier != '-']
    objid[-2] = 'Cl* IC 348 LRL 67'
    objid[-1] = 'Cl* IC 348 LRL 63'
    ra, dec = get_ra_dec(objid)
    return 'N06', ra, dec, arr, 1e-4


def ngc2264_Furesz2006():
    """Loads in the radial velocity from Furesz et al. (2006).
    """
    arr = sp.array(pyfits.getdata(os.path.join(directory, 'ngc2264', 'Furesz06.fit')))
    return 'F06', arr['_RA'], arr['_DE'], arr, 1e-3


def ic348_twomass():
    """Loads in the 2MASS photometry for IC 348.
    """
    filename = os.path.join(directory, 'ic348', '2MASS_from_vizier.dat')
    arr = sp.genfromtxt(filename, skip_header=172, names=True, delimiter='|')
    return '2MA', arr['RAJ2000'], arr['DEJ2000'], arr, 1e-4


def ic348_Bell2013():
    """Loads in the optical photometry from Bell et al. (2013)
    """
    filename = os.path.join(directory, 'ic348', 'Naylor_cluster_collaboration', 'ugriz_phot.fits')
    arr = pyfits.getdata(filename, 1)
    return 'B13', arr['RA'], arr['DEC'], arr, 1e-4


def ic348_Mayne2007():
    """Loads in the optical photometry from Mayne et al. (2007)
    """
    filename = os.path.join(directory, 'ic348', 'Naylor_cluster_collaboration', 'BVI_Mayne07.fit')
    arr =  pyfits.getdata(filename, 1)
    return 'M07', arr['RAJ2000'], arr['DEJ2000'], arr, 1e-4


def ic348_Dahm2008():
    """Loads in the RVs, v sini, EW(Halpha), and EW(Li) from Dahm (2008)
    """
    filename = os.path.join(directory, 'ic348', 'Dahm2008.dat')
    arr = sp.genfromtxt(filename, delimiter='|', skip_header=2, names=True, dtype=['S6', 'S15'] + ['f8'] * 12 + ['S30', 'S50'])[2:]
    for name in ['ST', 'Other_emission', 'Comments']:
        arr[name] = [value.strip() for value in arr[name]]
    objid = ['Cl* IC 348 LRL %s' % identifier[1:] for identifier in arr['Identifier']]
    ra, dec = get_ra_dec(objid)
    return 'D08', ra, dec, arr, 1e-4


def pleiades_Bell2012():
    """Loads in the optical photometry from Bell (2012)
    """
    filename = os.path.join(directory, 'pleiades', 'Bell2012.fits')
    arr = sp.array(pyfits.getdata(filename, 1))
    return 'B12', arr['RA'], arr['DEC'], arr, 1e-3


def ic348_Luhman2003():
    """Loads in the IC 348 census from Luhman et al. (2003)
    """
    filename = os.path.join(directory, 'ic348', 'Luhman2003.fit')
    arr = pyfits.getdata(filename, 1)
    return 'L03', arr['_RAJ2000'], arr['_DEJ2000'], arr, 1e-3


def pleiades_Soderblom93():
    """Loads in the Soderblom et al. (1993) FGK census for the Pleiades
    """
    filename = os.path.join(directory, 'pleiades', 'Soderblom93.fit')
    arr = pyfits.getdata(filename, 1)
    arr = arr[arr['fgk'] != 'k531']
    objid = ['Cl* Melotte 22 SSHJ %s' % name for name in arr['fgk']]
    ra, dec = get_ra_dec(objid)
    return 'S93', ra, dec, arr, 1e-4


def pleiades_Soderblom09():
    """Loads in the Soderblom et al. (2009) synthetic Teff.
    """
    filename = os.path.join(directory, 'pleiades', 'Soderblom09.dat')
    arr = sp.genfromtxt(filename, skip_header=3, names=True)
    objid = ['HII %i' % name for name in arr['H_II']]
    ra, dec = get_ra_dec(objid)
    return 'S09', ra, dec, arr, 1e-4

def orion_DaRio():
    """Loads in the overview table sent by Nicola da Rio for Orion Teff/logL/Av
    """
    filename = os.path.join(directory, 'orion', 'HRD_DaRio2012.txt')
    arr = sp.genfromtxt(filename, names=True, dtype=['i4'] + ['f8'] * 8 + ['i4', 'S8'])
    result = sp.zeros(arr.shape, dtype=arr.dtype.descr + [('Teff', '<f8'), ('eTeff', '<f8')])
    for name in arr.dtype.names:
        result[name] = arr[name]
    result['Teff'] = 10 ** result['logT']
    result['eTeff'] = sp.log(10) * result['Teff'] * result['err_logT']
    return 'D13', result['RA'], result['Dec'], result, 10 ** (-3.5)

def ic348_Cieza06():
    """Loads in the periods from Cieza & Baliber (2006)
    """
    data = pyfits.getdata(os.path.join(directory, 'ic348', 'Cieza2006.fit'))
    return 'C06', data['RAJ2000'], data['DEJ2000'], data, 10 ** (-3.5)

def ic348_Cohen04():
    """Loads in the periods from Cohen et al. (2004)
    """
    data = pyfits.getdata(os.path.join(directory, 'ic348', 'Cohen2004.fit'))
    return 'C04', data['_RAJ2000'], data['_DEJ2000'], data, 1e-3

def ic348_Littlefair05():
    """Loads in the periods from Littlefair et al. (2005)
    """
    data = pyfits.getdata(os.path.join(directory, 'ic348', 'Littlefair2005.fit'))
    return 'L05', data['RAJ2000_1_'], data['DEJ2000_1_'], data, 10 ** (-3.5)

def ic348_Alexander12():
    """Loads in the periods from Alexander & Preibisch (2012)j
    """
    data = pyfits.getdata(os.path.join(directory, 'ic348', 'Alexander12.fit'))
    return 'A12', data['RAJ2000'], data['DEJ2000'], data, 10 ** (-3.5)





# Helper functions

def degToStr(angle, delimiter=':', sig=2, print_after=False):
    """converts an angle into a string describing the angle in degrees:arcminutes:arcseconds or hours:minutes:seconds.

    Arguments:
    - `angle`: angle in degrees or hours.
    - `delimiter`: tuple, giving the splits between the degrees or hours and the hours/minutes. If the tuple has length 3, the third one will be added before the dot in the seconds (i.e. 16:27:35<third delimiter>.16). If the delimiter is a single string, this one will be used in the first two places.
    If set to 'deg' or 'hour' the delimiters will be set to latex versions to denote the coordinates.
    - `sig`: number of significant numbers, the seconds is given in. If negative, the seconds are not given (e.g. sig=-1 leads to 16:27.3, sig=-2 to 16:27, sig=-3 to 16.5 and sig=-4 to 16)
    - `print_after`: print the delimiter, even if there is nothing behind it.
    """
    if delimiter == 'deg':
        delimiter=('^{\circ}','^{\prime}','^{\prime\prime}')
        print_after=True
    elif delimiter == 'hour':
        delimiter=('^{\mathrm{h}}','^{\mathrm{m}}','^{\mathrm{s}}')
        print_after=True
    elif isinstance(delimiter,basestring):
        delimiter=(delimiter,delimiter,'')
    elif len(delimiter) == 2:
        delimiter=(delimiter[0],delimiter[1],'')
    if sig == -4 or sig == -3:
        if sig == -4:
            result=str(int(sp.around(angle)))
            if print_after:
                result=result+delimiter[0]
            return result
        else:
            result=str(sp.around(angle,1))
            if print_after:
                result=result[:-2]+delimiter[0]+result[-2:]
            return result
    first=str(int(angle))+delimiter[0]
    minutes=(angle-int(angle))*60.
    if sig == -2 or sig == -1:
        if sig == -2:
            result=first+'{0:02d}'.format(int(abs(minutes)))
            if print_after:
                result=result+delimiter[1]
            return result
        else:
            result=first+'{0:04.1f}'.format(abs(minutes))
            if print_after:
                result=result[:-2]+delimiter[1]+result[-2:]
            return result
    first=first+'{0:02d}'.format(int(abs(minutes)))+delimiter[1]
    seconds=(minutes-int(minutes))*60.
    if sig == 0:
        result=first+'{0:02d}'.format(int(abs(seconds)))
        if print_after:
            result=result+delimiter[2]
        return result
    else:
        result=first+('{0:0'+str(3+sig)+'.'+str(sig)+'f}').format(abs(seconds))
        if print_after:
            result=result[:-1-sig]+delimiter[2]+result[-1-sig:]
        return result


def strToDeg(name, delimiter=None):
    """converts a string of RA or Dec to degrees.

    Arguments:
    - `name`: string to be converted (or an iterable containing hours/degrees, minutes, seconds)
    - `delimiter`: character splitting the hours/degrees from minutes and seconds. Delimiter should not be a number or a dot. (default: delimiter is the first character in the stripped name, that is not a number or a '+' or '-' sign.)
    """
    if isinstance(name, basestring):
        if delimiter == None:
            name_short = name.strip()
            for character in name_short:
                if character not in '+-0123456789':
                    delimiter = character
                    break
        parts = name.strip().split(delimiter)
    else:
        parts = name
    units = abs(sp.float64(parts[0]))
    if len(parts) >= 3:
        units = units + sp.float64(parts[2]) / 3600.
    if len(parts) >= 2:
        units = units + sp.float64(parts[1]) / 60.
    if sp.float64(parts[0]) < 0:
        units = -units
    return units


def distance(ra1, dec1, ra2, dec2):
    """calculates the distance in degrees using the Vincenty formula

    Arguments:
    - `ra1`: right ascension of the first object in degrees.
    - `dec1`: declination of the first object in degrees.
    - `ra2`: right ascension of the second object in degrees.
    - `dec2`: declination of the second object in degrees.
    """
    dRA = sp.pi / 180. * (ra2 - ra1)
    d1 = dec1 * sp.pi / 180.
    d2 = dec2 * sp.pi / 180.
    dphi = sp.arctan2(sp.sqrt((sp.cos(d2) * sp.sin(dRA)) ** 2. + (sp.cos(d1) * sp.sin(d2) - sp.sin(d1) * sp.cos(d2) * sp.cos(dRA)) ** 2.),
                    sp.sin(d1) * sp.sin(d2) + sp.cos(d1) * sp.cos(d2) * sp.cos(dRA))
    return dphi * 180. / sp.pi

import scipy as sp
import scipy.integrate
import scipy.stats
import scipy.interpolate
import matplotlib.pyplot as plt
import functools
from matplotlib import patches, lines


def magnitude(qslope=0, fbin=0.5, Lslope=3):
    """outer curried function to compute probability of measuring given magnitude offset.

    Arguments:
    - `qslope`: slope of power-law mass ratio distribution (qslope=0 is flat)
    - `fbin`: binary fraction.
    - `Lslope`: F_J ~ M^Lslope to compute brightness of secondary star.
    """
    deltaM_offset = sp.integrate.quad(lambda q: -2.5 * sp.log10(1 + q ** Lslope) * q ** qslope * (qslope + 1), 0., 1)[0] * fbin
    def prob_magoffset(deltaMag, sigmaMag, rel_radius=1):
        """inner curried function to compute probabiliyt of measuring given magnitude offset.

        Arguments:
        - `deltaMag`: offset from median value in magnitude.
        - `sigmaMag`: uncertainty in measured magnitude.
        - `rel_radius`: relative radius of star with respect to from median value.
        """
        gaussian = lambda offset: sp.exp(- (offset + 5 * sp.log10(rel_radius) + deltaM_offset) ** 2. / (2 * sigmaMag ** 2.)) / sp.sqrt(2 * sp.pi) / sigmaMag
        prob_single = gaussian(deltaMag)
        if fbin == 0:
            return prob_single
        prob_multi = sp.integrate.quad(lambda q: gaussian(deltaMag + 2.5 * sp.log10(1 + q ** 3.5)) * q ** qslope * (qslope + 1), 0., 1)[0]
        return prob_multi * fbin + prob_single * (1 - fbin)
    return sp.vectorize(prob_magoffset)

def surface_gravity(delta_g, sigma_g, rel_radius=1, mass_slope=0.):
    """function to compute probability of measuring given log(g) offset.
    """
    slope = mass_slope - 2
    g_expected = slope * sp.log10(rel_radius)
    return sp.exp(- (delta_g - g_expected) ** 2. / (2 * sigma_g ** 2.)) / sp.sqrt(2 * sp.pi) / sigma_g

@sp.vectorize
def rsini(log_delta_r, log_sigma_r, rel_radius=1):
    """function to compute probabiliy of measuring given v sin i
    """
    if not sp.isfinite(log_delta_r):
        return sp.nan
    return sp.integrate.quad(lambda cosi: sp.exp(-(log_delta_r - sp.log10(rel_radius * sp.sqrt(1 - cosi ** 2) / 0.88)) ** 2 / (2 * log_sigma_r ** 2)), 0, 1)[0] / sp.sqrt(2 * sp.pi) / log_sigma_r


class LuminosityFit(object):
    """Fit to a luminosity distribution as estimated by extinction-corrected magnitude, surface gravity, and rotational velocity/period."""
    mass_slope = 0
    qslope = 0
    fbin = 0.5
    Lslope = 3.5

    def __init__(self, arr):
        self.arr = arr
        self.variables = sp.zeros(self.arr.shape, dtype=[(name, 'f8') for name in ['rsini', 'logg', 'mag']])
        self.variables['rsini'] = sp.log10(self.arr.weighted('vsini')[0] * self.arr.combined_literature(['Period', 'Per']))
        self.variables['logg'] = self.arr.weighted('logg')[0]
        self.variables['mag'] = self.arr['e_relJ']

        self.noise = self.variables.copy()
        self.noise['logg'] = self.arr.weighted('logg')[1]
        self.noise['mag'] = 0.17
        self.noise['rsini'] = (self.arr.weighted('vsini')[1] / self.arr.weighted('vsini')[0]) / sp.log(10)

        self.rel_var = self.variables.copy()
        def correct(Teff, variables, name=None):
            sor = sp.argsort(Teff)
            use = sp.isfinite(variables[sor])
            var_med = sp.ndimage.filters.median_filter(variables[sor][use], 50)
            fspline = sp.interpolate.LSQUnivariateSpline(Teff[sor][use], var_med, t=[4000., 5000.], k=3)
            self.shift100K[name] = fspline(3500) - fspline(3400)
            var_spline = fspline(Teff)
            return variables - var_spline
        self.shift100K = {}
        for name in self.variables.dtype.names:
            send_var = self.variables[name]
            if name == 'rsini':
                send_var[self.arr['l_C06_Per'] > 10.] = sp.nan
            self.rel_var[name] = correct(self.arr.weighted('Teff')[0], self.variables[name], name)

        self.radii = sp.ones(arr.size)

    def correction(self, parameter_name):
        return {'rsini': 1, 'mag': -5, 'logg': self.mass_slope - 2 }[parameter_name]

    def corrected(self, ):
        """Correct parameters for radii.
        """
        arr = self.rel_var.copy()
        for name in arr.dtype.names:
            arr[name] = self.rel_var[name] - self.correction(name) * sp.log10(self.radii)
        return arr

    def prob_func(self, parameter):
        return {'rsini': rsini,
                'logg': functools.partial(surface_gravity, mass_slope=self.mass_slope),
                'mag': magnitude(self.qslope, self.fbin, self.Lslope)}[parameter]

    def probability(self, log_radius=sp.linspace(-1, 1, 200)):
        """Computes the probability for all stars at given radius.
        """
        log_radius = sp.atleast_2d(log_radius).T
        prob = sp.zeros((log_radius.size, self.rel_var.size), dtype=self.rel_var.dtype)
        for parameter in prob.dtype.names:
            pfunc = self.prob_func(parameter)
            prob[parameter] = pfunc(self.rel_var[parameter], self.noise[parameter], 10 ** log_radius)
        return log_radius, prob

    def median_star(self, log_radius=sp.linspace(-1, 1, 1000)):
        """Computes the probability of given radius offset for typical star
        """
        prob = sp.zeros(log_radius.size, dtype=self.rel_var.dtype)
        for parameter in prob.dtype.names:
            pfunc = self.prob_func(parameter)
            prob[parameter] = pfunc(0., sp.median(self.noise[parameter][sp.isfinite(self.noise[parameter])]), 10 ** log_radius)
        return log_radius, prob

    def radius_prob(self, sigma_log_radius, parameters=['rsini', 'logg', 'mag']):
        """Compute parameter probability for a log-normal radius distribution.
        """
        functions = [self.prob_dist(parameter) for parameter in parameters]
        def to_integrate(log_radius):
            prob = [function(self.rel_var[parameter], self.noise[parameter], 10 ** log_radius) for parameter, function in zip(parameters, functions)]
            return reduce(lambda a, b: a * b, prob) * sp.exp(0.5 * (log_radius / sigma_log_radius) ** 2) / sp.sqrt(2 * sp.pi) / sigma_log_radius
        logr = sp.linspace(-1, 1., 50)
        return logr, to_integrate(logr)

    def single_histogram(self, parameter, rwidth=[0, 0.07, 0.1, 0.15], axes=None):
        """Plots the histogram of offsets + expected distribution for parameter.

        Arguments:
        - `parameter`: one of ['rsini', 'logg', 'mag'].
        - `axes`: axes object to plot on.
        """
        if axes is None:
            axes = plt.axes()
        toplot = (self.arr.weighted('Teff')[1] < 100.) & (self.arr.weighted('Teff')[0] < 4500) & (self.arr.weighted('Teff')[0] > 3000) & sp.isfinite(self.rel_var[parameter])
        if parameter == 'logg':
            toplot = toplot & (self.noise['logg'] < 0.1)
        if parameter == 'mag':
            toplot = toplot # & (self.arr['e_AJ'] < 1)
        if parameter == 'rsini':
            #toplot_temp = toplot & (self.arr.weighted('vsini')[0] < 15.)
            #axes.hist(self.rel_var[parameter][toplot_temp], bins=30, range=(min(self.limits[parameter]), max(self.limits[parameter])), histtype='step', color='black')
            toplot = toplot & (self.arr.weighted('vsini')[0] > 15.) & (self.noise['rsini'] < 0.1)
        axes.hist(self.rel_var[parameter][toplot], bins=30, range=(min(self.limits[parameter]), max(self.limits[parameter])), histtype='step', lw=2)
        axes.set_xlabel(parameter)
        axes.set_ylabel('N')
        axes.set_xlim(self.limits[parameter])

        fprob = self.prob_func(parameter)
        xval = sp.linspace(self.limits[parameter][0], self.limits[parameter][1], 201)
        for rw, color in zip(rwidth, ['black', 'orange', 'red', 'magenta']):
            width = sp.sqrt(sp.mean(self.noise[parameter][toplot] ** 2) + (rw * self.correction(parameter)) ** 2)
            yprob = fprob(xval, width, 1.)
            yval = yprob * sp.sum(toplot) * abs(self.limits[parameter][1] - self.limits[parameter][0]) / 30
            axes.plot(xval, yval, color=color)

    def radius_from_logg(self, ):
        """estimate radii from log(g)"""
        self.radii = 10 ** (self.rel_var['logg'] / (self.mass_slope - 2))
        return self.radii

    limits = {'logg': (-1.4, 1.6),
              'rsini': (-0.6, 1.1),
              'mag': (2, -2.9)}

    def surface_plot(self, xparam, yparam, rwidth=sp.log10([1, 1.15, 1.25, 1.35]), with_shift=False, axes=None):
        """Plots the surface distribution of two parameters"""
        if axes is None:
            axes = plt.axes()
        toplot = (self.arr.weighted('Teff')[1] < 100.) & (self.arr.weighted('Teff')[0] < 4500) & (self.arr.weighted('Teff')[0] > 3000) & sp.isfinite(self.rel_var[xparam]) & sp.isfinite(self.rel_var[yparam])
        if xparam == 'logg' or yparam == 'logg':
            toplot = toplot & (self.noise['logg'] < 0.1)
        if xparam == 'mag' or yparam == 'mag':
            toplot = toplot # & (self.arr['e_AJ'] < 1)
        if xparam == 'rsini' or yparam == 'rsini':
            #toplot_temp = toplot & (self.arr['em_vsini'] < 15.)
            #axes.scatter(self.rel_var[xparam][toplot_temp], self.rel_var[yparam][toplot_temp], s=5, c='black', marker='x')
            toplot = toplot & sp.isfinite(self.rel_var['rsini'])
        axes.scatter(self.rel_var[xparam][toplot], self.rel_var[yparam][toplot], s=5, edgecolor='none', label="star in IC 348")
        print 'Pearson(%s, %s) = %f (p-value = %e)' % ((xparam, yparam, ) + sp.stats.pearsonr(self.rel_var[xparam][toplot], self.rel_var[yparam][toplot]))
        print 'Spearman(%s, %s) = %f (p-value = %e)' % ((xparam, yparam, ) + sp.stats.spearmanr(self.rel_var[xparam][toplot], self.rel_var[yparam][toplot]))
        print 'Kendall(%s, %s) = %f (p-value = %e)' % ((xparam, yparam, ) + sp.stats.kendalltau(self.rel_var[xparam][toplot], self.rel_var[yparam][toplot]))

        lrad = sp.linspace(-1, 1, 1001)
        xcorr = -lrad * self.correction(xparam)
        ycorr = -lrad * self.correction(yparam)
        xprob = self.prob_func(xparam)(xcorr, sp.sqrt(sp.mean(self.noise[xparam][toplot] ** 2)), 1.)
        yprob = self.prob_func(yparam)(ycorr, sp.sqrt(sp.mean(self.noise[yparam][toplot] ** 2)), 1.)
        full_prob = xprob[:, None] * yprob[None, :]
        for rw, color in zip(rwidth, ['black', 'red', 'yellow']):
            shifted_prob = full_prob.copy()
            if rw != 0:
                for shift in sp.arange(1, lrad.size, 1):
                    pshift = sp.exp(-(lrad[shift] - lrad[0]) ** 2. / (2 * rw ** 2))
                    if pshift < 1e-10:
                        break
                    shifted_prob[shift:, shift:] += full_prob[:-shift, :-shift] * pshift
                    shifted_prob[:-shift, :-shift] += full_prob[shift:, shift:] * pshift
            sor_full_prob = sp.sort(shifted_prob, None)[::-1]
            mark = [sor_full_prob[sp.where(sp.cumsum(sor_full_prob, None) / sp.sum(shifted_prob, None) > pbord)[0][0]] for pbord in [0.68, 0.95]]
            contour = axes.contour(xcorr, ycorr, shifted_prob.T, mark, colors=color)
            # axes.clabel(contour, fmt = {mark[0]: "68%", mark[1]: "95%"}, fontsize="smaller", inline_spacing=0)
        axes.set_xlim(self.limits[xparam])
        axes.set_ylim(self.limits[yparam])
        if with_shift:
            start = {'logg': -0.6, 'rsini': 0.7, 'mag': 1}
            head_length={('rsini', 'logg'): 0.016,
                        ('rsini', 'mag'): 0.05,
                        ('logg', 'mag'): 0.07}
            axes.add_patch(patches.FancyArrow(start[xparam], start[yparam], self.shift100K[xparam], self.shift100K[yparam], width=0.002, length_includes_head=True, head_length=head_length[(xparam, yparam)]))
            #            axes.annotate("", xy=(start[xparam] + self.shift100K[xparam], start[yparam] + self.shift100K[yparam]), xycoords="data", xytext=(start[xparam], start[yparam]), textcoords="data", arrowprops={"arrowstyle": "->", "shrinkA": 0, "shrinkB": 0})
        return xcorr, ycorr, sor_full_prob

    def triangle_plot(self, rwidth=sp.log10([1, 1.15, 1.25, 1.35]), fig=None):
        """Plots the correlations between the three size measurements.
        """
        if fig is None:
            fig = plt.figure()
        label = {"mag": r"$\Delta$ extinction-corrected J (mag)",
                 "logg": r"$\Delta \log g$",
                 "rsini": r"$\Delta \log_{\rm 10} R \sin i$"}
        fig.subplots_adjust(wspace=0, hspace=0)
        for ix1, param1 in enumerate(self.rel_var.dtype.names):
            for ix2, param2 in enumerate(self.rel_var.dtype.names):
                if ix2 > ix1:
                    if ix1 == 1:
                        ax_scatter = fig.add_subplot(3, 3, ix2 * 3 + ix1 + 1, sharey=ax_scatter)
                    elif ix2 == 2:
                        ax_scatter = fig.add_subplot(3, 3, ix2 * 3 + ix1 + 1, sharex=ax_scatter)
                    else:
                        ax_scatter = fig.add_subplot(3, 3, ix2 * 3 + ix1 + 1)
                    self.surface_plot(param1, param2, rwidth[[0, 2]], axes=ax_scatter, with_shift=True)
                    if ix2 == 1:
                        plt.setp(ax_scatter.get_xticklabels(), visible=False)
                        ax_scatter.set_xlabel(" ")
                    else:
                        ax_scatter.set_xlabel(label[param1])
                        if param1 == 'rsini':
                            ax_scatter.set_xticks([-0.5, 0., 0.5, 1.0])
                        else:
                            ax_scatter.set_xticks([-1.0, -0.5, 0., 0.5, 1.0])
                            stars = lines.Line2D([0, 1], [0, 1], color='blue', marker='.', ls='none')
                            #contours = [lines.Line2D([0, 1], [0, 1], color='g', ls='-'), lines.Line2D([0, 1], [0, 1], color='c', ls='-')]
                            contours = [patches.Ellipse((0, 1), 1, 1, ec='black', fc='none'), patches.Ellipse((0, 1), 1, 1, ec='red', fc='none')]
                            ax_scatter.legend([stars] + contours, ['observed', 'model of uncertainties', 'model of uncertainties and radius spread of $25\%$'], title='Distributions in scatter plot', numpoints=1, bbox_to_anchor=(2., 2.43))
                    if ix1 == 1:
                        plt.setp(ax_scatter.get_yticklabels(), visible=False)
                        ax_scatter.set_ylabel(" ")
                    else:
                        ax_scatter.set_ylabel(label[param2])
                        if param2 == 'logg':
                            ax_scatter.set_ylim(-0.9, 0.7)
            if ix1 != 2:
                ax = fig.add_subplot(3, 3, ix1 * 4 + 1, sharex=ax_scatter)
            else:
                ax = fig.add_subplot(3, 3, ix1 * 4 + 1)
            self.single_histogram(param1, rwidth, ax)
            plt.setp(ax.get_yticklabels(), visible=False)
            if ix1 == 0:
                ax.set_ylabel(label[param1])
                leg_patches = [patches.Rectangle((0, 0), 1, 1, ec='b', fc='none', lw=2), lines.Line2D([0, 1], [0, 1], color='black', ls='-'), lines.Line2D([0, 1], [0, 1], color='orange', ls='-'), lines.Line2D([0, 1], [0, 1], color='red', ls='-'), lines.Line2D([0, 1], [0, 1], color='magenta', ls='-')]
                leg_labels = ['observed', 'model of uncertainties', 'model of uncertainties and radius spread of $15\%$', 'model of uncertainties and radius spread of $25\%$', 'model of uncertainties and radius spread of $35\%$']
                ax.legend(leg_patches, leg_labels, title='Distributions in histogram', numpoints=1, bbox_to_anchor=(3., 1.03))
            else:
                ax.set_ylabel(" ")
            if ix1 != 2:
                ax.set_xlabel(" ")
                plt.setp(ax.get_xticklabels(), visible=False)
            else:
                ax.set_xlabel(label[param1])
        return fig

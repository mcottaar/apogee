import scipy as sp
import os
try:
    from astropy.io import fits
except ImportError:
    import pyfits as fits


class TableWrite(object):
    def __init__(self, arr, no_velocity=False):
        self.arr = arr
        if no_velocity:
            self.lines = tuple([line for line in self.lines if not ('vrad' in line[0] or 'vcorr' in line[0])])
        self.table = [self.get_element(el) for el in zip(*self.lines)[0]]

    def as_ascii(self, filename):
        """save table as per epoch values in ASCII format as `filename`
        """
        element_size = '%18s '
        elem, title, form, fits_form = zip(*self.lines)

        with open(filename, 'w') as result:
            title_line = ((element_size * len(title)) % tuple(title)) + '\n'
            result.write(title_line)
            for table_line in zip(*tuple(self.table)):
                as_strings = [element_size % (format_elem % value) for format_elem, value in zip(form, table_line)]
                merged_string = reduce(lambda a, b: a + b, as_strings) + '\n'
                result.write(merged_string)

    def as_fits(self, filename):
        """save table as per epoch values in FITS format as `filename`
        """
        elem, title, ascii_form, form = zip(*self.lines)
        columns = [fits.Column(name=elem_title, format=elem_form, array=elem_table) for elem_title, elem_form, elem_table in zip(title, form, self.table)]
        tbhdu = fits.new_table(columns)
        prihdu = fits.PrimaryHDU()
        thdulist = fits.HDUList([prihdu, tbhdu])
        thdulist.writeto(filename)
        return thdulist


class PerEpochTable(TableWrite):
    lines = (('objid', '2MASS', '%s', '20A'),
             ('date', 'date', '%.3f', 'E'),
             ('h_SNR', 'S/N', '%.0f', 'E'),
             ('em_Teff', 'Teff', '%.1f', 'E'),
             ('esc_Teff', 'sig_Teff', '%.1f', 'E'),
             ('off_Teff', 'eta_Teff', '%.2f', 'E'),
             ('em_logg', 'log(g)', '%.4f', 'E'),
             ('esc_logg', 'sig_log(g)', '%.4f', 'E'),
             ('off_logg', 'eta_log(g)', '%.2f', 'E'),
             ('em_vsini', 'vsini', '%.4e', 'E'),
             ('esc_vsini', 'sig_vsini', '%.4e', 'E'),
             ('off_vsini', 'eta_vsini', '%.2f', 'E'),
             ('em_vrad', 'RV', '%.3f', 'E'),
             ('em_vcorr', 'corrected_RV', '%.3f', 'E'),
             ('esc_vrad', 'sig_RV', '%.3f', 'E'),
             ('off_vrad', 'eta_RV', '%.2f', 'E'),
             ('em_veiling', 'R_H', '%.4e', 'E'),
             ('esc_veiling', 'sig_R_H', '%.4e', 'E'),
             ('off_veiling', 'eta_R_H', '%.2f', 'E'))

    def get_element(self, elem):
        if elem[:4] == 'off_':
            return self.arr.per_epoch(self.arr.norm_offset(elem[4:], corrected=True))
        return self.arr.per_epoch(elem)

        
class PerStarTable(TableWrite):
    lines = (('objid', '2MASS', '%s', '20A'),
             ('h_RA', 'RA(deg)', '%.5f', 'E'),
             ('h_DEC', 'Dec(deg)', '%.5f', 'E'),
             ('cluster', 'Cluster', '%s', '15A'),
             ('nepochs', 'N(epochs)', '%i', 'I'),
             ('baseline', 'baseline(days)', '%.3f', 'E'),
             ('h_SNR', 'S/N', '%.0f', 'E'),
             ('em_Teff', 'Teff', '%.1f', 'E'),
             ('esc_Teff', 'sig_Teff', '%.1f', 'E'),
             ('off_Teff', 'P(cnst_Teff)', '%.2e', 'E'),
             ('em_logg', 'log(g)', '%.4f', 'E'),
             ('esc_logg', 'sig_log(g)', '%.4f', 'E'),
             ('off_logg', 'P(cnst_log(g))', '%.2e', 'E'),
             ('em_vsini', 'vsini', '%.4e', 'E'),
             ('esc_vsini', 'sig_vsini', '%.4e', 'E'),
             ('off_vsini', 'P(cnst_vsini)', '%.2f', 'E'),
             ('em_vrad', 'RV', '%.3f', 'E'),
             ('em_vcorr', 'corrected_RV', '%.3f', 'E'),
             ('esc_vrad', 'sig_RV', '%.3f', 'E'),
             ('off_vrad', 'P(cnst_RV)', '%.2e', 'E'),
             ('em_veiling', 'R_H', '%.4e', 'E'),
             ('esc_veiling', 'sig_R_H', '%.4e', 'E'),
             ('off_veiling', 'P(cnst_R_H)', '%.2e', 'E'),
             ('J', '2MASS_J', '%.2f', 'E'),
             ('H', '2MASS_H', '%.2f', 'E'),
             ('K', '2MASS_Ks', '%.2f', 'E'),
             ('e_EJ_H', 'E(J-H)', '%.2f', 'E'),
             ('e_relJ', 'extinction-corrected_J', '%.2f', 'E'),
             )

    cluster_translation = {'Orion': 'Orion A', 'Control': 'Field'}

    def get_element(self, elem):
        if elem[:4] == 'off_':
            return self.arr.prob_epoch_to_epoch(elem[4:], corrected=True)
        if elem[:3] == 'em_':
            return self.arr.weighted(elem[3:])[0]
        if elem[:4] == 'esc_':
            return self.arr.weighted(elem[4:])[1]
        if elem == 'cluster':
            return sp.array([self.cluster_translation.get(name, name) for name in self.arr['cluster']])
        if elem == 'baseline':
            return [max(dates) - min(dates) if isinstance(dates, list) else 0 for dates in self.arr['date']]
        if elem == 'h_SNR':
            return sp.array([sp.sqrt(sp.sum(sp.array(snr) ** 2)) if isinstance(snr, list) else snr for snr in self.arr['h_SNR']])
        if elem in ['h_RA', 'h_DEC']:
            return self.arr.get_median(elem)
        return self.arr[elem]


def write_tables(arr, directory='tables', no_velocity=False):
    """write per star and per epoch tables in given directory in ASCII and FITS format"""
    tab_epoch = PerEpochTable(arr, no_velocity=no_velocity)
    tab_star = PerStarTable(arr, no_velocity=no_velocity)
    for name in ['per_epoch.fit', 'per_star.fit']:
        if os.path.isfile(os.path.join(directory, name)):
            os.remove(os.path.join(directory, name))
    tab_epoch.as_ascii(os.path.join(directory, 'per_epoch.dat'))
    tab_epoch.as_fits(os.path.join(directory, 'per_epoch.fit'))
    tab_star.as_ascii(os.path.join(directory, 'per_star.dat'))
    tab_star.as_fits(os.path.join(directory, 'per_star.fit'))


import scipy as sp
import matplotlib.pyplot as plt
from apogee import observed_spectrum
from matplotlib.backends.backend_pdf import PdfPages


def extract_teff_outlier(arr, Tout=1000):
    """returns an array containing only those stars with outlying effective temperatures by more than `Tout` K.
    """
    offset = arr.per_epoch(arr.norm_offset('Teff')) * arr.per_epoch('es_Teff')
    bad_stars = sp.unique(arr.per_epoch('objid')[(offset > 1000) & (arr.per_epoch('h_SNR') > 10)])
    return arr[[sp.where(arr['objid'] == objid)[0] for objid in bad_stars]]
 

def plot_outlier(tofit, axes=None):
    """Plots the epoch with the highest and lowest effective temperatures
    """
    if axes is None: axes = plt.axes()
    ixplot = sp.argmin(tofit.parameters['Teff']), sp.argmax(tofit.parameters['Teff'])
    for toplot in ixplot:
        obs = tofit[toplot].observed()
        for obs_arr in obs:
            use = ~obs_arr['mask']
            axes.plot(obs_arr['wavelength'][use], obs_arr['flux'][use], '-')
    axes.set_xlabel('Wavelength')
    axes.set_ylabel('Flux')
    axes.set_title(tofit.meta('OBJID'))

def all_outliers(arr, to_file='temp.pdf', specdir='spectra'):
    """Plots all epochs with outlying velocities to a pdf file
    """
    specinfo = observed_spectrum.cluster_list(specdir)
    fig = plt.figure(figsize=(8, 7))
    with PdfPages(to_file) as pp:
        for star in extract_teff_outlier(arr):
            axes = fig.add_subplot(111)
            try:
                tofit = arr.spectrum(star['objid'], specinfo=specinfo)
            except ValueError:
                continue
            plot_outlier(tofit, axes)
            pp.savefig(fig)
            fig.clf()

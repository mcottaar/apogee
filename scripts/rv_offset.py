import scipy as sp
import scipy.ndimage
import scipy.interpolate


def spline_medianfilter(arr, nmed=30., nknot=4, k=3, axes=None):
    """Returns the corrected radial velocities using a spline and median filtering.

    Arguments:
    - `nmed`: number of RVs to take the median of in filter.
    - `k`: degree of polynomials in spline.
    """
    rv_list = []
    teff_list = []
    for cluster, color, marker in zip(['Pleiades', 'IC 348', 'NGC 1333'],
                                      ['c', 'b', 'r'],
                                      ['d', 's', 'o']):
        arrc = arr.get_cluster(cluster)
        if arrc.size == 0:
            continue
        teff_list.append(arrc.weighted('Teff')[0])
        rv_cluster = sp.median(arrc[(teff_list[-1] > 3500.) & (teff_list[-1] < 5000.)].weighted('vrad')[0])
        rv_list.append(arrc.weighted('vrad')[0] - rv_cluster)
        if axes is not None:
            axes.scatter(teff_list[-1], rv_list[-1], label=cluster, color=color, marker=marker, s=8)
    rv = reduce(sp.append, rv_list)
    teff = reduce(sp.append, teff_list)
    sor = sp.argsort(teff)
    rvmed = sp.ndimage.filters.median_filter(rv[sor], nmed)

    use = teff[sor] > 2500
    spline = sp.interpolate.LSQUnivariateSpline(teff[sor][use], rvmed[use], t=[3000., 3250., 3500., 4500.], k=k)
    rvspline = spline(arr.weighted('Teff')[0])
    rvspline[arr.weighted('Teff')[0] < 2700] = spline(2700.)

    if axes is not None:
        axes.errorbar(2700, 7, sp.median(arr.weighted('vrad')[1]), sp.median(arr.weighted('Teff')[1]), color='k', fmt=',')
        axes.plot(teff[sor], rvmed, 'g-', label='median filter')
        sor = sp.argsort(arr.weighted('Teff')[0])
        axes.plot(arr.weighted('Teff')[0][sor], rvspline[sor], 'm-', label='spline')
        axes.set_ylim(-10, 20)
        axes.set_xlabel('Effective temperature (K)')
        axes.set_ylabel('Radial velocity (km/s)')
    return rvspline


def add_rv_correction(arr):
    """Returns a record array with the corrected radial velocities added.
    """
    result = sp.zeros(arr.size, arr.dtype.descr + [('rv_spline', 'f8'), ('em_vcorr', arr['em_vrad'].dtype),
                                                   ('es_vcorr', arr['es_vrad'].dtype),
                                                   ('esc_vcorr', arr['esc_vrad'].dtype)])
    for name in arr.dtype.names:
        result[name] = arr[name]
    rvcorr = spline_medianfilter(arr)
    result['rv_spline'] = rvcorr
    rvcorr[arr.weighted('Teff')[0] > 4000.] = 0.
    if result['em_vrad'].dtype == sp.dtype('object'):
        for ixvel, vel in enumerate(result['em_vrad']):
            if isinstance(vel, list):
                result['em_vcorr'][ixvel] = [v - rvcorr[ixvel] for v in vel]
            else:
                result['em_vcorr'][ixvel] = vel - rvcorr[ixvel]
    else:
        result['em_vcorr'] = result['em_vrad'] - rvcorr
    result['es_vcorr'] = result['es_vrad']
    result['esc_vcorr'] = result['esc_vrad']
    return result.view(type(arr))

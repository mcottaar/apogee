import scipy as sp
import matplotlib.pyplot as plt
import utils
from apogee import read_data
from scipy import optimize


class MassComputer(object):
    def __init__(self, arr):
        self.arr = arr
        self.targets = read_data.target_list()
        self.targets = self.targets[(self.targets['HMAG'] > 0) & (self.distance() < 2.)]

    @property
    def arri(self):
        return self.arr.get_cluster('IC 348')

    def spec_mass(self, Tcorr=0):
        """Returns the spectroscopic mass for stars observed by APOGEE (in self.arri).
        """
        result = sed.OpticalPhotometricFit.iso_locus(self.arri.weighted('Teff')[0] + Tcorr, self.arri['e_absJ'], 'J')['mass']
        result[(result < 0.25)] = 0
        fin = sp.isfinite(result)
        sor = sp.argsort(self.arri[fin].weighted('Teff')[0])
        result[~fin] = sp.interpolate.interp1d(self.arri[fin].weighted('Teff')[0][sor], result[fin][sor], 'linear', bounds_error=False, fill_value=0.)(self.arri[~fin].weighted('Teff')[0])
        return result

    def mean_mass(self, nmean=20):
        """Returns the mean mass as a function of H-band magnitude"""
        mass = self.spec_mass()
        sor = sp.argsort(self.arri['H'])
        use = sp.isfinite(mass[sor])
        mean_mass = sp.ndimage.filters.uniform_filter1d(mass[sor][use], nmean)
        return sp.interpolate.interp1d(self.arri.get_index('H')[sor][use], mean_mass, 'nearest', bounds_error=False)

    def random_mass(self, nselect=20):
        """Selects random mass from observed stars.
        """
        mass = self.spec_mass()
        use = sp.isfinite(mass)
        sor = sp.argsort(self.arri['H'][use])
        ixclose = sp.argmin(abs(self.arri['H'][use][sor][:, None] - self.targets['HMAG'][None, :]), 0)
        ixdraw = ixclose + sp.random.random_integers(-nselect / 2, nselect / 2, ixclose.size * 100).reshape(100, ixclose.size)
        ixdraw[ixdraw < 0] = -ixdraw[ixdraw < 0]
        ixdraw[ixdraw > (sor.size - 1)] = sor.size - ixdraw[ixdraw > (sor.size - 1)]
        return mass[use][sor][ixdraw]

    def plot_mean_mass(self, nmean=20, mmax=0.25):
        plt.plot(self.arri.get_index('h_H'), self.spec_mass(), '.')
        #high_snr = self.arri.get_median('h_SNR') > 20
        #plt.plot(self.arri.get_index('h_H')[high_snr], self.spec_mass()[high_snr], '.')
        Hmag = sp.linspace(7, 15, 500)
        plt.plot(Hmag, self.mean_mass(nmean, mmax)(Hmag), '-')
        plt.axhline(mmax, color='k', linestyle='dashed')
        plt.xlabel('H-band magnitude')
        plt.ylabel('mass (solar masses)')

    def target_mass(self, ):
        """Returns the estimated mass of the target stars.
        """
        massfunc = self.mean_mass()
        mass = massfunc(self.targets['HMAG'])
        #mass[self.targets['HMAG'] < 7] = massfunc(7)
        mass[self.targets['HMAG'] > 14.7] = massfunc(14.7)
        #mass[self.targets['HMAG'] > 13.] = 0.
        return mass

    def final_mass(self, ):
        """Returns the mass of the target and observed stars.
        """
        tmass = sp.mean(self.random_mass(), 0)
        omass = self.spec_mass()
        ix_obs, ix_targ = self.counterparts()
        fmass = tmass.copy()
        fmass[ix_targ] = omass[ix_obs]
        return fmass

    def distance(self, center=(56.13, 32.145)):
        """Return the distance from the cluster center"""
        return utils.functions.distance(self.targets['RA'], self.targets['DEC'], center[0], center[1])

    @staticmethod
    def incompleteness(mmax=0.25):
        """Returns the fraction of the found mass missing due to the lower mass limit.
        """
        imf = utils.functions.MaschbergerIMF()
        mass_density = lambda mass: mass * imf.pdf(mass)
        normalization = sp.integrate.quad(mass_density, mmax, 300.)[0]
        return sp.integrate.quad(mass_density, 0., mmax)[0] / normalization

    def counterparts(self, ):
        """Identify the counterparts between the targets and observed stars.

        returns the indices for the observed stars and the targets to their mutual closest neighbours
        """
        distance = utils.functions.distance(self.arri.get_index('h_RA')[:, None], self.arri.get_index('h_DEC')[:, None], self.targets['RA'], self.targets['DEC'])
        direct1 = sp.argmin(distance, 0)
        direct2 = sp.argmin(distance, -1)
        mutual = direct1[direct2] == sp.arange(direct2.size)
        return direct1[direct2[mutual]], direct2[mutual]

    def IC348_mass(self, outer=2.):
        """Computes the total mass of IC 348 within given radius.
        """
        use = self.distance() < outer
        return sp.sum(self.final_mass()[use]) * (1. + self.incompleteness())

    @classmethod
    def p_EFF_cdf(cls, radius, params, distmax=None):
        """Computes the CDF of the EFF
        """
        cumulative = 2 * sp.pi * params[0] ** 2 / (params[1] - 2.) * (1 - (1. + radius ** 2. / params[0] ** 2) ** (1. - params[1] / 2.))
        if distmax is None:
            return cumulative
        else:
            normalization = cls.p_EFF_cdf(distmax, params)
            return cumulative / normalization

    @staticmethod
    def EFF_rhm(params):
        """computes the half-mass radius from the parameters.
        """
        return params[0] * sp.sqrt(0.5 ** (1./ (1. - params[1] / 2)) - 1)
            
    @classmethod
    def p_EFF_profile(cls, params, distance, distmax=20):
        """Returns probability of having star at given distance for
        P(R) proportional to (1 + (R / params[0]) ** 2) ** (- params[1] / 2)
        """
        normalization = cls.p_EFF_cdf(distmax, params)
        probability = 2 * sp.pi * distance * (1 + (distance / params[0]) ** 2) ** (- params[1] / 2.) / normalization
        return probability

    def logl_EFF(self, distmax=20, center=(56.13, 32.145)):
        """Returns a log-likelihood of the EFF profile to a distribution up to a maximum radius of `distmax` in arcmin
        """
        dist = utils.functions.distance(self.arri.get_index('h_RA'), self.arri.get_index('h_DEC'), center[0], center[1]) * 60.
        #dist = (self.distance() * 60.)
        use = (dist < distmax) & (self.arri['e_relJ'] < 12.5)
        print '%i stars fitted' % sp.sum(use)
        return lambda params: sp.sum(sp.log(self.p_EFF_profile(params, dist[use], distmax)))

    def EFF_params(self, distmax=20):
        """Returns the best-fit EFF parameters.

        return tuple with (radius, gamma)
        """
        logl = self.logl_EFF(distmax)
        tofit = lambda params: -logl(params)
        return optimize.fmin(tofit, (7, 3))

    def mass_profile(self, distmax=20):
        """plots the mass profile of IC 348.
        """
        distance = sp.linspace(0, 2., 500)
        targ_dist = self.distance()
        corr_mass = self.final_mass() * (1. + self.incompleteness())
        imass = sp.cumsum(corr_mass[sp.argsort(targ_dist)])
        #imass = sp.vectorize(lambda dist: sp.sum(corr_mass[targ_dist < dist]))(distance)
        params = self.EFF_params(distmax)
        cdf_eff = self.p_EFF_cdf(distance * 60., params, distmax)
        plt.plot(sp.sort(targ_dist) * 60., imass, 'c-')
        plt.plot(distance * 60., cdf_eff * 200, 'y-')
        plt.xlabel('distance from cluster center (arcmin)')
        plt.ylabel('enclosed stellar mass (M$_\odot$)')
        plt.xlim(0, 40)
        plt.ylim(0, 250)
        return sp.sort(targ_dist) * 60., imass

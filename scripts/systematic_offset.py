import scipy as sp
import pyfits
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
from apogee import observed_spectrum

def basic_offset(arr):
    """Computes the offset distribution in every spectrum as a function of pixel.
    """
    # tofit = arr.spectrum(arr['objid'][0])
    spec_shape = (3, 4096) # sp.array(tofit[0].observed()).shape
    result = sp.zeros((arr.size, ) + spec_shape)
    specinfo = observed_spectrum.cluster_list(arr.specdir, 10)
    use = sp.ones(arr.size, dtype='bool')
    for ixobj, objid in enumerate(arr['objid']):
        try:
            tofit = arr.spectrum(objid, specinfo=specinfo)
        except ValueError:
            use[ixobj] = False
            continue
        spectrum = sp.array(tofit.compute_fit(renormalize=True))
        weight = spectrum['error'] ** (-2.)
        weight[spectrum['mask']] = 0.
        mean_ratio = sp.sum(spectrum['flux'] / spectrum['theoretical'] * weight, 0) / sp.sum(weight, 0)
        result[ixobj, ...] = mean_ratio
    return result[use, :, :]

def plot_offset(arr, nmed=1, fig=None, fitsdir='.', vmin=0.9, vmax=1.1, cluster='all'):
    """Plots the offset in the three bands."""
    offset = sp.load(os.path.join(fitsdir, 'offset.npy'))
    sor = sp.argsort(arr['em_Teff'])
    offset = offset[sor, ...]
    arr = arr[sor]
    if cluster != 'all':
        use = arr['cluster'] == cluster
        arr = arr[use]
        offset = offset[use, ...]
    if nmed == 1:
        toplot = offset
    else:
        toplot = sp.ndimage.filters.median_filter(offset, (nmed, 1, 1))

    wavelength = pyfits.getdata(os.path.join(fitsdir, 'spectra', 'apVisit-5534-55847-002.fits'), 4)
    Tval = reduce(sp.append, [2500, sp.arange(3100, 4550, 200), 5000, 5300, 5600, 6000, 7000])
    Txval = sp.interp(Tval, sp.sort(arr['em_Teff']), sp.arange(arr.size))

    if fig is None:
        fig = plt.figure(figsize=(9, 3))
        fig.subplots_adjust(left=0.12, bottom=0.19, top=0.93)
    grid = mpl.gridspec.GridSpec(1, 13)
    for ixax in range(3):
        axes = fig.add_subplot(grid[ixax * 4:(ixax + 1) * 4])
        mappable = axes.imshow(toplot[:, ixax, :], vmin=vmin, vmax=vmax, aspect='auto')

        wvmin = sp.around(min(wavelength[ixax, :]) + 50, -2)
        wvmax = sp.around(max(wavelength[ixax, :]) - 50, -2)
        wvval = sp.arange(wvmin, wvmax + 10, 100)
        wxval = sp.interp(wvval, wavelength[ixax, :][::-1], sp.arange(wavelength[ixax, :].size)[::-1])
        axes.set_xticks(wxval)
        axes.set_yticks(Txval)
        axes.set_xlabel('wavelength(micron)')
        axes.set_xticklabels(wvval / 1e4)
        if ixax == 0:
            axes.set_yticklabels(Tval)
            axes.set_ylabel('Effective temperature (K)')
        else:
            axes.set_yticklabels([""] * len(Tval))
    axes = fig.add_subplot(grid[-1])
    cbar = fig.colorbar(mappable, cax=axes)
    cbar.set_label('observed / model')
    cbar.set_ticks([0.9, 0.95, 1, 1.05, 1.1])
    return fig

def plot_spectra(Teff, arr, grid=None, offset=None, axes=None, nmed=50, fitsdir='.'):
    """Plot spectrum at given temperatures, median filtering over `nmed` closest stars at that temperature."""
    wavelength = pyfits.getdata(os.path.join(fitsdir, 'spectra', 'apVisit-5534-55847-002.fits'), 4).T
    if offset is not None:
        offset = sp.load(os.path.join(fitsdir, 'offset.npy'))
    use = sp.argsort(abs(arr['em_Teff'] - Teff)[:nmed])
    sin_offset = sp.median(offset[use, ...], 0)
    model_spec = grid.get_spectrum(Teff=Teff, logg=sp.median(arr[use]['em_logg'])).interpolate(wavelength)
    if axes is None:
        axes = plt.axes()
    axes.plot(wavelength, model_spec['flux'])
    axes.plot(wavelength, model_spec['flux'] * sin_offset.T)
    return axes

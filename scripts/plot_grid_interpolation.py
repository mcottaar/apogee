"""A script to produce a plot which shows the grid interpolation.

$ python plot_grid_interpolation.py path/to/coelho <Teff> <logg>

The plot is best looked at interactively.
"""
import matplotlib.pyplot as plt
from apogee import model_spectra
import scipy as sp


if __name__ == '__main__':
    import sys
    griddir = sys.argv[1]
    Teff = float(sys.argv[2])
    logg = float(sys.argv[3])
    grid = model_spectra.coelho(griddir).at_value('FeH', 0.).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.)).convert_spectra(resolution=20000, new_wavelength=15000 * sp.exp(sp.arange(1e4) / 70000.))
    fig, axes = plt.subplots(5, 1, sharex=True, sharey=True)
    
    
    def plot(ax, Teff, logg):
        spec = grid.get_spectrum(Teff=Teff, logg=logg)
        ax.plot(spec.wavelength, spec.flux, label='Teff = %i; log(g) = %.2f' % (Teff, logg))
    
    Tlow = max([Tgrid for Tgrid in grid.variable('Teff') if Tgrid < Teff])
    Thigh = min([Tgrid for Tgrid in grid.variable('Teff') if Tgrid > Teff])
    glow = max([ggrid for ggrid in grid.variable('logg') if ggrid < logg])
    ghigh = min([ggrid for ggrid in grid.variable('logg') if ggrid > logg])
    plot(axes[0], Teff, logg)
    plot(axes[0], Tlow, glow)
    plot(axes[0], Tlow, ghigh)
    plot(axes[0], Thigh, glow)
    plot(axes[0], Thigh, ghigh)
    plot(axes[1], Teff, logg)
    plot(axes[1], Teff, glow)
    plot(axes[1], Teff, ghigh)
    plot(axes[2], Teff, logg)
    plot(axes[2], Tlow, logg)
    plot(axes[2], Thigh, logg)
    plot(axes[3], Tlow, logg)
    plot(axes[3], Tlow, glow)
    plot(axes[3], Tlow, ghigh)
    plot(axes[4], Teff, glow)
    plot(axes[4], Tlow, glow)
    plot(axes[4], Thigh, glow)
    for ax in axes:
        ax.legend(loc='lower center', ncol=5)
        ax.set_ylabel('flux')
    fig.show()
    axes[-1].set_xlabel('wavelength')

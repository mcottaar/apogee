"""Script that reads in and stores an array containing a summary of all the result of the spectral fits.

Call using
$ python summarize_pickles.py path/to/pickles
All pickles are reading in and an array storing the information is created in the same directory as the pickles under "per_star.npy".
"""
import scipy as sp
import cPickle as pickle
import glob
import os
import pyfits
import literature
import sed
import rv_offset
from apogee import spectral_parameters


def read_pickles(directory='.'):
    """Returns a list with all the dictionaries stored in the pickle files.

    Adds the filename of the pickle file to the dictionary, before returning it.
    """
    def real_uncertainty(emcee_data, SNR, red_chisq):
        if not(sp.isfinite(SNR)): SNR = 0
        corr_red_chisq = red_chisq - (0.02 * SNR) ** 2
        if corr_red_chisq < 1.3: corr_red_chisq = 1.3
        sig_mcmc = sp.std(emcee_data, None)
        corr_sig = 3 * sig_mcmc * sp.sqrt(corr_red_chisq)
        return corr_sig

    pickle_list = []
    for filename in glob.glob(os.path.join(directory, '*.pickle')):
        with open(filename) as open_file:
            pickle_dict = pickle.load(open_file)
        pickle_dict['pickle_filename'] = filename
        pickle_dict['pickle_fullfilename'] = os.path.abspath(filename)
        pickle_dict['min_chisq_emcee'] = sp.amin(pickle_dict['emcee']['chisq'])
        pickle_dict['parameters']['chisq'] = pickle_dict['chisq']
        emcee_full = pickle_dict['emcee'][:, -100:].flatten().view(('f8', len(pickle_dict['emcee'].dtype.names)))
        pickle_dict['covariance'] = sp.cov(emcee_full[:, :-1].T)
        pickle_dict['covariance_labels'] = sp.array(pickle_dict['emcee'].dtype.names[:-1])
        pickle_dict['emcee'] = dict([(name, (sp.median(pickle_dict['emcee'][name], None), sp.std(pickle_dict['emcee'][:, -100:][name], None), real_uncertainty(pickle_dict['emcee'][:, -100:][name], sp.median(pickle_dict['meta']['SNR']), pickle_dict['min_chisq_emcee'] / pickle_dict['ndata']), sp.std(pickle_dict['emcee'][:, -50:][name], None) / sp.std(pickle_dict['emcee'][:, -100:-50][name], None))) for name in pickle_dict['emcee'].dtype.names])
        pickle_list.append(pickle_dict)
    return pickle_list

def cut_epochs(pickle_list, min_SNR=0, min_Teff=0):
    """Cut all epochs with too low S/N ratio or effective temperature.
    """
    keep = [pickle_dict for pickle_dict in pickle_list if (not (pickle_dict['meta']['SNR'] < min_SNR)) and (pickle_dict['emcee']['Teff'][0] >= min_Teff)]
    return keep

def merge_pickles(pickle_list):
    objid_long = sp.array([pick['objid'] for pick in pickle_list])
    all_objid = sp.unique(objid_long)
    def get_objid(objid):
        use = objid_long == objid
        indices = sp.where(objid_long == objid)[0]
        if sp.sum(use) == 1:
            return pickle_list[indices[0]]
        def merge_dict(dictionaries):
            result = {}
            all_keys = []
            [all_keys.extend(dic.keys()) for dic in dictionaries]
            all_keys = sp.unique(all_keys)
            for key in all_keys:
                values = [dic.get(key, sp.nan) for dic in dictionaries]
                if isinstance(values[0], dict):
                    result[key] = merge_dict(values)
                else:
                    try:
                        if sp.unique(values).size == 1:
                            result[key] = values[0]
                        else:
                            result[key] = values
                    except TypeError:
                        result[key] = values
            return result
        return merge_dict([pickle_list[index] for index in indices])
    return map(get_objid, all_objid)

def get_dtype(as_list):
    """Gets the dtype of the elements in the list.

    Helper function needed to convert to an array.
    """
    type_list = [sp.dtype(type(element)) for element in as_list]
    if any([single_type == 'object' for single_type in type_list]):
        return sp.dtype('object')
    if type_list[0] == sp.dtype(str):
        return sp.dtype((str, max([len(element) for element in as_list])))
    return type_list[0]


def convert_to_array(pickle_list):
    """Returns a list of spectral fits read in by read_pickles into an array.

    Values from the spectral fits will start with an 'p_'.
    The results from emcee are stored in 'es_' (standard deviation) and 'em_' (median value).
    Values from the headers of the original fits files will start with an 'h_'.
    Values from the ASPCAP files will start with 'a_' (from the ASPCAP header file with 'ah_').
    Values from the AllStar file start with 's_'.
    Values from the AllVisit file start with 'v_'.
    Values from the literature start with 'l_XYY_', where X is the first letter of the first autor's name and YY are the last two digits of the year.
    """
    result_dict = {}
    result_dict['objid'] = [item['objid'] for item in pickle_list]
    result_dict['date'] = [item['jd-mid'] for item in pickle_list]
    #result_dict['date'] = [item['hjd'] for item in pickle_list]
    result_dict['nepochs'] = [(len(item) if isinstance(item, list) else 1) for item in result_dict['date']]
    result_dict['fit_chisq'] = [item['chisq'] for item in pickle_list]
    result_dict['min_chisq'] = [item['min_chisq_emcee'] for item in pickle_list]
    for label in ['covariance', 'covariance_labels', 'ndata', 'npoly']:
        result_dict[label] = [item[label] for item in pickle_list]

    defaults_dict = {}
    for element in pickle_list:
        meta = element['meta']
        for key in meta:
            if key not in defaults_dict:
                defaults_dict[key] = meta[key]
    for key in defaults_dict:
        if isinstance(defaults_dict[key], basestring):
            default = ''
        elif isinstance(defaults_dict[key], int):
            default = -9999
        else:
            default = sp.nan
        result_dict['h_' + key] = [item['meta'][key] if key in item['meta'].keys() else default for item in pickle_list]

    ra = sp.array([sp.median(coord) for coord in result_dict['h_RA']])
    dec = sp.array([sp.median(coord) for coord in result_dict['h_DEC']])
    if 'h_TARG2' in result_dict.keys():
        targ2 = sp.array([[ixtarg for ixtarg in targ if sp.isfinite(ixtarg)][0] if isinstance(targ, list) else targ for targ in result_dict['h_TARG2']])
    else:
        targ2 = sp.zeros(ra.size)
    starg = sp.array([[ixtarg for ixtarg in targ if sp.isfinite(ixtarg)][0] if isinstance(targ, list) else targ for targ in result_dict['h_SECTARG']])
    in_cluster = ((targ2.astype('int') & 2 ** 13) != 0) | ((starg.astype('int') & 2 ** 13) != 0)
    pleiades = ((targ2.astype('int') & 2 ** 10) != 0) | ((starg.astype('int') & 2 ** 10) != 0)
    subgroup = {'NGC 1333': (ra < 54) & (dec > 29.) & in_cluster,
        'IC 348': (ra > 54) & (ra < 60) & (dec > 29.) & in_cluster,
        'Control': ~in_cluster & ~pleiades,
        'NGC 2264': (ra > 95) & (dec < 13.) & in_cluster,
        'Pleiades': (ra < 60) & (dec < 29.) & pleiades,
        'Orion': (dec < 0.) & in_cluster}
    cluster = sp.zeros(ra.size, dtype='S20')
    for name, use in subgroup.items():
        cluster[use] = name
    #cluster[:] = 'Orion'
    result_dict['cluster'] = list(cluster)
    result_dict['ra'] = ra
    result_dict['dec'] = dec
    for band in ['J', 'H', 'K']:
        if 'h_%s' % band in result_dict.keys():
            mag1 = [element[0] if isinstance(element, list) else element for element in result_dict['h_%s' % band]]
            mag2 = [element[0] if isinstance(element, list) else element for element in result_dict['h_%sMAG' % band]]
            result_dict[band] = [m1 if (sp.isfinite(m1) and m1 < 99) else m2 for m1, m2 in zip(mag1, mag2)]
        else:
            result_dict[band] = result_dict['h_%sMAG' % band]

    for param_name in pickle_list[0]['parameters'].keys() + ['chisq']:
        result_dict['p_' + param_name] = [item['parameters'][param_name] for item in pickle_list]
        std_list = []
        median_list = []
        ratio_list = []
        stdc_list = []
        for item in pickle_list:
            def helper_get_param(param_name, index):
                """Recursive function to return varying values of param_name.
                """
                emcee_dict = item['emcee']
                if param_name not in emcee_dict.keys():
                    return sp.nan
                elif isinstance(emcee_dict[param_name], list):
                    return [epoch[index] for epoch in emcee_dict[param_name]]
                else:
                    return emcee_dict[param_name][index]
            if helper_get_param(param_name, 0) is sp.nan:
                break
            median_list.append(helper_get_param(param_name, 0))
            std_list.append(helper_get_param(param_name, 1))
            stdc_list.append(helper_get_param(param_name, 2))
            ratio_list.append(helper_get_param(param_name, 3))
        if len(median_list) != len(pickle_list):
            continue
        result_dict['es_' + param_name] = std_list
        result_dict['esc_' + param_name] = stdc_list
        result_dict['em_' + param_name] = median_list
        result_dict['ec_' + param_name] = ratio_list

    aspcap_results = [res for res in [get_aspcap(obj) for obj in result_dict['objid']]]
    all_keys = []
    [all_keys.extend(res.keys()) for res in aspcap_results if res is not None]
    for key in all_keys:
        dt = get_dtype([res[key] for res in aspcap_results if (res is not None and key in res.keys())])
        result_dict[key] = [res.get(key, sp.zeros(1, dt)[0]) if res is not None else sp.zeros(1, dt)[0] for res in aspcap_results]

    field_names = result_dict.keys()
    field_names.sort()
    field_types = [get_dtype(result_dict[name]) for name in field_names]
    print 'stitching it together'
    result = sp.zeros(len(pickle_list), dtype=zip(field_names, field_types))
    for name in result.dtype.names:
        result[name] = result_dict[name]
    return result


def get_aspcap(objid):
    """get the ASPCAP results for given star"""
    try:
        filename = glob.glob(os.path.join(os.path.split(__file__)[0], 'ASPCAP', '*', 'aspcapStar-v*-%s.fits' % objid))[0]
    except IndexError:
        print 'No ASPCAP file found for %s' % objid
        return
    hrd = pyfits.open(filename, memmap=False)
    hrd.verify('fix')
    result = {}
    for name, value in hrd[0].header.items():
        result['ah_%s' % name] = value
    for name in hrd[4].data.dtype.names:
        result['a_%s' % name] = hrd[4].data[name]
    del hrd[4].data
    for marker in ['', 'F']:
        for ixvar, var in enumerate(['Teff', 'logg', 'vmicro', 'FeH', 'CFe', 'NFe', 'alphaFe']):
            result['a_%s%s' % (marker, var)] = result['a_%sPARAM' % marker][0, ixvar]
            result['a_e%s%s' % (marker, var)] = sp.sqrt(result['a_%sPARAM_COV' % marker][0, ixvar, ixvar])
    hrd.close()
    return result


def add_allfiles(arr):
    """Adds the values in allVisit and allStar files to the array.
    """
    star = glob.glob(os.path.join(os.path.split(__file__)[0], 'ASPCAP', 'allStar-v.fits'))[0]
    visit = glob.glob(os.path.join(os.path.split(__file__)[0], 'ASPCAP', 'allVisit-v.fits'))[0]
    dtype = arr.dtype.descr + [('s_' + dt[0], ) + dt[1:] for dt in star.dtype.descr]+ [('v_' + name, 'object') for name in visit.dtype.names]
    result = sp.zeros(arr.size, dtype=dtype)
    for name in arr.dtype.names:
        result[name] = arr[name]
    for ixobjid, objid in enumerate(arr['objid']):
        if (star['APOGEE_ID'] == objid).any():
            for name in star.dtype.names:
                assert sp.sum(star['APOGEE_ID'] == objid) == 1
                result['s_' + name][ixobjid] = star[star['APOGEE_ID'] == objid][0][name]
        else:
            print '%s not found in AllStar' % objid
        nvisit = len(arr[ixobjid]['h_JD-MID']) if isinstance(arr[ixobjid]['h_JD-MID'], list) else 1
        use = visit['APOGEE_ID'] == objid
        if use.any():
            if sp.sum(use) != nvisit:
                print 'for star %s: found %i observations in allVisit, but %i epochs have been fitted' % (objid, sp.sum(use), nvisit)
            elif not all([date in visit[use]['JD'] for date in sp.atleast_1d(arr[ixobjid]['h_JD-MID'])]):
                print 'for star %s: epochs in Visit file do not match with fitted epochs' % objid
            else:
                if nvisit == 1:
                    for name in visit.dtype.names:
                        result['v_' + name][ixobjid] = visit[use][0][name]
                else:
                    indices = [list(visit[use]['JD']).index(date) for date in arr[ixobjid]['h_JD-MID']]
                    for name in visit.dtype.names:
                        result['v_' + name][ixobjid] = list(visit[use][indices][name])
        else:
            print '%s not found in AllVisit' % objid
    return result

def to_text(input, output, include=[('objid', 'identifier'), ('h_RA', 'RA(dec)'), ('h_DEC', 'DEC(deg)'), ('nepochs', 'N_epochs'), ('h_SNR', 'SNR'), ('chisq', 'chisq'), ('em_Teff', 'Teff(K)'), ('es_Teff', 'eTeff(K)'), ('em_logg', 'log(g)'),
                                    ('es_logg', 'elog(g)'), ('em_vrad', 'vrad(km/s)'), ('es_vrad', 'evrad(km/s)'), ('em_vsini', 'vsini(km/s)'), ('es_vsini', 'vsini(km/s)'), ('h_JMAG', 'J'), ('h_HMAG', 'H'), ('h_KMAG', 'Ks')]):
    """Store the data array from `input` into `output` as a text file.

    `include` is a list with pair of strings, where the first element indicates the variable to include and the second string contains the title of the column.
    """
    arr = spectral_parameters.load_parameters(input, per_epoch=True)
    format = '%18s ' * len(include) + '\n'
    lines = [format % tuple([item[1] for item in include])]
    for epoch in arr:
        lines.append(format % tuple([epoch[item[0]] for item in include]))
    with open(output, 'w') as fil:
        fil.writelines(lines)

def add_photometry(array):
    """Add the extinction, relative J-band magnitude, and absolute J-band magnitude to the array.
    """
    result = sp.zeros(array.size, array.dtype.descr + [(name, 'f8') for name in ['e_EJ_H', 'e_AV', 'e_AJ', 'e_relJ', 'e_absJ']]).view(type(array))
    for name in array.dtype.names:
        result[name] = array[name]
    arr_use = array.view(spectral_parameters.AllParameters)
    locus = sed.pleiades_locus(arr_use).observed_locus
    loc_use = sp.sort(locus[sp.isfinite(locus['Teff'])], order='Teff')
    spline = sp.interpolate.InterpolatedUnivariateSpline(loc_use['Teff'], loc_use['J'] - loc_use['H'], k=1)
    J_H_intr = spline(arr_use.weighted('Teff')[0])
    #J_H_intr = sp.interp(arr_use.weighted('Teff')[0], loc_use['Teff'], loc_use['J'] - loc_use['H'], sp.nan, sp.nan)
    result['e_EJ_H'] = arr_use.get_median('J') - arr_use.get_median('H') - J_H_intr + 0.044 * 0.33
    result['e_AV'] = result['e_EJ_H'] * 7.91
    result['e_AJ'] = result['e_EJ_H'] * 2.72
    result['e_relJ'] = arr_use.get_median('J') - result['e_AJ']
    result['e_absJ'] = sp.nan
    dm = {'IC 348': 6.98,
          'NGC 1333': 6.98,
          'Pleiades': 5.63}
    for cluster, distance in dm.items():
        result['e_absJ'][result['cluster'] == cluster] = result['e_relJ'][result['cluster'] == cluster] - distance
    return result


if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        directory = sys.argv[1]
    else:
        directory = '.'
    print 'reading pickles'
    pickle_list = read_pickles(directory)
    if True: # set to True if every epoch is fitted individually, set to False if if every star is fitted individually
        cut_list = cut_epochs(pickle_list, min_SNR=0, min_Teff=0)
        print 'merging single epochs'
        star_list = merge_pickles(cut_list)
    else:
        star_list = pickle_list
    print 'converting to array'
    arr = convert_to_array(star_list)
    print 'adding literature values'
    arr_with_lit = literature.get_data(arr)
    print 'adding extinction + RV correction'
    arr_with_ext = add_photometry(arr_with_lit)
    arr_with_vcorr = rv_offset.add_rv_correction(arr_with_ext.view(spectral_parameters.AllParameters))
    print 'storing array'
    sp.save(os.path.join(directory, 'per_star.npy'), arr_with_vcorr.view(sp.recarray))

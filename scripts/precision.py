#! /usr/bin/env python
"""Script to create and fit fake spectra"""
from APOGEE_shared import fit_script, fake_spectrum, model_spectra
from optparse import OptionParser
import scipy as sp


if __name__ == '__main__':
    parser = OptionParser(usage="Usage: %prog [options] number_of_fits", description='Creates a mock spectrum given the provided parameters and fits it `number_of_fits` times.')
    parser.add_option("-T", "--Teff", dest="Teff", help='Effective temperature of fake spectrum in K', type='float', default=4000.)
    parser.add_option("-g", "--logg", dest="logg", help='Surface gravity of fake spectrum in log(cgs)', type='float', default=4.5)
    parser.add_option("--SNR", dest="snr", help='Signal to noise ratio of fake spectrum', type='float', default=10)
    parser.add_option("-r", "--vsini", dest="vsini", help='Rotational velocity of fake spectrum in km/s', type='float', default=30.)
    parser.add_option("-v", "--veiling", dest="veiling", help='Veiling (background featureless flux)', type='float', default=0.)
    parser.add_option("-m", "--MCMC", dest="mcmc", help='Run MCMC for every dataset', action="store_true", default=False)
    parser.add_option("-a", "--global", dest="glob", help='Run a global fit (i.e. differential evolution from openopt)', action="store_true", default=False)
    parser.add_option("--nolocal", dest="local", help='Do NOT run a local fit (i.e. l_bfgs_b from scipy)', action="store_false", default=True)
    parser.add_option("-e", "--nepochs", dest='nepochs', help='Number of epochs to include in the fit (only Rv will vary between epochs)', type='int', default=1)
    parser.add_option("--file", dest="file", help='Filename to store the result, default is a filename composed of the selected parameters')

    (options, args) = parser.parse_args()
    nrun = int(args[0])
    if options.file is not None:
        filename = options.file
    else:
        addition = ''
        if options.local:
            addition += '_local'
        if options.mcmc:
            addition += '_mcmc'
        if options.glob:
            addition += '_global'
        filename = "T%i_g%.1f_snr%i_rot%i_veil%.2f%s.npz" % (options.Teff, options.logg, options.snr, options.vsini, options.veiling, addition)
    grid = model_spectra.coelho(fit_script.griddir).at_value('FeH', 0.).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.)).convert_spectra(resolution=22500, new_wavelength=15000 * sp.exp(sp.arange(1e4) / 70000.))
    kwargs = dict([(key, getattr(options, key)) for key in ('Teff', 'logg', 'snr', 'vsini', 'veiling', 'nepochs')])
    fake_spec = fake_spectrum.FakeData.from_grid(grid, **kwargs)
    result = fake_spec.multifit(nrun, options.local, options.glob, options.mcmc)
    result.update(kwargs) 
    sp.savez(filename, **result)
    
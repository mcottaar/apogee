import scipy as sp
import matplotlib.pyplot as plt
import os
import pyfits

all_bands = ['g', 'r', 'i', 'Z', 'J', 'H', 'Ks']

def pleiades_benchmark():
    """Load in the emperical Pleiades benchmark from Bell et al. (2012)
    """
    return sp.genfromtxt(os.path.join(os.path.split(__file__)[0], 'literature', 'Bell12.dat'), names=True)

def dartmouth_tracks(directory=os.path.join(os.path.split(__file__)[0], 'literature', 'Dartmouth')):
    """Loads in the Dartmouth tracks from given directory
    """
    filenames = glob.glob(os.path.join(directory, 'm*.jc2mass'))
    result_list = []
    for filename in filenames:
        #arr = sp.genfromtxt(filename, names=['age', 'logTeff', 'logg', 'logL', 'u', 'g', 'r', 'i', 'z'])
        arr = sp.genfromtxt(filename, names=['age', 'logTeff', 'logg', 'logL', 'U', 'B', 'V', 'R', 'I', 'J', 'H', 'Ks'])
        with open(filename) as fil:
            line = fil.readline()
        mass = re.search('# M/Mo=([0-9.]*)', line).groups()[0]
        result = sp.zeros(arr.shape, arr.dtype.descr + [('mass', 'f8'), ('Teff', 'f8'), ('log_age_yr', 'f8')])
        for name in arr.dtype.names:
            result[name] = arr[name]
        result['mass'] = mass
        result['Teff'] = 10 ** result['logTeff']
        result['log_age_yr'] = sp.log10(result['age'])
        result_list.append(result)
    return reduce(sp.append, result_list)

def IC348_phot(arr):
    """Returns photometry from IC 348

    Parameters:
    - `arr`: record array containing photometry.
    """
    arrc = arr[arr['l_B13']]
    result = sp.zeros(arrc.size, dtype=[(name, 'f8') for name in (all_bands + ['e%s' % band for band in all_bands])])
    for band in all_bands:
        for long_band in ['l_B13_%s_MAG', 's_%s']:
            if long_band % band[:1] in arrc.dtype.names:
                result[band] = arrc[long_band % band[:1]]
        for long_band in ['l_B13_%s_UNCERT', 's_%s_ERR']:
            if long_band % band[:1] in arrc.dtype.names:
                result['e' + band] = arrc[long_band % band[:1]]
        discard = (result['e' + band] == 0) | (result[band] == 0)
        result[band][discard] = sp.nan
        result['e' + band][discard] = sp.nan
    return result



def synthetic_isochrones():
    """Loads in the synthetic isochrones.

    Contains AB magnitudes from the INT-WFC filters and Vega magnitudes from Johnsons/Cousins/2MASS.
    """
    filename = os.path.join(os.path.split(__file__)[0], 'literature', 'intwfc_tada', 'intwfc_siess_btsettl_%s.dat')
    arr = sp.genfromtxt(filename % 'vega', skip_header=18, comments='#', names=True)
    arr_ab = sp.genfromtxt(filename % 'ab', skip_header=18, comments='#', names=True)
    for band in ['u', 'g', 'r', 'i', 'z']:
        full_band = 'intwfc_%s' % band
        arr[full_band] = arr_ab[full_band]
    result = sp.zeros(arr.size, arr.dtype.descr + [(name, 'f8') for name in ['Teff', 'age_Myr', 'U', 'g', 'r', 'i', 'Z', 'J', 'H', 'Ks']])
    for name in arr.dtype.names:
        result[name] = arr[name]
    for band in ['U', 'g', 'r', 'i', 'Z']:
        result[band] = result['intwfc_%s' % band.lower()]
    for band in ['J', 'H', 'Ks']:
        result[band] = result['2mass_%s' % band.lower()]
    result['Teff'] = 10 ** result['log_Teff']
    result['age_Myr'] = 10 ** result['log_age_yr'] / 1e6
    return result


class LocusPhotometry(object):
    """Generates griZJHK synthetic photometry locus corrected using the relative photometry
    """
    def __init__(self, locus, age=130, dm=5.63):
        """Parameters:
        - `locus`: structured array with locus photometry
        - `age`: cluster age in Myr (default 100 Myr)
        - `dm`: distance modulus.
        """
        self.observed_locus = locus
        self.age = age
        self.dm = dm
        self.all_iso = synthetic_isochrones()

    def synthetic(self, age=None, dm=None):
        """Returns a synthetic locus in griZJHK

        By default the age and dm of the observed locus are used.
        """
        if age is None:
            age = self.age
        log_age = sp.log10(age) + 6.
        if dm is None:
            dm = self.dm
        upper_age = sp.amin(self.all_iso['log_age_yr'][self.all_iso['log_age_yr'] >= log_age])
        lower_age = sp.amax(self.all_iso['log_age_yr'][self.all_iso['log_age_yr'] <= log_age])
        if upper_age == lower_age:
            result = self.all_iso[self.all_iso['log_age_yr'] == log_age]
        else:
            arr_upper = self.all_iso[self.all_iso['log_age_yr'] == upper_age]
            arr_lower = self.all_iso[self.all_iso['log_age_yr'] == lower_age]
            mass = sp.intersect1d(arr_upper['mass_Msun'], arr_lower['mass_Msun'])
            result = sp.zeros(mass.size, arr_upper.dtype)
            sub_upper = arr_upper[[sp.where(arr_upper['mass_Msun'] == m)[0][0] for m in mass]]
            sub_lower = arr_upper[[sp.where(arr_lower['mass_Msun'] == m)[0][0] for m in mass]]
            for name in result.dtype.names:
                result[name] = (sub_upper[name] * (log_age - lower_age) +
                                sub_lower[name] * (upper_age - log_age)) / (upper_age - lower_age)
        for band in all_bands:
            result[band] = result[band] + dm
        return result

    def correction(self, keep_constant='Ks'):
        """Calculates the photometric correction based on the observed and synthetic loci.

        Returns the observed - synthetic loci.

        Parameter:
        - `keep_constant`: variable used to map the synthetic to observed photometry.
        """
        sort_synth = sp.sort(self.synthetic(), order=[keep_constant])
        sort_obs = sp.sort(self.observed_locus, order=[keep_constant])

        param = sp.sort(sp.append(sort_synth[keep_constant], self.observed_locus[keep_constant]))
        arr = sp.zeros(param.size, dtype=[(name, 'f8') for name in all_bands + ['Teff', 'synth_Teff', 'constant']])
        for band in all_bands + ['Teff']:
            observed_int = sp.interp(param, sort_obs[keep_constant],
                                     sort_obs[band], sp.nan, sp.nan)
            synth_int = sp.interp(param, sort_synth[keep_constant],
                                  sort_synth[band], sp.nan, sp.nan)
            use = sp.isfinite(observed_int) & sp.isfinite(synth_int)
            arr[band] = observed_int - synth_int
        arr['synth_Teff'] = sp.interp(param, sort_synth[keep_constant],
                                sort_synth['Teff'], sp.nan, sp.nan)
        arr['constant'] = param
        return arr

    def plot_correction(self, keep_constant='Ks', axes=None):
        """Plots the correction in all bands.
        """
        if axes is None:
            axes = plt.axes()
        corr = self.correction()
        plot_bands = list(all_bands)
        plot_bands.remove(keep_constant)
        for band in plot_bands:
            axes.plot(corr['constant'], corr[band], label='%s %s' % ('INT-WFC' if band in 'griZ' else '2MASS', band))
        axes.set_xlabel(keep_constant)
        axes.set_ylabel('Offset in band X (mag)')
        return axes

    def semi_emperical(self, age, dm, keep_constant='Ks'):
        """Returns semi-emperical loci from synthetic photometry calibrated to the observed locus.
        """
        synth = sp.sort(self.synthetic(age, dm), order=['Teff'])
        corr = sp.sort(self.correction(keep_constant), order=['synth_Teff'])
        res = sp.zeros(synth.shape, synth.dtype.descr)
        for name in synth.dtype.names:
            res[name] = synth[name]
        for band in all_bands + ['Teff']:
            int_corr = sp.interp(synth['Teff'], corr['synth_Teff'], corr[band], sp.nan, sp.nan)
            res[band] = synth[band] + int_corr
        return res


class OpticalPhotometricFit(object):
    """Fits the optical + NIR photometry in IC 348
    """
    Rv = 3.1
    def __init__(self, arr, age=7., dm=7.38):
        self.pleiades_locus = pleiades_locus(arr)
        self.locus = self.pleiades_locus.semi_emperical(age, dm)
        self.age = age
        self.dm = dm
        self.phot = IC348_phot(arr)
        self.extinction_arr = sp.load(os.path.join(os.path.split(__file__)[0], 'extinction_law.npy'))
        self._single_extinction = dict([(band.split('_')[-1], self.extinction_arr[2][band])
                                         for band in self.extinction_arr.dtype.names])
        self.load_monte_carlo()
        self.other_parameters = arr[arr['l_B13']]

    def __getitem__(self, item):
        return self.phot[item]

    fitted_extinction = {'Ks': 0.21863298831778336, 'r': 1.8610120338931748, 'g': 2.3353909957421073, 'i': 1.2622547442057366, 'H': 0.32322568898200738, 'Z': 0.86101203389317482, 'J': 0.48741970510681398}

    def extinction(self, Av=1, Rv=None):
        """Returns structured array with the extinction vector for given Av.
        """
        if Rv is None:
            Rv = self.Rv
        Av = sp.asarray(Av)
        extinction = dict([(band.split('_')[-1], sp.interp(Rv, self.extinction_arr['Rv'], self.extinction_arr[band]))
                                         for band in self.extinction_arr.dtype.names])
        res = sp.zeros(Av.shape, dtype=[(band, 'f8') for band in all_bands])
        for band in all_bands:
            res[band] = Av * extinction[band.lower()]
        return res

    def fit_Exi_iZ(self, fig=None):
        """Fits the extinction law given the excess extinction by fitting all stars with Teff > 3300 K and E(i - Z) < 0.7

        Returns a dictionary with for every band X: E(X - i) / E(i - Z)
        """
        if fig is None:
            fig = plt.figure()
        excess = self.excess(color=True, corr_ext=False)
        comp_color = ('i', 'H')
        comparison = excess[comp_color[0]] - excess[comp_color[1]]
        toplot = (comparison < 7.)
        colors = [('g', 'i'), ('r', 'i'), ('i', 'Z'), ('J', 'H'), ('H', 'Ks')]
        for ixband, color in zip(range(len(colors)), colors):
            axes = fig.add_subplot(3, 2, ixband + 1)
            axes.scatter(comparison[toplot], (excess[color[0]] - excess[color[1]])[toplot],
                c=self.other_parameters[toplot].weighted('Teff')[0], edgecolor='none', vmin=3000., vmax=4500, s=10)
            xval = [-0.5, 6.5]
            Alam_bands = ['g', 'r', 'i', 'Z', 'J', 'H', 'Ks']
            Alam3 = sp.array([ 1.151857, 0.85258065,  0.62706452,  0.49967742,  0.29141935, 0.18290323, 0.11393548]) # RV = 3.1
            Alam4 = sp.array([1.12127,  0.87   ,  0.66575,  0.547  ,  0.319  ,  0.20025,  0.12475]) # RV=4
            Alam5 = sp.array([ 1.09253, 0.88636364,  0.70209091,  0.59145455,  0.34490909, 0.21654545, 0.13490909]) # RV=5.5
            axes.set_xlim(*xval)
            for ext, mark, label in []: #[(Alam3, 'k-', 3.1), (Alam4, 'k--', 4.), (Alam5, 'k:', 5.5)]:
                index_band = [Alam_bands.index(band) for band in color]
                comp_index = [Alam_bands.index(band) for band in comp_color]
                axes.plot(xval, sp.array(xval) * (ext[index_band[0]] - ext[index_band[1]]) / (ext[comp_index[0]] - ext[comp_index[1]]), mark, label= '$R_{\mathrm{V}} = %.f$' % label, zorder=0)
            for mark, label in [('r-', 3.1), ('r--', 4.), ('r:', 5.5)]:
                ext = self.extinction(1., label)
                slope = (ext[color[0]] - ext[color[1]]) * sp.array(xval) / (ext[comp_color[0]] - ext[comp_color[1]])
                axes.plot(xval, slope, mark, label='$R_{\mathrm{V}} = %.1f$' % label, zorder=0)
            #axes.set_ylim(*slope)
            #axes.plot(xval, sp.polyval(fit, xval), 'k-')
            axes.set_ylabel('E(%s - %s)' % color)
            if color[1] not in 'Ks':
                for tick in axes.get_xaxis().get_ticklabels():
                    tick.set_visible(False)
            else:
                axes.set_xlabel('E (%s - %s)' % comp_color)
        axes = fig.add_subplot(3, 2, 6)
        axes.scatter(comparison[toplot], self.excess(False, False)['i'][toplot],
                c=self.other_parameters[toplot].weighted('Teff')[0], edgecolor='none', vmin=3000., vmax=4500, s=10)
        for ext, mark, label in []: #[(Alam3, 'k-', 3.1), (Alam4, 'k--', 4.), (Alam5, 'k:', 5.5)]:
            comp_index = [Alam_bands.index(band) for band in comp_color]
            axes.plot(xval, ext[2] * sp.array(xval) / (ext[comp_index[0]] - ext[comp_index[1]]), mark, label='$R_{\mathrm{V}} = %.1f$' % label, zorder=0)
        for mark, label in [('r-', 3.1)]: #, ('r--', 4.), ('r:', 5.5)]:
            ext = self.extinction(1., label)
            axes.plot(xval, ext['i'] * sp.array(xval) / (ext[comp_color[0]] - ext[comp_color[1]]), mark, label='$R_{\mathrm{V}} = %.1f$' % label, zorder=0)
        axes.set_xlabel('E(%s - %s)' % comp_color)
        axes.set_ylabel('A$_{\mathrm{i}}$')
        axes.set_xlim(*xval)
        #axes.set_ylim(0, 12)

    def color_ext_ind(self, photometry=None, color1=('r', 'i'), color2=('i', 'Z'), uncertainty=False):
        """returns the extinction-independent color.

        By default the color of the IC348 optical photometry is returned
        """
        if photometry is None:
            photometry = self.phot
            #ext = self.fit_Exi_iZ()
        ext = self.extinction(1., 3.1)
        Eri_Eiz = (ext[color1[0]] - ext[color1[1]]) / (ext[color2[0]] - ext[color2[1]])
        return (photometry[color1[0]] - photometry[color1[1]]) - Eri_Eiz * (photometry[color2[0]] - photometry[color2[1]])

    def phot_Teff(self, ):
        """Returns the photometric parameters of the extinction-independent colors from riZ of the observed stars

        Arguments:
        - `parameter`: name of the parameter to be returned, one
        """
        loc_Q = self.color_ext_ind(self.locus)
        use = sp.isfinite(loc_Q)
        return sp.interp(self.color_ext_ind(), loc_Q[use][::-1],
                         self.locus['Teff'][use][::-1])

    def bolometric_correction(self, ):
        """Returns the bolometric correction in the J-band computed from the spectroscopic effective temperature (Pecaut & Mamajek, 2012)"""
        return 2.920272 - 3.220428e-4 * self.other_parameters.weighted('Teff')[0]

    def intrinsic_sed(self, Teff=None):
        """Returns the intrinsic SEDs of the observed stars based on their photometric temperature.
        """
        if Teff is None:
            Teff = self.other_parameters.weighted('Teff')[0]
        if Teff == 'photometric':
            Teff = self.phot_Teff()
        res = sp.zeros(self.phot.size, dtype=[(band, 'f8') for band in all_bands])
        for band in all_bands:
            res[band] = sp.interp(Teff - 1e-5, self.locus['Teff'], self.locus[band], sp.nan, sp.nan)
        return res

    def Av(self, color=('J', 'H')):
        """Returns the estimated extinction using ri photometry.
        """
        offset = self.excess(False, False)
        ext = self.extinction()
        return (offset[color[0]] - offset[color[1]]) / (ext[color[0]] - ext[color[1]])

    def extinction_corrected(self, ):
        """Returns the extinction-corrected colors.
        """
        res = sp.zeros(self.phot.size, dtype=[(band, 'f8') for band in all_bands])
        for band in all_bands:
            res[band] = self.phot[band] - self.extinction()[band] * self.Av()
        return res

    def excess(self, color=True, corr_ext=True):
        """Returns the excess magnitude in all bands.

        Arguments:
        - `color`: If True returns the color excess in X-i rather than the photometric offset in X.
        - `corr_ext`: If True correct extinction before computing the excess.
        """
        excess = sp.zeros(self.phot.size, dtype=[(band, 'f8') for band in all_bands])
        intr = self.intrinsic_sed()
        if corr_ext:
            ext_corr = self.extinction_corrected()
        else:
            ext_corr = self.phot
        for band in all_bands:
            excess[band] = ext_corr[band] - intr[band]
        if color:
            i_excess = excess['i'].copy()
            for band in all_bands:
                excess[band] = excess[band] - i_excess
        return excess

    def luminosity(self, ):
        """Returns the extinction-corrected log(L/Lsun) from the stars.

        Using Mbol_sun = 4.755 from Mamajek (2012)
        """
        return 10. ** ((self.extinction_corrected()['J'] + self.bolometric_correction() - self.dm - 4.755) / -2.5)

    @classmethod
    def iso_locus(cls, Teff, secondary, sec_name='J'):
        """Returns the locus of the star on the isochrone.
        """
        all_iso = dartmouth_tracks()
        after_first_interp = sp.zeros((secondary.size, sp.unique(all_iso['mass']).size), dtype=all_iso.dtype)
        for ixmass, mass in enumerate(sp.unique(all_iso['mass'])):
            use = (mass == all_iso['mass']) & (all_iso['age'] < 1e9)
            sor = sp.argsort(all_iso[sec_name][use])
            for name in all_iso.dtype.names:
                after_first_interp[:, ixmass][name] = sp.interp(secondary, all_iso[sec_name][use][sor], all_iso[use][sor][name], sp.nan, sp.nan)
        locus = sp.zeros(Teff.size, dtype=after_first_interp.dtype)
        for ixTeff, Tfit in enumerate(Teff):
            sor = sp.argsort(after_first_interp[ixTeff, :]['Teff'])
            to_interp = after_first_interp[ixTeff, :][sor]
            for name in all_iso.dtype.names:
                locus[name][ixTeff] = sp.interp(Tfit, to_interp['Teff'], to_interp[name], sp.nan, sp.nan)
        return locus

    def mass(self, ):
        """Returns the stellar mass of every star in solar masses
        """
        return self.iso_locus()['mass']

    def age(self, ):
        """Returns the age of every star in yr.
        """
        return self.iso_locus()['age']

    def get_photometry(self, col_id, source='observed', uncertainty=False):
        """Returns the photometry.
        """
        if source == 'corrected':
            phot = self.extinction_corrected()
        elif source == 'observed':
            phot = self.phot
        elif source == 'isochrone':
            phot = self.locus
        if isinstance(col_id, tuple):
            if uncertainty:
                return sp.sqrt(phot['e' + col_id[0]] ** 2 + phot['e' + col_id[1]] ** 2)
            return phot[col_id[0]]- phot[col_id[1]]
        else:
            if uncertainty:
                return phot['e' + col_id]
            return phot[col_id]

    def get_label(self, col_id):
        """Helper function to return labels for axis for given color.
        """
        if isinstance(col_id, tuple):
            return '%s - %s' % (self.get_label(col_id[0])[:-6], self.get_label(col_id[1]))
        if col_id in 'UgriZ':
            scaffold = 'INT-WFC'
        elif col_id in 'JHKs':
            scaffold = '2MASS'
        return '%s %s (mag)' % (scaffold, col_id)

    def plot_diagram(self, idx, idy, plot_obs=True, plot_corr=True, isochrone=True, errorbars=False,
                     only_good=True, axes=None):
        """Plots a CMD of CCD

        Arguments:
        - `idx`: name of color (tuple of two strings) or magnitude (string) on x-axis.
        - `idy`: name of color (tuple of two strings) or magnitude (string) on y-axis.
        - `plot_obs`: if True plots the observed colors/magnitudes.
        - `plot_intr`: if True plots the extinction-corrected colors/magnitudes.
        - `isochrone`: if True plots the isochrone.
        - `only_good`: only plot the extinction-corrected colors for thosse stars that fall onto the isochrone
        - `axes`: matplotlib Axes object.
        """
        if axes is None:
            axes = plt.axes()
        on_iso = abs(self.excess()['Z']) < 1e-2
        for source, toplot, color in [('observed', plot_obs, 'b.'), ('isochrone', isochrone, 'k-'),
                                      ('corrected', plot_corr, 'r.')]:
            if toplot:
                xval = self.get_photometry(idx, source)
                yval = self.get_photometry(idy, source)
                if source == 'corrected' and only_good:
                    xval = xval[on_iso]
                    yval = yval[on_iso]
                if errorbars and source == 'observed':
                    xerr = self.get_photometry(idx, source, True)
                    yerr = self.get_photometry(idy, source, True)
                    axes.errorbar(xval, yval, yerr, xerr, fmt=color)
                else:
                    axes.plot(xval, yval, color)
        if plot_obs and plot_corr:
            xval = sp.vstack((self.get_photometry(idx), self.get_photometry(idx, 'corrected')))
            yval = sp.vstack((self.get_photometry(idy), self.get_photometry(idy, 'corrected')))
            if only_good:
                xval = xval[:, on_iso]
                yval = yval[:, on_iso]
            axes.plot(xval, yval, 'g:')
        axes.set_xlabel(self.get_label(idx))
        axes.set_ylabel(self.get_label(idy))
        if not isinstance(idx, tuple):
            xl = axes.get_xlim()
            axes.set_xlim(xl[1], xl[0])
        if not isinstance(idy, tuple):
            yl = axes.get_ylim()
            axes.set_ylim(yl[1], yl[0])

    def all_ccd_cmd(self, zoomed=False):
        """Creates a plot with all interesting CCDs and CMDs.
        """
        fig, axes = plt.subplots(3, 2, figsize=(12, 12))
        for idx, idy, ax, ylim in zip([('r', 'i')] * 6, [('r', 'Z'), 'i', ('g', 'r'), ('i', 'J'), ('i', 'H'), ('i', 'Ks')],
                                    axes.flatten(), [(0, 3), (17, 10), (0, 2), (1, 2.5), (1, 3), (1, 4)]):
            self.plot_diagram(idx, idy, axes=ax, errorbars=False)
            if zoomed:
                ax.set_ylim(ylim)
                ax.set_xlim(0, 2.5)

    def monte_carlo(self, nrun=100):
        """Returns the best-fit temperature, luminosity, mass, age, excess for the values and a Monte Carlo simulation of additionalvalues.
        """
        from_isochrone = ['mass', 'age', 'logg']
        result = sp.zeros((1 + nrun, self.phot.size), dtype=[(name, 'f8') for name in
            (all_bands + from_isochrone + ['luminosity', 'Teff', 'Q', 'Av'] + ['excess_%s' % band for band in all_bands])])

        def assign_to_result(res):
            excess = self.excess(False, True)
            for band in all_bands:
                res[band] = self.phot[band]
                res['excess_' + band] = excess[band]
            iso = self.iso_locus()
            for name in from_isochrone:
                res[name] = iso[name]
            res['luminosity'] = self.luminosity()
            res['phot_Teff'] = self.phot_Teff()
            res['Q'] = self.color_ext_ind()
            res['Av'] = self.Av()

        assign_to_result(result[0, :])
        phot_old = self.phot.copy()
        for ixrun in xrange(nrun):
            print ixrun
            try:
                for band in all_bands:
                    if band != 'H':
                        self.phot[band] = self.phot[band] + self.phot['e' + band] * sp.randn(self.phot.size)
                assign_to_result(result[ixrun + 1, :])
            finally:
                self.phot = phot_old.copy()
        self.result = result
        return result

    def save_monte_carlo(self, ):
        sp.save(os.path.join(os.path.split(__file__)[0], 'photometric_fit_MC.npy'), self.result)

    def load_monte_carlo(self, ):
        filename = os.path.join(os.path.split(__file__)[0], 'photometric_fit_MC.npy')
        if os.path.isfile(filename):
            self.result = sp.load(filename)

    def spline_fit(self, ):
        """Fits a spline to the observed extinction-corrected color loci.
        """
        result = {}
        teff = self.other_parameters.weighted('Teff')[0]
        ext_corr = self.extinction_corrected()
        fig, axes = plt.subplots(3, 2, figsize=(10, 15))
        for ax, color in zip(axes.flatten(), [('g', 'i'), ('r', 'i'), ('i', 'Z'), ('i', 'H'), ('J', 'H'), ('H', 'Ks')]):
            val_color = ext_corr[color[0]] - ext_corr[color[1]]
            offset = val_color - sp.median(val_color)
            ax.plot(teff, val_color, '.')
            if color == ('i', 'H'):
                result[color] = lambda Teff: self.intrinsic_sed(Teff)['i'] - self.intrinsic_sed(Teff)['H']
                continue
            if color == ('H', 'Ks'):
                max_offset = 2
            else:
                max_offset = 2
            use = sp.isfinite(offset) & (teff > 2800.)
            for ix in range(5):
                use = (abs(offset) < (sp.std(offset[use]) * max_offset)) & (teff > 2800.)
                sor = sp.argsort(teff[use])
                intp = sp.interpolate.LSQUnivariateSpline(teff[use][sor], val_color[use][sor], sp.linspace(3200, 4500, 3), k=3)
                offset = val_color - intp(teff)
                tnew = sp.linspace(2500, 6000, 100)
            ax.plot(tnew, intp(tnew))
            ax.plot(teff[use], val_color[use], '.')
            ax.set_xlabel('Effective temperature (K)')
            ax.set_ylabel('(%s - %s)$_0$' % color)
            result[color] = intp
        return result

def IC348_sequence(phot, age=7., dm=6.38, fig=None):
    if fig is None:
        fig = plt.figure()
    ext_corr = phot.extinction_corrected()
    semi_locus = phot.pleiades_locus.semi_emperical(age, dm)
    synth_locus = phot.pleiades_locus.synthetic(age, dm)
    obs_locus = phot.pleiades_locus.observed_locus
    kevin_locus = kevin_GAIAESO_sequence(logg=4.5, FeH=0.)
    orion_locus = nicola_Orion_sequence()
    colors = [('g', 'i'), ('r', 'i'), ('i', 'Z'), ('i', 'H'), ('J', 'H'), ('H', 'Ks')]
    ylim = [(-1.2, 3.2), (-0.2, 2.5), (-0.7, 1.2), (1.2, 3.3), (-0.25, 1.15), (-0.15, 0.85)]
    for ixcolor, yl, color in zip(range(len(colors)), ylim, colors):
        axes = fig.add_subplot(3, 2, ixcolor + 1)
        for toplot, fmt in [(ext_corr, '.'), (semi_locus, 'r-'), (synth_locus, 'r:'),
                            (obs_locus, 'r--'), (kevin_locus, 'k-')]:
            if fmt == '.':
                Teff = phot.other_parameters.weighted('Teff')[0]
            else:
                Teff = toplot['Teff']
            axes.plot(Teff, toplot[color[0]] - toplot[color[1]], fmt)
        nicola_color = '%s%s_0' % (color[0].lower(), color[1].lower())
        if nicola_color in orion_locus.dtype.names:
            axes.plot(orion_locus['Teff'], orion_locus[nicola_color], 'k--')
        if ixcolor < 4:
            for tick in axes.get_xaxis().get_ticklabels():
                tick.set_visible(False)
        else:
            axes.set_xlabel('Effective temperature (K)')
        axes.set_ylabel('%s - %s' % color)
        axes.set_xlim(2600, 6000)
        axes.set_ylim(yl)

def hrdiagram(phot, axes=None):
    """Plots the HR-diagram for IC 348 for stars with optical photometry.
    """
    if axes is None:
        axes = plt.axes()
    iso = dartmouth_tracks()
    axes.plot(phot.other_parameters.weighted('Teff')[0], phot.luminosity(), '.', label='IC 348 members')
    for age, label in zip([1e5, 1e6, 2e6, 5e6, 1e7, 1e9], ['0.1 Myr', '1 Myr', '2 Myr', '5 Myr', '10 Myr', '1 Gyr']):
        Teff = []
        lum = []
        for mass in sp.unique(iso['mass']):
            use = iso['mass'] == mass
            isosor = sp.sort(iso[use], order='age')
            Teff.append(sp.interp(age, isosor['age'], isosor['Teff']))
            lum.append(10 ** sp.interp(age, isosor['age'], isosor['log_L']))
        axes.plot(Teff, lum, label='age = %s' % label)
    axes.set_yscale('log')
    axes.set_xlim(2500, 7000)
    axes.set_ylabel('Luminosity (L$_{\odot}$)')
    axes.set_ylim(1e-2, 1e2)
    axes.legend(loc='lower right')
    axes.set_xlabel('Effective temperature (K)')


def get_pleiades(arr):
    """Get the Pleiades stars with optical photometry
    """
    return arr[arr['l_B12']]


def kevin_GAIAESO_sequence(Teff=None, logg=None, FeH=None):
    """Returns the SDSS synthetic photometric sequence sent by Kevin Covey based on the ESO-GAIA grid
    """
    translation = dict([(band, 'SDSS_%s' % band.upper()) for band in 'griZ'] +
                       [(band, 'TWO_%s' % band[:1]) for band in ['J', 'H', 'Ks']])
    arr = pyfits.getdata(u'/Users/mcottaar/Work/data/apogee/photometry/Phoenix_grid.fits')
    new_dtype = arr.dtype.descr + [(name, 'f8') for name in translation.keys() + ['Teff', 'logg', 'FeH']]
    result = sp.zeros(arr.size, new_dtype)
    for name in arr.dtype.names:
        result[name] = arr[name]
    for band, long_band in translation.items():
        result[band] = arr[long_band]
    for name in ['Teff', 'logg', 'FeH']:
        result[name] = arr[name.upper()]
    for constraint in ['Teff', 'logg', 'FeH']:
        if locals()[constraint] is not None:
            result = result[result[constraint] == locals()[constraint]]
    return result

def nicola_Orion_sequence(filename): # os.path.join(utils.config.datdir, 'photometry', 'Nicola', 'ri0_and_iz0_vs_teff_in_orion.txt')):
    """Returns the SDSS observed sequence in Orion.
    """
    return sp.genfromtxt(filename, names=True)

def distance_to_single(arr):
    """Calculates the photometric distance to the single-star locus.
    """
    pleiades = get_pleiades(arr)
    sequence = pleiades_benchmark()
    phot = sp.array([sp.interp(pleiades.photometry(band), sequence[band], sequence['r']) for band in all_bands[:-3]])
    sigphot = (sp.array([sp.interp(pleiades.photometry(band) + pleiades.photometry(band, uncertainty=True), sequence[band], sequence['r']) for band in all_bands[:-3]]) - phot)
    weight = 1. /sigphot ** 2.
    fin_phot = 1. / sp.sqrt(sp.nansum(weight, 0))
    mean_phot = sp.nansum(phot * weight, 0) / sp.nansum(weight, 0)
    chisq = sp.nansum(((phot - mean_phot[None, :]) / sigphot) ** 2., 0) / (sp.sum(sp.isfinite(phot), 0) - 1)
    return sp.mean(phot, 0), sp.std(phot, 0), chisq


def pleiades_locus(arr):
    """Computes the Pleiades locus with effective temperatures.
    """
    Teff, sTeff = get_pleiades(arr).weighted('Teff')
    poslocus, siglocus, chisq = distance_to_single(arr)
    locus = pleiades_benchmark()
    use = sp.isfinite(poslocus)
    straight_line = sp.polyfit(poslocus[use], sp.log10(Teff[use]), 1, w=1./siglocus[use]**2.)
    print straight_line
    res = sp.zeros(locus.shape, dtype=locus.dtype.descr + [('Teff', 'f8')])
    res['Teff'] = 10. ** sp.polyval(straight_line, locus['r'])
    for name in locus.dtype.names:
        res[name] = locus[name]
    return LocusPhotometry(res)

def corrected_locus(arr, fig=None):
    """Computes a recalibrated Pleaides locus.
    """
    arri = arr[arr['l_B13']].get_cluster('IC 348')
    full_loc = pleiades_locus(arr).observed_locus
    loc = full_loc[sp.isfinite(full_loc['Teff'])][::-1]
    phot = OpticalPhotometricFit(arr)
    Jexc = arri['e_relJ'] - sp.interp(arri.weighted('Teff')[0], loc['Teff'], loc['J'])
    EJ_H = arri['J'] - arri.get_median('H') - sp.interp(arri.weighted('Teff')[0], loc['Teff'], loc['J'] - loc['H'])

    if fig is not None:
        ax_before = fig.add_subplot(121)
        ax_after = fig.add_subplot(122)
        fig.subplots_adjust(left=0.15, bottom=0.14, right=0.94, top=0.95, wspace=0.40, hspace=0.32)
    optical = {}
    ext = phot.extinction(Rv=5.5)
    for color, bands in zip(['m', 'b', 'g', 'r'], ['g', 'r', 'i', 'Z']):
        extinction = ext[bands] / (ext['J'] - ext['H']) * EJ_H
        corrected = arri['l_B13_%s_MAG' % bands] - extinction
        pleiades_magn = sp.interp(arri.weighted('Teff')[0], loc['Teff'], loc[bands])
        slope = (ext[bands] - ext['J']) / (ext['J'] - ext['H'])
        offs_col = (loc[bands] - loc['J']) - slope * (loc['J'] - loc['H'])
        derivative = sp.interp(arri.weighted('Teff')[0], loc['Teff'][5:-5], (offs_col[10:] - offs_col[:-10]) / (loc['Teff'][10:] - loc['Teff'][:-10]))
        excess = corrected - pleiades_magn
        optical.update({bands:(excess - Jexc) / derivative})
        if fig is not None:
            ax_before.scatter(arri.weighted('Teff')[0], (excess - Jexc), s=3, edgecolor='None', color=color)
    arri = arr[arr['l_B13']].get_cluster('IC 348')
    full_loc = pleiades_locus(arr).observed_locus
    loc = full_loc[sp.isfinite(full_loc['Teff'])][::-1]
    phot = OpticalPhotometricFit(arr)
    Jexc = arri['e_relJ'] - sp.interp(arri.weighted('Teff')[0], loc['Teff'], loc['J'])
    EJ_H = arri['J'] - arri.get_median('H') - sp.interp(arri.weighted('Teff')[0], loc['Teff'], loc['J'] - loc['H'])
    use = arri['e_AJ'] < 1.

    Tfull = reduce(sp.append, [arri.weighted('Teff')[0]] * 4)
    sor = sp.argsort(Tfull)
    offset = reduce(sp.append, optical.values())
    med_off = sp.ndimage.filters.median_filter(offset[sor], 50)
    spline = sp.interpolate.LSQUnivariateSpline(Tfull[sor], med_off, t=[2500, 3500, 4500., 5500], k=3)
    off_loc = spline(loc['Teff'])
    off_loc[loc['Teff'] < 2500] = spline(2500)
    off2_loc = sp.interp(loc['Teff'], loc['Teff'] + off_loc, off_loc)
    loc['Teff'] = loc['Teff'] - off2_loc
    if fig is not None:
        for color, bands in zip(['m', 'b', 'g', 'r'], ['g', 'r', 'i', 'Z']):
            extinction = ext[bands] / (ext['J'] - ext['H']) * EJ_H
            corrected = arri['l_B13_%s_MAG' % bands] - extinction
            pleiades_magn = sp.interp(arri.weighted('Teff')[0], loc['Teff'], loc[bands])
            slope = (ext[bands] - ext['J']) / (ext['J'] - ext['H'])
            offs_col = (loc[bands] - loc['J']) - slope * (loc['J'] - loc['H'])
            derivative = sp.interp(arri.weighted('Teff')[0], loc['Teff'][5:-5], (offs_col[10:] - offs_col[:-10]) / (loc['Teff'][10:] - loc['Teff'][:-10]))
            excess = corrected - pleiades_magn
            optical.update({bands:(excess - Jexc) / derivative})
            ax_after.scatter(arri.weighted('Teff')[0], (excess - Jexc), s=3, edgecolor='None', color=color)
    return loc



def plot_Teff_locus(arr, axes=None):
    """Plots the calibration of the spectroscopic effective temperatures to the locus.
    """
    if axes is None:
        axes = plt.axes()
    locm, locs = distance_to_single(arr)
    sol = pleiades_locus(arr)
    arr2 = get_pleiades(arr)
    axes.errorbar(arr2.weighted('Teff')[0], locm, locs, arr2.weighted('Teff') * 2, fmt='.')
    axes.plot(sol['Teff'], sol['r'], '-')
    axes.set_xlim(2500, 6000)
    axes.set_ylim(19, 10)
    axes.set_ylabel('closest position on locus (g magnitude)')
    axes.set_xlabel('spectroscopic T$_{\mathrm{eff}}$ (K)')

def plot_distances(arr, axes=None):
    """plot the distribution of photometric disctances from the Pleiades single-star locus.
    """
    if axes is None:
        axes = plt.axes()
    axes.hist(sp.log10(distance_to_single(arr)['distance']), bins=20)
    axes.set_xlabel('photometric distance (mag)')
    axes.set_ylabel('N')
    return axes


def pleiades_single_stars(arr):
    """Select the seemingly single stars in the Pleiades.
    """
    use = distance_to_single(arr)['distance'] < 1.
    return distance_to_single(arr)[use], get_pleiades(arr)[use]


def plot_sequence(arr, as_Teff='Teff', axes=None):
    """Plots the sequence of magnitude versus effective temperature for seemingly single stars.
    """
    if axes is None:
        axes = plt.axes()
    phot, single = pleiades_single_stars(arr)
    scat = axes.scatter(single.weighted(as_Teff)[0], phot['r'], c=sp.log10(phot['distance']))
    cbar = axes.figure.colorbar(scat)
    axes.plot(teff_pleiades_benchmark(arr, as_Teff), pleiades_benchmark()['r'])
    axes.set_xlabel('effective temperature (K)')
    axes.set_ylabel('INT-WFC r (mag)')
    cbar.set_label('photometric distance from the single-star locus')
    return axes


def teff_pleiades_benchmark(arr, as_Teff='Teff', min_Teff=3510):
    """Returns the effective temperatures of the Bell et al. (2012) sequence.
    """
    phot = distance_to_single(arr)
    arrp = get_pleiades(arr)
    use = (arrp.weighted(as_Teff)[0] > min_Teff) & (phot['distance'] < 1.)
    teff = sp.polyval(sp.polyfit(phot['r'][use], arrp.weighted(as_Teff)[0][use], 1), pleiades_benchmark()['r'])
    teff[(pleiades_benchmark()['r'] > max(phot['r'][use])) |
         (pleiades_benchmark()['r'] < min(phot['r'][use]))] = sp.nan
    return teff


def teff_pleiades_benchmark_synth(band='Ks'):
    """Returns the effective temperature from the synthetic spectra based on given band.
    """
    all_iso = synthetic_isochrones()
    iso = all_iso[all_iso['log_age_yr'] == 8.0249996]
    for scaffold in ['2mass_%s', 'intwfc_%s']:
        long_band = scaffold % band.lower()
        if long_band in iso.dtype.names:
             break
    benchmark = pleiades_benchmark()
    teff = sp.interp(benchmark[band], iso[long_band][::-1] + 5.45, 10 ** iso['log_Teff'][::-1])
    teff[~(benchmark[band] < max(iso[long_band] + 5.45))] = sp.nan
    return teff


def logg_correction(Teff, age_out=5, age_in=110):
    """Returns the color corrections due to age differences for given effective temperatures.

    Arguments:
    - `Teff`: array-like effective temperature (K).
    - `age_out`: age at which the colors are needed (Myr).
    - `age_in`: age at which the colors are known (Myr).
    """
    Teff = sp.asarray(Teff)
    arrtot = synthetic_isochrones()
    phot_correctT = sp.zeros((Teff.size, 2), dtype=arrtot.dtype)
    for ixage, age in enumerate((age_in, age_out)):
        available_log_age = sp.unique(arrtot['log_age_yr'])
        log_age = available_log_age[sp.argmin(abs(sp.log10(age) + 6. - available_log_age))]
        arr_age = arrtot[arrtot['log_age_yr'] == log_age]
        for name in arrtot.dtype.names:
            phot_correctT[:, ixage][name] = sp.interp(sp.log10(Teff), arr_age['log_Teff'], arr_age[name])
    result = sp.zeros(Teff.size, dtype=arrtot.dtype)
    for name in arrtot.dtype.names:
        result[name] = phot_correctT[name][:, 1] - phot_correctT[name][:, 0]
    return result

def sequence_cluster(arr, cluster='IC 348'):
    """Returns the single-star sequence for the age and distance from another cluster
    """
    age = {'NGC 2264': 10, 'IC 348': 8, 'NGC 1333': 2, 'Pleiades': 110}
    DM = {'NGC 2264': 0, 'IC 348': 7.38, 'NGC 1333': 0, 'Pleiades': 5.45}
    if cluster not in age.keys():
        raise ValueError('No age defined for %s' % cluster)

    teff_arr = teff_pleiades_benchmark_synth()
    finite_teff = sp.isfinite(teff_arr)
    correction = logg_correction(teff_arr[finite_teff], age[cluster])
    pleiades_sequence = pleiades_benchmark()
    corrected_sequence = pleiades_sequence.copy()
    for band1, band2 in zip(['g', 'r', 'i', 'Z', 'J', 'H', 'Ks'], ['intwfc_g', 'intwfc_r', 'intwfc_i', 'intwfc_z', '2mass_j', '2mass_h', '2mass_ks']):
        corrected_sequence[band1] = sp.nan
        corrected_sequence[band1][finite_teff] = pleiades_sequence[band1][finite_teff] + correction[band2] - DM['Pleiades'] + DM[cluster]
    return teff_arr[finite_teff], corrected_sequence[finite_teff]

def intrinsic_color(arr, as_Teff='Teff', min_Teff=3550):
    """Adding intrinsic SEDs to the stored arrays.

    Columns listing the intrinsic SED start with 'i_'.
    """
    pleiades_sequence = pleiades_benchmark()
    new_names = ['i_%s' % name for name in ('Teff', ) + pleiades_sequence.dtype.names]
    dtype_new = arr.dtype.descr + [(name, 'f8') for name in new_names]
    result = sp.zeros(arr.size, dtype=dtype_new)

    for name in arr.dtype.names:
        result[name] = arr[name]
    for name in new_names:
        result[name] = sp.nan

    for cluster in sp.unique(arr['cluster']):
        use = arr['cluster'] == 'cluster'
        try:
            teff, sequence = sequence_cluster(arr, cluster)
        except ValueError as err:
            print err
            continue
        for name in sequence.dtype.names:
            result['i_%s' % name][use] = sp.interp(arr.weighted(as_Teff)[0][use], teff[::-1], sequence[name][::-1], left=sp.nan, right=sp.nan)
        result['i_Teff'][use] = arr.weighted(as_Teff)[use]
    return result


def sed_fit(arr, cluster='IC 348', as_Teff='Teff'):
    """Fit the SEDs of the stars in chosen cluster with optical and NIR photometry.
    """
    use = (arr['cluster'] == cluster) & arr['l_B13']
    teff, sequence = sequence_cluster(arr, 'IC 348', as_Teff)
    tsequence = lambda Teff, band: sp.interp(Teff, teff[::-1], sequence[band][::-1], left=sp.nan, right=sp.nan)

    bands = ('g', 'r', 'i', 'Z', 'J', 'H', 'Ks')
    bands_sequence = ['%s_%s' % (system, band.lower()) for system, band in zip(['intwfc'] * 4 + ['2mass'] * 3, bands)]
    from .. import spectral_parameters
    arr_spec = arr.view(spectral_parameters.AllParameters)
    #bands_meas = ['l_B13_%s_MAG' % band for band in bands]
    extinction = sp.load(os.path.join(os.path.split(__file__)[0], 'extinction_law.npy'))
    single_extinction = extinction[2]
    AxAv = sp.array([single_extinction['intwfc_%s' % band.lower() if 'intwfc_%s' % band.lower() in single_extinction.dtype.names
                        else '2mass_%s' % band.lower()] for band in bands]) / single_extinction['johnson_v']

    result = sp.zeros((sp.sum(use), 1000), dtype=[(name, 'f8') for name in ('Teff', 'Av', 'DM', 'goodness_of_fit')])
    teff_test = sp.linspace(min(teff), max(teff), 1000)
    result['Teff'] = teff_test[sp.newaxis, :]
    for ixtuse, tuse in enumerate(teff_test):
        intrinsic = sp.array([arr_spec[use].photometry(band) - tsequence(tuse, band) for band in bands])
        Av, DM = sp.polyfit(AxAv, intrinsic, 1)
        result[:, ixtuse]['Av'] = Av
        result[:, ixtuse]['DM'] = DM
        result[:, ixtuse]['goodness_of_fit'] = sp.sum((AxAv[:, None] * Av[None, :] + DM[None, :] - intrinsic) ** 2., 0)
    return arr_spec[use], result


def benchmark_fit(arr, correct=True):
    """Fit the Pleiades benchmark to the photometry
    """
    from .. import spectral_parameters
    bands = ('g', 'r', 'i', 'Z', 'J', 'H', 'Ks')
    arr_spec = arr.view(spectral_parameters.AllParameters)
    if correct:
        benchmark = sequence_cluster(arr, 'IC 348')[1]
    else:
        benchmark = pleiades_benchmark()

    extinction = sp.load(os.path.join(os.path.split(__file__)[0], 'extinction_law.npy'))
    single_extinction = extinction[2]
    AxAv = sp.array([single_extinction['intwfc_%s' % band.lower() if 'intwfc_%s' % band.lower() in single_extinction.dtype.names
                        else '2mass_%s' % band.lower()] for band in bands]) / single_extinction['johnson_v']

    int_benchmark = sp.zeros(1000, benchmark.dtype)
    rint = sp.linspace(sp.nanmin(benchmark['r']), sp.nanmax(benchmark['r']), int_benchmark.size)
    for name in benchmark.dtype.names:
        int_benchmark[name] = sp.interpolate.interp1d(benchmark['r'], benchmark[name], 'linear')(rint)

    use = sp.isfinite(arr_spec.photometry('g'))
    offset = sp.array([arr_spec.photometry(band)[:, None] - int_benchmark[None, :][band] for band in bands])
    Av = sp.zeros((arr_spec.size, int_benchmark.size))
    Av[:] = sp.nan
    DM = Av.copy()
    Av[use, :], DM[use, :] = sp.polyfit(AxAv, offset[:, use, :].reshape(7, -1), 1).reshape(2, sp.sum(use), int_benchmark.size)
    goodness_of_fit = sp.sum((offset - AxAv[:, None, None] * Av[None, :, :] - DM[None, :, :]) ** 2, 0)
    bestfit = sp.nanargmin(goodness_of_fit[use, :], 1)

    result = sp.zeros(arr_spec.shape, arr_spec.dtype.descr + [('p_' + name, 'f8') for name in benchmark.dtype.names + ('Av', 'DM', 'offset')])
    for name in benchmark.dtype.names + ('Av', 'DM', 'offset'):
        result['p_' + name] = sp.nan
    for name in arr_spec.dtype.names:
        result[name] = arr_spec[name]
    for name in benchmark.dtype.names:
        result['p_' + name][use] = int_benchmark[name][bestfit]
    result['p_Av'][use] = Av[use, bestfit]
    result['p_DM'][use] = DM[use, bestfit]
    result['p_offset'][use] = goodness_of_fit[use, bestfit]

    return result

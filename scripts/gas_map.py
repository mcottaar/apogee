import pyfits
import os
import scipy as sp
import matplotlib.pyplot as plt
from astropy import wcs


def ic348_map():
    """Loads the Herschell gas map for IC 348 from Jonathan Foster
    """
    return load_gas_map(os.path.join(os.path.split(__file__)[0], 'gas', 'IC348_pub_coln.fits'))

def ngc1333_map():
    """Loads the Herschell gas map for NGC 1333 from Jonathan Foster
    """
    return load_gas_map(os.path.join(os.path.split(__file__)[0], 'gas', 'NGC1333_pub_coln.fits'))


def load_gas_map(filename):
    """Loads a Herschel gas map sent by Jonathan Foster
    """
    hrd = pyfits.open(filename)
    pos = wcs.WCS(hrd[0].header)
    xpix = sp.arange(hrd[0].data.shape[1])
    ypix = sp.arange(hrd[0].data.shape[0])
    ra, dec = pos.all_pix2world(xpix[:, None], ypix[None, :], 1)
    return ra, dec, sp.array(hrd[0].data)


def plot_map(ra, dec, data, axes=None, vmin=0., vmax=6.5, **kwargs):
    """Plots an extinction map
    """
    if axes is None:
        axes = plt.axes()

    mappable = axes.contourf(ra, dec, data, aspect=1. / sp.cos(sp.median(dec) / 180. * sp.pi), levels=sp.arange(vmin, vmax, 0.1), extend='max', **kwargs)
    axes.set_xlabel('RA (deg)')
    axes.set_ylabel('dec (deg)')
    colorbar = plt.colorbar(mappable, ax=axes)
    colorbar.set_label('column density (A$_{\mathrm{V}}$)')
    return axes, colorbar

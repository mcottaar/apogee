"""Script that reads and fits the APOGEE RV grid with the Coelho grid.
"""

import pyfits
import os
import scipy as sp
from apogee import model_spectra, fit


class RVGridSpectrum(sp.recarray):
    """An observed Apogee spectrum"""
    meta = None


def read_grid(grid='RV', filename=None):
    """Reads in version 2 of the requested grid.
    """
    if grid == 'RV':
        model_grid = model_spectra.RVgrid(os.path.join('grid', 'apg_rvsynthgrid_v2.fits') if filename is None else filename)
    elif grid == 'btsettl':
        model_grid = model_spectra.pre_loaded(os.path.join('grid', 'BT-Settlgrid.npz') if filename is None else filename)
    return convert_to_fitters(convert_to_spectra(dict(model_grid._variables), model_grid._spectra))


def convert_to_spectra(parameters, spectra):
    """Converts the 2D array of spectra loaded from the fits-file into an array of spectra suitable for fitting.
    """
    result = sp.zeros(spectra.size, dtype='object')
    for index in range(spectra.size):
        spec_as_rec = sp.zeros(spectra[0].size, dtype=[(name, 'f8') for name in ('wavelength', 'flux', 'error', 'maxchi')] + [('mask', 'bool')])
        spec_as_rec['wavelength'] = spectra[index]['wavelength']
        spec_as_rec['flux'] = spectra[index]['flux']
        spec_as_rec['error'] = 1e-3
        spec_as_rec['maxchi'] = sp.inf
        spec_as_rec['mask'] = (spectra[index]['flux'] == 0.) | (spectra[index]['wavelength'] > 17000) | (spectra[index]['wavelength'] < 15100)
        spec = spec_as_rec.view(RVGridSpectrum)
        spec.meta = dict([(name, value[index]) for name, value in parameters.items()])
        result[index] = spec
    return result

def convert_to_fitters(spectra):
    """Converts the spectra from `convert_to_spectra` to a SpectralFit object, which fits the spectra.
    """
    grid = model_spectra.coelho('/Users/mcottaar/Work/data/spectra/Coelho05').at_value('FeH', 0.).convert_spectra(new_wavelength=15000 * sp.exp(sp.arange(1e5) / 700000.)).convert_spectra(resolution=22500) 
    tofit = fit.SpectralFit(list(spectra), grid)
    for name in tofit.parameters.possible_keys:
        if name != 'hydrogen_params':
            tofit.parameters.vary_till_depth(name, 1)
    return tofit


if __name__ == '__main__':
    import sys
    fitters = read_grid('btsettl', filename='BT-Settlgrid.npz')
    tofit = fitters[int(sys.argv[1])]
    input_par = dict([('inp_%s' % name, value) for name, value in tofit.meta().items()])
    tofit.set_default()
    tofit.optimize(('vrad', 'Teff', 'logg', 'vsini', 'veiling'), method='de', maxFunEvals=1e4, renormalize=True)
    import pickle
    filename = 'fit_rvsynthgrid_%i.pickle' % int(sys.argv[1])
    input_par.update(tofit.parameters)
    pickle.dump(input_par, open(filename, 'w'))
